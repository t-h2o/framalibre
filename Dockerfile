FROM ruby:3.1

WORKDIR /usr/site

COPY ./Gemfile /usr/site/Gemfile
COPY ./Gemfile.lock /usr/site/Gemfile.lock

RUN gem install bundler
RUN bundle install

EXPOSE 4000