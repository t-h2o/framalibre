---
nom: "2fast"
date_creation: "Vendredi, 18 novembre, 2022 - 12:37"
date_modification: "Vendredi, 18 novembre, 2022 - 12:37"
logo:
    src: "images/logo/2fast.png"
site_web: "https://github.com/2fast-team/2fast"
plateformes:
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Le client TOTP libre pour Windows."
createurices: "Jan Philipp Weber / Malte Hellmeier"
alternative_a: "Authy"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "sécurité"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

2fast (acronyme anglais pour 'two factor authenticator supporting TOTP', ie authentificateur à deux facteurs pour TOTP) est le premier logiciel libre pour la double authentification sous Windows permettant de stocker les données sensibles cryptées à un endroit de votre choix (au lieu d'un site cloud).

