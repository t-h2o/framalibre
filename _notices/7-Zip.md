---
nom: "7-Zip"
date_creation: "Mardi, 17 février, 2015 - 02:16"
date_modification: "Lundi, 10 mai, 2021 - 13:59"
logo:
    src: "images/logo/7-Zip.png"
site_web: "https://www.7-zip.fr/"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "7-zip est un logiciel de compression et de décompression"
createurices: ""
alternative_a: "winrar, winzip"
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "système"
    - "compression de fichiers"
    - "zip"
lien_wikipedia: "https://fr.wikipedia.org/wiki/7-Zip"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
---

7-zip est un logiciel de compression et de décompression de fichiers. Il vous permettra ainsi d'archiver facilement plusieurs fichiers ou dossiers dans un seul fichier compressé. Il est également utile pour décompresser ces fichiers d'archive.
7-zip a le gros avantage de supporter un très grand nombre de formats :
 En compression et décompression : 7z, ZIP, GZIP, BZIP2 et TAR ;
 Uniquement en décompression : ARJ, CAB, CHM, CPIO, DEB, DMG, HFS, ISO, LZH, LZMA, MSI, NSIS, RAR, RPM, UDF, WIM, XAR et Z. 
7-zip intègre également un navigateur de fichiers, mais vous pourrez accéder à la compression et décompression avec l'explorateur de fichiers de Windows.
7-zip désigne également un format de compression (fichiers avec l'extension 7z) qui est beaucoup plus efficace que le célèbre zip. Pour les autres plateformes que Windows, on trouvera d'autres logiciels supportant ce format ici.

