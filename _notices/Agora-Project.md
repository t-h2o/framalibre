---
nom: "Agora-Project"
date_creation: "Mardi, 13 juin, 2017 - 15:06"
date_modification: "Vendredi, 13 août, 2021 - 18:15"
logo:
    src: "images/logo/Agora-Project.png"
site_web: "https://www.omnispace.fr"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
    - "Web"
langues:
    - "Français"
    - "English"
    - "Español"
description_courte: "Une plateforme de travail collaboratif complète et intuitive."
createurices: "xavier armenteros"
alternative_a: "Google Drive, Google calendar, Facebook, Google groups"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "travail collaboratif"
    - "groupware"
    - "espace numérique de travail (ent)"
    - "réseau social"
    - "partage de fichiers"
    - "actualités"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Agora-project"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Agora-Project est une application web de partage de fichiers, d'agendas, d'actualités, de forums, etc. pour un travail collaboratif simple et efficace. Créez un espace de travail pour partager avec votre équipe des fichiers, des agendas, des fils d'actualité, des visioconférences, des contacts, des notes, des forums, des sondages...
Agora-Project est une plateforme de travail collaboratif complète et intuitive qui centralise des outils de gestion de fichiers, de gestion du temps et des outils de communication.

