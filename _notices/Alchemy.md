---
nom: "Alchemy"
date_creation: "Vendredi, 10 mars, 2017 - 22:37"
date_modification: "Samedi, 25 mars, 2017 - 12:07"
logo:
    src: "images/logo/Alchemy.png"
site_web: "http://al.chemy.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Dessiner avec une touche d'aléatoire et d'expérimentation pour des résultats rapides !"
createurices: "Karl D.D. Willis et Jacob Hina"
alternative_a: "Microsoft Paint, Paint.net"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "dessin"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Alchemy est plutôt un carnet de croquis pour des dessins expérimentaux qui propose une approche à l'opposé des classiques MyPaint, Gimp et Krita :
Pas de retour en arrière ni d’enregistrement, seule une exportation est possible ;
Pas de fonctionnalités comme la plume ou le pinceau, mais un tracé qui se déforme plus ou moins selon la vitesse, ou un autre qui modifie le tracé en fonction du son capté par le microphone de l’ordinateur.
Plus d'info sur LinufFr.org : http://linuxfr.org/news/l-expression-graphique-sous-gnu-linux#alchemy--c...
Il faut vraiment utiliser ce logiciel pour se rendre compte du potentiel de ses fonctionnalités. Cependant, le fignolage n'est pas facile : un outil de dessin plus classique est souvent nécessaire pour transformer son croquis en dessin final.
Un des créateurs, Karl D.D. Willis, continue de créer de nouvelles forme de créativité.

