---
nom: "Arch Linux"
date_creation: "Lundi, 11 février, 2019 - 20:53"
date_modification: "Mercredi, 12 mai, 2021 - 15:37"
logo:
    src: "images/logo/Arch Linux.png"
site_web: "https://www.archlinux.org/"
plateformes:
    - "GNU/Linux"
langues:
    - "English"
description_courte: "La distribution GNU/Linux pour les utilisateurs avancés."
createurices: "Judd Vinet"
alternative_a: "Microsoft Windows, MacOS"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "système d'exploitation (os)"
    - "distribution gnu/linux"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Arch_Linux"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Arch Linux est une distribution GNU/Linux basée sur le principe KISS (Keep it simple, stupid) et proposée en "rolling release" (développement continu). Cette distribution est très minimaliste et légère, elle ne possède pas d'interface graphique par défaut ni d'installateur. Elle se réserve donc à un usage par des professionnels de l'informatique ou des geeks.

