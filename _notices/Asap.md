---
nom: "Asap"
date_creation: "Lundi, 11 mars, 2019 - 10:57"
date_modification: "Mercredi, 12 mai, 2021 - 15:31"
logo:
    src: "images/logo/Asap.png"
site_web: "https://www.omnibus-type.com/fonts/asap/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Apple iOS"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Asap est une famille de police de caractères standardisée aux terminaisons sobrement arrondies."
createurices: "Pablo Cosgaya, Héctor Gatti, Andrés Torresi"
alternative_a: "Meddle"
licences:
    - "Open Font License (SIL-OFL)"
tags:
    - "bureautique"
    - "police de caractères"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Asap est une famille de police de caractères basée sur Ancha. Elle est standardisée sur tous les styles, ce qui permet de passer de l'un à l'autre sans modifier la longueur de ligne. Ses glyphes possèdent des terminaisons sobrement arrondies, offrant une douceur sans tendre vers le côté ludique. Elle est disponible sous SIL Open Font License.
Ci-dessous, les noms des 8 styles :
Asap Regular
Asap Italic
Asap Medium
Asap Medium Italic
Asap Semibold
Asap Semibold Italic
Asap Bold
Asap Bold Italic

