---
nom: "Asymptote"
date_creation: "Lundi, 26 décembre, 2016 - 21:07"
date_modification: "Lundi, 26 décembre, 2016 - 21:40"
logo:
    src: "images/logo/Asymptote.png"
site_web: "http://asymptote.sourceforge.net/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Langage de description de graphismes vectoriels"
createurices: "Andy Hammerlindl, John Bowman, Tom Prince"
alternative_a: ""
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "science"
    - "dessin vectoriel"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Asymptote_(logiciel)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Asymptote est à la fois un logiciel de dessin vectoriel et un langage de programmation généraliste, quoique orienté vers la production de graphiques, schémas, figures géométriques, etc.
Il permet de dessiner en deux et trois dimensions.
Il s’interface simplement à (La)TeX.

