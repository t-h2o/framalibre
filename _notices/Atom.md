---
nom: "Atom"
date_creation: "Dimanche, 24 avril, 2016 - 19:11"
date_modification: "Vendredi, 10 juillet, 2020 - 15:31"
logo:
    src: "images/logo/Atom.png"
site_web: "https://atom.io/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Atom est un éditeur de texte inspiré de Sublime Text."
createurices: ""
alternative_a: "Notepad, Sublime Text, Brackets"
licences:
    - "Licence MIT/X11"
tags:
    - "développement"
    - "traitement de texte"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Atom_%28%C3%A9diteur_de_texte%29"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
---

Atom est un éditeur de texte inspiré de Sublime Text. Il permet le développement de petits projets et dispose de nombreux plug-ins pour s'adapter à différentes utilisations.
Il est par exemple possible de visualiser la syntaxe markdown, de bénéficier de l'autocompletion, de l'affichage en mode projet du contenu d'un répertoire, de la coloration syntaxique sur de nombreux languages de programmation, de la possibilité de diviser la fenêtre verticalement ou horizontalement pour comparer deux documents et de bien d'autres fonctionnalités.
Atom utilise le framework Electron.

