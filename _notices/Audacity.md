---
nom: "Audacity"
date_creation: "Jeudi, 29 décembre, 2016 - 16:30"
date_modification: "Mardi, 1 septembre, 2020 - 11:30"
logo:
    src: "images/logo/Audacity.png"
site_web: "http://www.audacityteam.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Le logiciel vedette pour l'edition audio"
createurices: "Dominic Mazzoni"
alternative_a: "Adobe Audition"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "retouche audio"
    - "logiciel audio"
    - "mao"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Audacity"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
---

Audacity est un logiciel libre pour la manipulation de données audio numériques. Audacity permet d'enregistrer du son numérique par le biais des entrées ligne/micro/cd des cartes sons. Il permet d'éditer (copier, coller, sectionner…) les sons sur plusieurs pistes, et il est accompagné de divers filtres et effets : pitch, tempo, réduction de bruit, égaliseur, filtres de Fourier, augmentation de fréquences précises, compression, amplification, normalisation, écho, phaser, wahwah, inversion…
Une liste Framaliste a été créée : https://framalistes.org/sympa/subscribe/audacity
et un salon Jabber : https://candy.jabberfr.org/audacity@chat.jabberfr.org

