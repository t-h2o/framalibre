---
nom: "Basket"
date_creation: "Samedi, 31 décembre, 2016 - 14:48"
date_modification: "Mercredi, 12 mai, 2021 - 15:51"
logo:
    src: "images/logo/Basket.png"
site_web: "https://basket-notepads.github.io/"
plateformes:
    - "GNU/Linux"
    - "BSD"
langues:
    - "Autres langues"
description_courte: "Logiciel pour prendre et organiser tout type de notes."
createurices: ""
alternative_a: "Evernote"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "notes"
    - "organisation"
    - "projet"
    - "tâche"
    - "gtd"
    - "prise de notes"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Basket_Note_Pads"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Basket est une application de prise de notes. On peut capturer toute sorte de contenu: texte, images, liens hypertextes, mais aussi cases à cocher, lanceurs de logiciels, liens vers des fichiers, captures de parties d'écran, etc. La fenêtre de capture, très pratique, permet de les disposer où on le souhaite. On peut ensuite ajouter de la couleur, des étiquettes, on peut grouper des notes, réduire un groupe, etc. Les notes peuvent être hiérarchisées et se lier entre elles.
Logiciel très pratique et coloré !

