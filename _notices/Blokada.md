---
nom: "Blokada"
date_creation: "Samedi, 16 novembre, 2019 - 12:03"
date_modification: "Lundi, 18 novembre, 2019 - 09:30"
logo:
    src: "images/logo/Blokada.png"
site_web: "https://blokada.org/"
plateformes:
    - "Android"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Un bloqueur de pub et de trackers des applications Android très efficace."
createurices: "Karol Gusak, Johnny Bergström, Pascal Single, Antonio Galea"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "bloqueur de publicité"
    - "android"
    - "vpn"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Blokada est un bloqueur de pub et de trackers (listes de blockage comme pour uBlock Origin) disponible uniquement sur Android qui permet d'empêcher les applications d'envoyer des statistiques d'utilisation et d'afficher des publicités en interceptant le trafique réseau à l'aide de la fonctionnalité VPN d'Android. Attention, Blokada ne se connecte pas à un serveur VPN distant mais utilise une sorte de serveur VPN local puisqu'il s'agit de la seule méthode permettant d'intercepter et contrôler le trafique réseau sans être root. Il propose également de changer de serveurs DNS (choix parmi une liste ou choix personnalisé) et d'utiliser un serveur VPN distant (service payant).

