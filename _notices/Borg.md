---
nom: "Borg"
date_creation: "Samedi, 7 janvier, 2017 - 22:08"
date_modification: "Dimanche, 8 janvier, 2017 - 10:24"
logo:
    src: "images/logo/Borg.png"
site_web: "https://borgbackup.readthedocs.io/en/stable/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Borg est un outil en ligne de commande de sauvegarde de données qui gère la dé-duplication et le chiffrement"
createurices: "Thomas Waldmann, Jonas Borgström, ..."
alternative_a: "time machine"
licences:
    - "Berkeley Software Distribution License (BSD)"
tags:
    - "sécurité"
    - "sauvegarde"
    - "chiffrement"
    - "copie"
    - "stockage"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Borg Backup (en raccourci borg) est un logiciel de sauvegarde de données. Il réalise la dé-duplication des données (c'est à dire qu'un bloc de données n'est copié qu'une fois dans les archives même s'il est présent en plusieurs copies dans les dossiers sauvegardés). Les sauvegardes sont compressées et chiffrées par défaut pour permettre leur stockage sur des serveurs distants en toute sécurité.
Borg permet de faire des sauvegardes quotidiennes très efficaces grâce à la dé-duplication. Une opération de sauvegarde interrompue (coupure réseau, pas le temps d'attendre la fin,...) peut être poursuivie plus tard grâce à la notion de points de reprise.
C'est un logiciel en ligne de commande, écrit en grande partie en python, avec quelques bouts critiques pour les performances en C.
Les archives sauvegardées peuvent être montées dans le système de fichiers pour retrouver facilement des fichiers détruits par erreur dont on a oublié l'emplacement exact.

