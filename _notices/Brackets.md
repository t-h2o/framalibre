---
nom: "Brackets"
date_creation: "Mercredi, 22 mars, 2017 - 09:38"
date_modification: "Lundi, 4 mars, 2019 - 01:18"
logo:
    src: "images/logo/Brackets.jpg"
site_web: "http://brackets.io/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Brackets est un éditeur open source pour le web design et le développement."
createurices: "Adobe Systems"
alternative_a: "Sublime Text, Notepad"
licences:
    - "Licence MIT/X11"
tags:
    - "développement"
    - "éditeur"
    - "éditeur html"
    - "html"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Brackets"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Brackets est un éditeur open source pour le web design et le développement HTML, CSS et JavaScript.
Ce projet fut créé par Adobe Systems, et est publié sous la licence MIT.
L'éditeur supporte également l'ajout de fonctionnalités via des extensions (sortes de modules complémentaires).

