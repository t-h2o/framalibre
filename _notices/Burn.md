---
nom: "Burn"
date_creation: "Jeudi, 20 juillet, 2017 - 22:13"
date_modification: "Lundi, 10 mai, 2021 - 12:35"
logo:
    src: "images/logo/Burn.png"
site_web: "http://burn-osx.sourceforge.net/Pages/English/home.html"
plateformes:
    - "Mac OS X"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Gravez simplement et rapidement depuis Mac OS !"
createurices: ""
alternative_a: "ImgBurn, Nero, CDBurnerXP, Roxio"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "gravure"
    - "livecd"
    - "dvd"
    - "cd-rom"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Burn est un logiciel de gravure de CD et de DVD pour Mac OS.
Il permet de créer des sauvegardes de données, des DVD de vos distributions Linux et Windows préférées, de créer ses propres compilations musicales, etc... Vous pouvez également copier un CD/DVD sur un autre.
Burn vous offre la possibilité d'utiliser la fonction de CD-Text, et d'éditer les tags* de fichiers mp3.
Le logiciel supporte les formats les plus populaires de fichiers et il supporte tous types de CD et de DVD
P.S. : Le logiciel n'est plus mis à jour mais la dernière version est pleinement fonctionnelle.
*Tags : Permettent d'avoir des informations sur le contenu du fichier comme le titre, le nom de l'interprète, les commentaires, ou encore la date de sortie.

