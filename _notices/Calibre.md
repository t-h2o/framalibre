---
nom: "Calibre"
date_creation: "Mercredi, 22 mars, 2017 - 22:08"
date_modification: "Vendredi, 7 mai, 2021 - 17:03"
logo:
    src: "images/logo/Calibre.png"
site_web: "https://calibre-ebook.com/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Gestionnaire de bibliothèque de livres numériques."
createurices: "Kovid Goyal"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "ebook"
    - "livre"
    - "bibliothèque"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Calibre_(logiciel)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
---

Logiciel qui permet, entre autres, de gérer sa bibliothèque d'ebooks, d'envoyer ses livres vers sa liseuse ou de les convertir dans divers formats. Il supporte tous les formats d'ebooks les plus populaires tels que : EPUB, AZW3, PDF, RTF, etc.

