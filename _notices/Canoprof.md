---
nom: "Canoprof"
date_creation: "Vendredi, 21 avril, 2017 - 15:10"
date_modification: "Jeudi, 5 janvier, 2023 - 18:38"
logo:
    src: "images/logo/Canoprof.png"
site_web: "https://www.reseau-canope.fr/canoprof.html"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
description_courte: "CANOPROF est un service offert aux enseignants pour produire leurs supports de cours (primaire et secondaire)."
createurices: ""
alternative_a: "Microsoft Office, Microsoft Word, Microsoft PowerPoint"
licences:
    - "Licence CECILL (Inria)"
    - "Licence Publique Générale GNU (GNU GPL)"
    - "Licence publique générale limitée GNU (LGPL)"
    - "Licence Publique Mozilla (MPL)"
tags:
    - "éducation"
    - "chaîne éditoriale"
    - "diaporama"
    - "exerciseur"
    - "traitement de texte"
    - "création de site web"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Canoprof est à la fois :

une chaîne éditoriale orientée pour les enseignants du primaire et du secondaire
un service en ligne (cloud) pour centraliser les productions et les partager entre collègues
un service de publication des production sur le web avec génération automatique du QRCode correspondant à la ressource mise en ligne

Avec le modèle documentaire CANOPROF l'enseignant va pouvoir :

élaborer des supports de cours aux formats papier / web
élaborer également des diaporama de présentation
proposer à ses élèves des exercices d'autoévaluation
préparer des ressources pour Moodle, EDX
…

CANOPROF est un des modèles documentaires de Scenari mais dont l'usage en tant que service de publication est réservé aux enseignants du primaire et du secondaire.

