---
nom: "Cargo"
date_creation: "Mercredi, 23 janvier, 2019 - 20:50"
date_modification: "Mercredi, 23 janvier, 2019 - 20:50"
logo:
    src: "images/logo/Cargo.png"
site_web: "https://doc.rust-lang.org/stable/cargo/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Cargo permet de gérer les dépendances et de compiler un « crate » Rust."
createurices: ""
alternative_a: ""
licences:
    - "Licence Apache (Apache)"
    - "Licence MIT/X11"
tags:
    - "développement"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Cargo est à la fois :
un gestionnaire de paquets pour Rust, permettant d’installer des applications et télécharger des bibliothèques écrites en Rust et publiées sur crates.io ;
un moteur de production, organisant les diverses étapes nécessaires à la compilation, le test, la génération de documentation d’un projet Rust.

