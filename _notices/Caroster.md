---
nom: "Caroster"
date_creation: "Mardi, 25 octobre, 2022 - 17:13"
date_modification: "Mardi, 25 octobre, 2022 - 17:13"
logo:
    src: "images/logo/Caroster.jpg"
site_web: "https://caroster.io/"
plateformes:
    - "Autre"
    - "Web"
langues:
    - "Français"
    - "English"
description_courte: "Plateforme de covoiturage pour l'organisation de covoiturage événementiel"
createurices: "octree"
alternative_a: ""
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:

lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Super simple d'utilisation, l'application permet d'organiser du covoiturage facilement pour tout type d'événement en un seul lien.
Concert, festival, anniversaire, séminaire, visite de site, sortie d'entreprise, colloque, week-end ...

