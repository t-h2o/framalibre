---
nom: "Chromium"
date_creation: "Jeudi, 7 décembre, 2017 - 10:04"
date_modification: "Mercredi, 12 mai, 2021 - 14:52"
logo:
    src: "images/logo/Chromium.png"
site_web: "https://chromium.woolyss.com/download/fr/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Apple iOS"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Chromium est un navigateur libre à l'origine du logiciel propriétaire Google Chrome
Ne pas les confondre !"
createurices: "The Chromium Project"
alternative_a: "Google Chrome, Vivaldi, Microsoft Edge, Apple Safari"
licences:
    - "Berkeley Software Distribution License (BSD)"
    - "Licence MIT/X11"
    - "Licence Publique Générale GNU (GNU GPL)"
    - "Licence publique générale limitée GNU (LGPL)"
    - "Licence Publique Mozilla (MPL)"
tags:
    - "internet"
    - "navigateur web"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Chromium_(navigateur_web)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Google a remplacé le moteur de rendu WebKit par Blink depuis avril 2013. Il permettrait de rendre le navigateur plus stable et plus léger 4,5. Il est compatible GNU/Linux, Mac OS X et Windows.
Sur le même principe, le projet de système d'exploitation libre Chromium OS6 est le projet supportant le Google Chrome OS. Les extensions de Chrome7 fonctionnent aussi avec Chromium8.
Chromium est en majeure partie sous licence BSD (de nombreuses parties du programme sont sous d'autres licences libres) alors que Google Chrome est distribué sous une licence propriétaire. Le code source de Chromium est par conséquent librement accessible et modifiable contrairement à celui de Google Chrome.

