---
nom: "Ciano"
date_creation: "Dimanche, 14 avril, 2019 - 17:44"
date_modification: "Mercredi, 12 mai, 2021 - 14:44"
logo:
    src: "images/logo/Ciano.png"
site_web: "https://robertsanseries.github.io/ciano/"
plateformes:
    - "GNU/Linux"
langues:
    - "English"
    - "Autres langues"
description_courte: "Un couteau suisse de conversion de fichier Vidéo,Audio,Images !"
createurices: "Robert San"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "convertisseur"
    - "retouche audio"
    - "vidéo"
    - "image"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Qu'est-ce que Ciano?
Ciano est une application de conversion multimédia de bureau chargée de convertir des vidéos, de la musique et des images. Créé à l'origine pour offrir la meilleure expérience possible dans le système d'exploitation élémentaire, Ciano utilise les plus grands outils de conversion: FFmpeg et ImageMagick.
Changer la langue de Ciano.
Vous aurez besoin de connaître le code de votre langue (ex. En = anglais).
Ajoutez ce code au fichier LINGUAS, dans une nouvelle ligne, après la dernière ligne.
Traduisez le fichier .pot en utilisant l'éditeur de PO de votre choix (je recommande POEdit).
Enregistrez-le sous .po dans ce dossier.
Et c'est tout! Vous avez traduit Ciano avec succès pour votre langue!

