---
nom: "ClamAV"
date_creation: "Dimanche, 10 mars, 2019 - 12:13"
date_modification: "Mercredi, 12 mai, 2021 - 15:19"
logo:
    src: "images/logo/ClamAV.png"
site_web: "https://www.clamav.net/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "English"
description_courte: "Un puissant antivirus pour Linux, Mac et Windows."
createurices: "Tomasz Kojm"
alternative_a: "Windows Defender, F-Secure, Norton, Avast"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "sécurité"
lien_wikipedia: "https://fr.wikipedia.org/wiki/ClamAV"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

ClamAv est un antivirus originellement développé pour les systèmes UNIX et pour le filtrage de courrier. Il a depuis, étendu ses fonctionnalités et est aujourd'hui multiplateforme.
Attention toutefois ClamAv ne possède pas d'interface graphique par défaut mais il est possible d'en installer une : ClamWin, ClamTk, KlamAV.

