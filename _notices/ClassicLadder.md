---
nom: "ClassicLadder"
date_creation: "Jeudi, 24 février, 2022 - 15:43"
date_modification: "Lundi, 28 février, 2022 - 13:49"
logo:
    src: "images/logo/ClassicLadder.png"
site_web: "https://sites.google.com/site/classicladder/"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Français"
    - "English"
description_courte: "Projet de langages utilisés en automatisme: langage à contact et grafcet.
Interface graphique de base."
createurices: "Le Douarain Marc"
alternative_a: ""
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "science"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Ce projet libre met à disposition deux types de langages couramment utilisés dans le monde de l'automatisme, à-savoir du langage à contact et du grafcet.
Il est directement utilisable en simulateur d'Automate Programmable Industriel (API), et intègre un éditeur graphique facile sous Gtk+
(utilisable dans le domaine de l'éducation).
Il permet également d'être utilisé sur des cibles embarquées, et éventuellement en temps-réel (sous Xenomai).
Possibilité à l'éditeur de se connecter sur une cible embarquée afin de pouvoir aisément observer dynamiquement le fonctionnement des réseaux, étapes/transitions du grafcet, ainsi que de transférer le paramétrage.
Prend en compte le protocole industriel modbus permettant, d'un côté de raccorder des modules d'entrées/sorties, et de l'autre à une supervision industrielle de s'y connecter pour superviser l'automate.
Intègre un journal des événements.
Développé en langage C portable. Fonctionnement interprété des langages.

