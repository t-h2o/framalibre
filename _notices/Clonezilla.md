---
nom: "Clonezilla"
date_creation: "Jeudi, 22 décembre, 2016 - 16:23"
date_modification: "Lundi, 4 mars, 2019 - 00:44"
logo:
    src: "images/logo/Clonezilla.png"
site_web: "http://clonezilla.org/"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Clonezilla Live est un Live CD permettant de cloner un disque entier sur un support externe local ou distant."
createurices: "Steven Shiau"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "sauvegarde"
    - "livecd"
    - "dépannage"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Clonezilla"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Clonezilla Live est un Live CD basé sur la distribution Debian GNU/Linux et contenant le logiciel Clonezilla. Il permet à l'utilisateur d'effectuer, directement depuis la machine :
une sauvegarde : copie du disque entier, ou d'une ou plusieurs partitions, sous forme d'une image envoyée vers tout moyen de stockage
une restauration : récupération d'une image depuis son point de stockage (même disque, autre disque, autre machine, clé USB, réseau, etc.)
une copie : copie directe des données depuis le disque original vers un autre disque
Cette version de Clonezilla peut se connecter à différents serveurs : serveur SSH, serveur Samba, système NFS…
Comme précisé dans son nom, le principe est celui du Live CD (CD-ROM ou DVD-ROM), mais il peut aussi être exécuté depuis une clé USB, un disque dur externe, etc. (source Wikipédia)

