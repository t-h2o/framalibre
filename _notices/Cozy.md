---
nom: "Cozy"
date_creation: "Vendredi, 6 janvier, 2017 - 01:39"
date_modification: "Mercredi, 5 décembre, 2018 - 18:33"
logo:
    src: "images/logo/Cozy.png"
site_web: "https://cozy.io/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Apple iOS"
    - "Web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Avec Cozy, votre nouveau domicile numérique, choisissez la simplicité."
createurices: ""
alternative_a: "Dropbox, One Drive, Google Drive"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "système"
    - "auto-hébergement"
    - "décentralisation"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Cozy"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Cozy est le premier domicile numérique du marché. Il offre aux utilisateurs un espace de stockage intelligent, décentralisé et sécurisé qui permet d'importer ses informations (telles que photos, relevés bancaires, factures d'électricité ou mobiles) dans un environnement personnel et personnalisable, accessible depuis n'importe quel périphérique. En plus du stockage, les utilisateurs ont accès à une multitudes d'applications pour gérer et organiser automatiquement leurs données.
Notre mission s’inscrit dans la vision d’un numérique éthique - ethical by design. Alors que les grandes plateformes récoltent de plus en plus de données personnelles, un Cozy est pour l’individu une solution dont il peut prendre le contrôle : un logiciel libre, auto-hébergeable et protégeant ses données - un service dont les intérêts sont "by design" alignés avec ses intérêts.

