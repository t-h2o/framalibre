---
nom: "Cwtch"
date_creation: "Vendredi, 16 juillet, 2021 - 23:08"
date_modification: "Mardi, 31 août, 2021 - 20:18"
logo:
    src: "images/logo/Cwtch.png"
site_web: "https://cwtch.im/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
langues:
    - "Autres langues"
description_courte: "Messagerie décentralisée ,protégeant la vie privée et résistante aux métadonnées."
createurices: "Open Privacy Research Society"
alternative_a: "Facebook Messenger, Messenger, WhatsApp, Google Hangouts, iMessage"
licences:
    - "Licence MIT/X11"
tags:
    - "internet"
    - "communication chiffrée"
    - "messagerie instantanée"
    - "tor"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Cwtch (prononcez  Keutch ) - C’est un mot gallois que l’on pourrai traduire par « un câlin qui crée un endroit sûr » , est un protocole de messagerie multipartite décentralisé : Il n'y a pas de "service Cwtch" ou de "réseau Cwtch". Les participants à Cwtch peuvent héberger leurs propres espaces sécurisés ou prêter leur infrastructure à d'autres personnes à la recherche d'un espace sécurisé, préservant la vie privée : toutes les communications dans Cwtch sont chiffrées de bout en bout sur les services tor en oignon v3. Cwtch a été conçu de telle sorte qu'aucune information n'est échangée ou disponible, pour quiconque, sans son consentement explicite, y compris les messages sur le fil et les métadonnées du protocole.
Des améliorations viendront avec le temps : les images,le partage de fichiers,les appels vidéo, etc. C’est une petite équipe , servant une organisation à but non lucratif qui fera de son mieux pour que nous puissions , dans un futur proche , Cwtch tous ensemble !

