---
nom: "Cytoscape"
date_creation: "Mardi, 10 février, 2015 - 15:44"
date_modification: "Samedi, 21 janvier, 2017 - 16:02"
logo:
    src: "images/logo/Cytoscape.png"
site_web: "http://www.cytoscape.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Cytoscape est un logiciel d'analyse et de visualisation de données de réseaux"
createurices: ""
alternative_a: ""
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "science"
    - "analyse statistique"
    - "statistiques"
    - "visualisation de données"
lien_wikipedia: "https://en.wikipedia.org/wiki/Cytoscape"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Cytoscape est un logiciel d'analyse et de visualisation de données de réseaux. Créé à l'origine pour le domaine de la biostatistique, il peut analyser n'importe quel type de réseau défini comme un ensemble de liens entre des objets (citations entre articles, réseaux informatiques, réseaux sociaux, etc.).
Cytoscape est très puissant et conçu pour gérer des réseaux de très grande taille. Il permet de produire toutes les statistiques descriptives classiques des données de réseaux.

