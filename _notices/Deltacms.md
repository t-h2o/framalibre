---
nom: "Deltacms"
date_creation: "Jeudi, 21 avril, 2022 - 08:35"
date_modification: "Jeudi, 23 mars, 2023 - 19:00"
logo:
    src: "images/logo/Deltacms.png"
site_web: "https://deltacms.fr"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Deltacms, un CMS sans base de données, libre, simple à installer et à utiliser."
createurices: "Sylvain L., Lionel C."
alternative_a: "Wordpress, Joomla"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "cms"
    - "gestionnaire de contenus"
    - "création de site web"
    - "flat-file"
    - "sans base de données"
    - "blog"
    - "agenda"
    - "diaporama"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Deltacms est un gestionnaire de contenus (CMS) sans base de données, qui permet de créer et de maintenir un site internet facilement.
Responsif : Deltacms s'adapte automatiquement à toutes les tailles d'écran.
Multilingue :  administration du site en français ou en anglais, rédaction dans votre langue, traduction automatique ou rédigée dans 16 langues européennes ou régionales.
Multi utilisateurs :  visiteur, membre pour l'accès à des pages privées, éditeur, modérateur ou administrateur pour des droits étendus.
Personnalisable: Vous pouvez choisir votre thème à l'installation, le personnaliser, le sauvegarder, importer un thème.
Modulaire : Deltacms dispose de nombreux modules qui permettent d'enrichir vos pages ( blog, gallery, agenda, diaporama, statistiques,...).

