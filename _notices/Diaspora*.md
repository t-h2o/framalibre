---
nom: "Diaspora*"
date_creation: "Samedi, 28 février, 2015 - 12:00"
date_modification: "Mercredi, 24 juillet, 2019 - 20:52"
logo:
    src: "images/logo/Diaspora*.png"
site_web: "https://diasporafoundation.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Web"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Un système de gestion de réseau social libre, éthique et décentralisé."
createurices: ""
alternative_a: "Facebook, Ello, Myspace, Twitter, Hubzilla"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "réseau social"
    - "décentralisation"
    - "vie privée"
lien_wikipedia: "http://fr.wikipedia.org/wiki/Diaspora_%28logiciel%29"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Diaspora* est un logiciel permettant de gérer une application de réseau social décentralisé et sécurisé. Créé en 2010, le projet Diaspora* vise à fournir des fonctionnalités similaires aux services de type Facebook sur la base d'un système décentralisé permettant de faire communiquer ensemble des « nœuds » Diaspora* (des instances) disséminés à travers le monde. Chaque participant peut ainsi communiquer avec les autres quelle que soit l'instance sur laquelle il est inscrit. Tout le monde peut ainsi héberger sa propre instance : particuliers, sociétés, associations, communautés...
Diaspora est écrit en Ruby. Des API sont disponibles pour assurer des interactions avec les autres réseaux sociaux existants (Twitter, Wordpress, FlikR...).

