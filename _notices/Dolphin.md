---
nom: "Dolphin"
date_creation: "Jeudi, 2 avril, 2020 - 19:52"
date_modification: "Samedi, 4 février, 2023 - 18:49"
logo:
    src: "images/logo/Dolphin.png"
site_web: "https://fr.dolphin-emu.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
langues:
    - "Autres langues"
description_courte: "Dolphin est un émulateur des consoles Wii et Gamecube."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "émulateur"
    - "jeu vidéo"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Dolphin_(%C3%A9mulateur)"
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/org.dolphinemu.dolphinemu/lates…"
identifiant_wikidata: ""
mis_en_avant: "non"
---

Dolphin est un émulateur multi-plateformes permettant de faire tourner les jeux-vidéo des consoles Nintendo Wii et Nintendo GameCube. Il est mis à jour régulièrement et les jeux tournent de manière optimale.

et personnalisation. Cela signifie que vous pouvez gérer vos fichiers de la manière exacte que vous voulez.

