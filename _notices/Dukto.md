---
nom: "Dukto"
date_creation: "Mercredi, 4 janvier, 2017 - 13:15"
date_modification: "Mercredi, 12 mai, 2021 - 15:42"
logo:
    src: "images/logo/Dukto.png"
site_web: "http://www.msec.it/blog/?page_id=11"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Windows Mobile"
langues:
    - "English"
description_courte: "Permet l'échange de fichiers sur un réseau local."
createurices: "Emanuele Colombo"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "réseau"
    - "partage"
    - "partage de fichiers"
    - "stockage"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Permet l'échange de fichier, dossier ou texte très facilement entre périphériques du même réseau local.
Il suffit de glisser-déposer les éléments à envoyer sur le périphérique sélectionné, et hop ! ceux-ci sont transférés par le réseau local.
Le logiciel n'est plus maintenu. Voici l'annonce (traduite avec DeepL) du développeur :
" MISE À JOUR : ce projet n'est plus maintenu. Vous pouvez le télécharger et l'utiliser s'il fonctionne sur vos appareils, mais je ne peux plus fournir de mise à jour et de support. Je ne fournirai pas non plus de mise à jour de sécurité ou de correction de bogues. "

