---
nom: "EasyTAG"
date_creation: "Vendredi, 7 juin, 2019 - 17:34"
date_modification: "jeudi, 10 août, 2023 - 10:40"
logo:
    src: "images/logo/EasyTAG.png"
site_web: "https://wiki.gnome.org/Apps/EasyTAG"
plateformes:
    - "Windows"
    - "GNU/Linux"
langues:
    - "English"
    - "Autres langues"
description_courte: "Un logiciel pour éditer les métadonnées de fichiers audio."
createurices: "Jerome Couderc, David King"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "métadonnées"
    - "gnome"
    - "audio"
    - "vidéo"
lien_wikipedia: "https://fr.wikipedia.org/wiki/EasyTAG"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
---

EasyTAG est un logiciel du projet GNOME permettant d'éditer les métadonnées des fichiers audio et vidéo (par exemple, le nom de l'artiste, de l'album, l'année, etc.).
Il permet notamment d'ajouter une ou plusieurs images à montrer pendant la lecture, de changer l'ordre dans lequel votre lecteur multimédia classe les fichiers pour respecter l'ordre de l'album (attention le gestionnaire de fichier continuera de les voir par ordre alphabétique), d'appliquer une étiquette a plusieurs fichiers simultanément...
EasyTAG peut même compléter automatiquement les métadonnées de certains fichiers en se basant sur l'encyclopédie musicale collaborative FreeDB (nécessite une connexion internet).
Bien que développé pour GNOME, EasyTAG est compatible avec les autres environnements.
EasyTAG supporte les formats suivants : MP2, MP3, MP4, FLAC, Ogg Opus, Ogg Speex, Ogg Vorbis, AAC, Monkey's Audio et WAV.
Bien que développé pour GNOME, EasyTAG est compatible avec les autres environnements.


