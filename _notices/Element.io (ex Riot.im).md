---
nom: "Element.io (ex Riot.im)"
date_creation: "Dimanche, 3 décembre, 2017 - 14:51"
date_modification: "Mercredi, 12 mai, 2021 - 16:12"
logo:
    src: "images/logo/Element.io (ex Riot.im).png"
site_web: "https://element.io"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Apple iOS"
langues:
    - "Autres langues"
description_courte: "Element rassemble toutes vos conversations et intégrations applicatives en une seule application."
createurices: ""
alternative_a: "Slack"
licences:
    - "Licence Apache (Apache)"
tags:
    - "internet"
    - "messagerie instantanée"
    - "chat"
    - "communication"
    - "vie privée"
    - "chiffrement"
    - "visioconférence"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Element_(logiciel)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
---

Construit autour des salons de discussion de groupe, Element vous permet de partager des messages, des images, des vidéos et des fichiers - interagissez avec vos outils et accédez à toutes vos différentes communautés sous un même toit. Une identité et un lieu unique pour toutes vos équipes : nul besoin de changer de compte, de travailler et de dialoguer avec des personnes de différentes organisations dans des espaces publics ou privés : des projets professionnels aux voyages scolaires, Element deviendra le centre de toutes vos discussions !
Pour les organisations, Element utilise la technologie Matrix permettant de créer sa propre instances fédérable avec les autres instance, dont celle de matrix.org
Jusqu'en juillet 2020, Element s'appelait riot.im.

