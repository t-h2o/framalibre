---
nom: "Epiphany Browser et Toolkit"
date_creation: "Jeudi, 25 juin, 2020 - 23:02"
date_modification: "Lundi, 29 juin, 2020 - 10:42"
logo:
    src: "images/logo/Epiphany Browser et Toolkit.png"
site_web: "https://www.gnome.org/"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un navigateur web léger pour l'environnement de bureau Gnome."
createurices: "Gnome"
alternative_a: "Chrome, Opera, Internet Explorer"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "navigateur web"
lien_wikipedia: "https://fr.wikipedia.org/wiki/GNOME_Web"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Epiphany Browser, ou simplement l'application Web, est un navigateur web léger construit pour le bureau Gnome.
Pour installer un bloqueur de publicités, regardez dans votre gestionnaire de paquets ceux commençant par webext-. Voire chromium- et contenant ublock pour vous protéger des pubs.
Epiphany n'a pas d'extension pour télécharger des vidéos, mais on peut le faire par ailleurs, par exemple avec le logiciel (en ligne de commande) youtube-dl, qui permet de télécharger des vidéos d'une grande variété de sites. On copie-colle le lien youtube (ou d'un autre site) sur le terminal contenant la ligne youtube-dl suivie d’un espace.

