---
nom: "EtherCalc"
date_creation: "Samedi, 27 octobre, 2018 - 19:07"
date_modification: "Vendredi, 21 mai, 2021 - 10:04"
logo:
    src: "images/logo/EtherCalc.png"
site_web: "https://ethercalc.net/"
plateformes:
    - "Autre"
    - "Web"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Un tableur collaboratif en temps réel !"
createurices: ""
alternative_a: "Office 365, Microsoft Excel, G Suite, Google Sheets"
licences:
    - "Common Public License (CPL)"
    - "Licence MIT/X11"
    - "Multiples licences"
tags:
    - "tableur"
    - "édition collaborative"
    - "travail collaboratif"
    - "décentralisation"
lien_wikipedia: "https://fr.wikipedia.org/wiki/EtherCalc"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

EtherCalc est un tableur collaboratif éditable en temps réel. Chaque tableur possède sa propre URL.
Il dispose des fonctionnalités de base : mise en forme, fonctions mathématiques, graphiques, ajout de commentaires, historique des versions, export en html ou en csv.
EtherCalc est un logiciel décentralisé : différentes instances permettent d'utiliser le service, elles sont mises à disposition par plusieurs organisations (par exemple le site éponyme ou le service Framacalc). Cela permet d'éviter de concentrer toutes les données entre les mains d'un même acteur. Les instances peuvent être personnalisées (limitation de la durée d'hébergement du tableur, modification du design, etc.).

