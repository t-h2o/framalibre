---
nom: "Evince"
date_creation: "Lundi, 9 avril, 2018 - 19:46"
date_modification: "Jeudi, 20 mai, 2021 - 16:39"
logo:
    src: "images/logo/Evince.png"
site_web: "https://wiki.gnome.org/Apps/Evince"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Evince est un logiciel ultra léger permettant de visualiser plus que des documents PDF et lire des BD"
createurices: "Mark Weiman"
alternative_a: "Adobe Acrobat Reader, ComicRack, WinDjView"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "lecteur pdf"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Cet outil libre propose de nombreuses fonctionnalités: zoom, rotation des documents et plusieurs options d'affichage pour lire dans les meilleures conditions.
Par ailleurs, Evince propose un moteur de recherche pour retrouver des mots ou des expressions au sein des documents.
Du côté de la compatibilité avec les formats, outre le PDF et le PostScript, Evince supporte également les extensions TIFF, DVI, XPS, DJVU, SyncTex et XPS.
Les Fans de comics, le logiciel prend en charge les formats les plus courants comme les CBZ, CBT et CB7.
En somme, Evince est très agréable visuellement et très simple d'utilisation.
NB :
Les développeurs ont privilégiés les développements pour Linux, mais la version 2.32 avait été compilée pour fonctionner sous Windows, on la trouve en téléchargement sur plusieurs sites non officiels et notamment en version portable sur https://portableapps.com/apps/office/evince_portable.

