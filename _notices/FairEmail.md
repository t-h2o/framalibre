---
nom: "FairEmail"
date_creation: "Jeudi, 4 avril, 2019 - 09:16"
date_modification: "Mercredi, 21 juillet, 2021 - 15:12"
logo:
    src: "images/logo/FairEmail.png"
site_web: "https://email.faircode.eu/"
plateformes: "Android"
langues: "Multilingue"
description_courte: "Client Android de messagerie, minimaliste privilégiant la lecture et la rédaction, orienté sécurité."
createurices: "Marcel Bokhorst"
licences: "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "Bureautique"
    - "mail"
    - "Android"
    - "f-droid"
    - "client mail"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/eu.faircode.email/latest/"
alternative_a: "Email"
mis_en_avant: "oui"
---

Principales caractéristiques
- Multiples comptes de réception et d'identités d’envoi,
- liste unifiée des courriels des différents comptes,
- fil de discussion,
- lecture en mode épuré (enlèvement des éléments non sûrs), on peut voir le mail selon son aspect original sur demande,
- synchronisation bidirectionnelle,
- stockage hors connexion,
- déplacement possible des courriels d'un compte vers un autre,
- principes sécuritaires.
FairEmail est un Logiciel opensource original (pas un fork ni un clone); gratuit en version de base, un don (7€) débloque toutes les fonctionnalités (le paiement ne se fait pas via gglaps, donc pas de compte ggl nécessaire).

