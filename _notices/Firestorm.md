---
nom: "Firestorm"
date_creation: "Lundi, 13 novembre, 2017 - 15:39"
date_modification: "Mercredi, 12 mai, 2021 - 16:55"
logo:
    src: "images/logo/Firestorm.png"
site_web: "http://www.firestormviewer.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Navigateur 3D type V3,  utilisé pour se connecter à des mondes virtuels OpenSimulator ou à Second Life™."
createurices: ""
alternative_a: "Second Life viewer"
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "jeu"
    - "simulation"
    - "3d"
    - "metarvers"
    - "éducation"
    - "monde virtuel"
    - "jeu vidéo"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Navigateur 3D,  utilisé pour se connecter à des mondes virtuels OpenSimulator ou à Second Life™ avec un compte utilisateur. Firestorm est basé sur le code Linden Lab V3 LGPL.
Ce client a  une interface récente et mieux adaptée à la construction que celle du viewer Singularity  ( par  exemple, il permet de régler les dimensions des objets 3D plus finement). En revanche, il semble un peu plus compliqué à utiliser pour un débutant. Mais c'est aussi une question d'habitude.

