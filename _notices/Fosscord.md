---
nom: "Fosscord"
date_creation: "Mardi, 27 juillet, 2021 - 19:12"
date_modification: "Vendredi, 7 avril, 2023 - 21:09"
logo:
    src: "images/logo/Fosscord.png"
site_web: "https://fosscord.com"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Apple iOS"
langues:
    - "English"
description_courte: "Un logiciel gratuit, open source et compatible avec Discord de chat."
createurices: "Flam3rboy, xnacly"
alternative_a: "Discord"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "internet"
    - "chat"
    - "visioconférence"
    - "vidéo"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Fosscord est un logiciel open source gratuit compatible avec Discord. Il s'agit d'une plateforme de chat, voix et vidéo similaire à Slack, Rocket.chat et compatible avec Discord.
Il est :
Auto-hébergeable
Configurable
Sécurisé
Décentralisé
Extensible
Thématisable

