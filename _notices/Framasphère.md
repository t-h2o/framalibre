---
nom: "Framasphère"
date_creation: "Samedi, 28 février, 2015 - 12:13"
date_modification: "Samedi, 7 janvier, 2017 - 23:35"
logo:
    src: "images/logo/Framasphère.png"
site_web: "https://framasphere.org/"
plateformes: "GNU/Linux, BSD, Mac OS X, Windows, Android, FirefoxOS, Windows Mobile, Autre"
langues: "Français, Multilingue"
description_courte: "Framasphère est un nœud (appelé pod) du réseau social libre Diaspora*"
createurices: ""
licences: "Licence Publique Générale Affero (AGPL)"
tags:
    - "Internet"
    - "réseau social"
    - "décentralisation"
    - "vie privée"
lien_wikipedia: ""
lien_exodus: ""
alternative_a: "Facebook, Ello"
mis_en_avant: "non"
---

Framasphère est un nœud (appelé pod) du réseau social libre Diaspora*.
Depuis Framasphère, vous pouvez :
échanger des messages et photos avec n'importe quelle autre personne du réseau Diaspora*
gérer vos contacts, tags, mentions, repartages…
garder le contrôle de vos données
publier sur d'autres réseaux sociaux (Facebook, Twitter, Tumblr ou Wordpress)
en somme, apprécier toutes les fonctionnalités d'un réseau social sans craindre la censure et dans le respect de votre vie privée

