---
nom: "FreeOrion"
date_creation: "Samedi, 1 avril, 2017 - 22:49"
date_modification: "Mercredi, 12 mai, 2021 - 16:52"
logo:
    src: "images/logo/FreeOrion.png"
site_web: "http://www.freeorion.org"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Conquête spatiale multi-joueur en tour par tour, jouable également en solo."
createurices: "Zach Laine, geoffthemedio, LGM-Doyle, Marcel Metz"
alternative_a: "Master of Orion, 4X"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "espace"
    - "stratégie"
    - "tour par tour"
    - "gestion"
    - "multi-joueur"
lien_wikipedia: "https://fr.wikipedia.org/wiki/FreeOrion"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

FreeOrion est un jeu de conquête spatiale style 4X, inspiré de Mater of Orion sans en faire un clone pour autant.
Après avoir choisi une espèces aux spécificité impactant le gameplay, vous pourrez commencez à vous développer, depuis votre planète natale, vers les voisines, puis la galaxy toute entière. Gestion des ressources, du ravitaillement, du moral, d'un arbre technologique fournie, des combats spaciaux et au sol (non interactif), un peu de diplomatie, une gestion de plusieurs degré de furtivité bien pensée... Il n'en faut pas plus pour faire un excellent jeu de stratégie, à jouer seul comme à plusieurs si personne n'est pressé. (Prévoyez plusieurs jours pour terminer une partie).

