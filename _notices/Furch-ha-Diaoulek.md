---
nom: "Furch-ha-Diaoulek"
date_creation: "Mercredi, 29 mars, 2017 - 15:22"
date_modification: "Lundi, 12 juin, 2017 - 09:25"
logo:
    src: "images/logo/Furch-ha-Diaoulek.png"
site_web: "http://furchhadiaoulek.free.fr"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Français"
    - "English"
description_courte: "Furch-ha-Diaoulek un outil pour faciliter l'étude des langues de l'Ouest Européen."
createurices: "Alphonse Nandert"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "langue"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

"Furch-ha-Diaoulek" est un outil pour l'apprentissage des langues. Il est constitué de :
"Diaoulek", un gestionnaire de vocabulaire. Un mot que vous cherchez à apprendre vous sera proposé tous les jours au début, puis à des intervalles de plus en plus espacés. La leçon du jour est constituée d'un ensemble de mots chacun à sa propre place dans son cycle d'apprentissage. Tout mot non su peut être remis dans la pile jusqu'à ce qu'il soit complètement connu. Les mots proviennent des leçons ou des dictionnaires du projet "FreeDict". Vous avez la possibilité de rédiger vos propres leçons.
"Furch" est une aide à la lecture des textes. En cliquant sur un mot, "Furch" recherche les articles de sa base de données qui lui semblent corrélés à ce mot. Il est possible de sélectionner certains articles pour les apprendre ensuite dans "Diaoulek". En l'état actuel "Furch" ne fonctionne encore que pour le breton.

