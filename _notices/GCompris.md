---
nom: "GCompris"
date_creation: "Dimanche, 11 décembre, 2016 - 20:06"
date_modification: "Mercredi, 12 mai, 2021 - 15:12"
logo:
    src: "images/logo/GCompris.png"
site_web: "http://gcompris.net/index-fr.html"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
langues:
    - "Autres langues"
description_courte: "Une suite qui propose de nombreuses activités éducatives pour les enfants."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "enfant"
    - "mathématiques"
    - "science"
    - "géographie"
lien_wikipedia: "https://fr.wikipedia.org/wiki/GCompris"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
---

GCompris est un logiciel éducatif qui propose diverses activités pour les enfants de 2 à 10 ans. Il couvre de nombreux domaines, qu'il est possible d'assimiler à 7 grandes catégories :
La découverte de l'ordinateur
Les mathématiques
Les sciences
La géographie
Les jeux
La lecture
Les autres activités (lecture de l'heure, puzzle, création d'un dessin animé…

