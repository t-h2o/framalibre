---
nom: "GLPI"
date_creation: "Samedi, 4 février, 2017 - 00:32"
date_modification: "Samedi, 4 février, 2017 - 00:32"
logo:
    src: "images/logo/GLPI.jpg"
site_web: "http://glpi-project.org/"
plateformes: "GNU/Linux, Mac OS X, Windows"
langues: "Français, Multilingue"
description_courte: "Gestionnaire de parc informatique, d'inventaire et de service \"Desk\" proche d'ITIL"
createurices: ""
licences: "Creative Commons -By-Sa, Licence Publique Générale GNU (GNU GPL)"
tags:
    - "Métiers"
    - "gestion de parc informatique"
    - "Help-desk"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Gestion_libre_de_parc_informatique"
lien_exodus: ""
alternative_a: "Peregrine system (AssetCenter), Remedy (BMC Software)"
mis_en_avant: "non"
---

GLPI (Gestion de Parc Libre Informatique) est un outil spécialisé dans la gestion de parc informatique. Il prend en charge les principaux processus ITIL qui gravitent autour de la gestion de parc (Gestion des demandes de services, des requêtes, des incidents, des problèmes, des changements, des SLA (contrats de services), ...
GLPI est une solution complète, adaptée à la majeure partie des parcs informatiques qu'ils soient petits, moyens ou grands.
L'interface web est claire, conviviale et intuitive. Elle est développée en PHP/AJAX sur une base de données MySQL/MariaDB.
Les fonctionnalités de l'outil peuvent être facilement étendues par une série de plugins disponibles directement sur le site de l'éditeur (gestionnaire d'inventaire, de badges, de licences, de base de données, de ressources Humaines, d'architecture réseau, ...)
Le produit peut être couplé avec une solution d'inventaire indépendante telle que OCS-Inventory.
L'installation est relativement simple et bien documentée.

