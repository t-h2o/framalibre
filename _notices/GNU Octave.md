---
nom: "GNU Octave"
date_creation: "Mardi, 14 mars, 2017 - 17:50"
date_modification: "Mardi, 14 mars, 2017 - 17:50"
logo:
    src: "images/logo/GNU Octave.png"
site_web: "https://www.gnu.org/software/octave/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
    - "Autres langues"
description_courte: "Logiciel de calcul numérique très fortement compatible avec les scripts de Matlab"
createurices: ""
alternative_a: "Matlab"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "science"
    - "calcul numérique"
    - "mathématiques"
lien_wikipedia: "https://fr.wikipedia.org/wiki/GNU_Octave"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

GNU Octave est un logiciel de calcul numérique (pas de calcul formel) qui peut être considéré comme une alternative libre à Matlab. En effet, sa syntax est celle de Matlab et les scripts de ce dernier sont presque entièrement compatibles.
Il est activement développé, et on apprend de temps en temps avec plaisir qu'une université (Standford, voir Wikipédia) le choisi.

