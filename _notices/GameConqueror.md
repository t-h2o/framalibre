---
nom: "GameConqueror"
date_creation: "Vendredi, 17 mars, 2017 - 21:34"
date_modification: "Samedi, 18 mars, 2017 - 11:07"
logo:
    src: "images/logo/GameConqueror.png"
site_web: "https://github.com/scanmem/scanmem"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Android"
langues:
    - "English"
description_courte: "GameConqueror embarque scanmem, un logiciel scannant la mémoire et permettant la triche dans les jeux vidéos."
createurices: ""
alternative_a: "ArtMoney"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "mémoire"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

GameConqueror est une interface graphique pour le scanneur de mémoire scanmem. Scanner la mémoire d'un programme permet de récupérer certaines valeurs. Il est ensuite possible de les modifier dans le but de tricher dans un jeu vidéo.
Le but de GameConqueror est de fournir un logiciel similaire à CheatEngine dans son utilisation et ses fonctions. Il est écrit en PyGTK.

