---
nom: "Gitlab"
date_creation: "Vendredi, 13 janvier, 2017 - 09:41"
date_modification: "Vendredi, 13 janvier, 2017 - 09:41"
logo:
    src: "images/logo/Gitlab.png"
site_web: "https://gitlab.com"
plateformes:
    - "GNU/Linux"
    - "Autre"
    - "Web"
langues:
    - "English"
description_courte: "Une forge logicielle extrêmement complète : forks, merge requests, tickets, intégration continue, communication avec d'autres services comme Mattermost…"
createurices: ""
alternative_a: "Github"
licences:
    - "Licence MIT/X11"
tags:
    - "git"
    - "forge logicielle"
lien_wikipedia: "https://fr.wikipedia.org/wiki/GitLab"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
---

Gitlab est une forge logicielle très complète et dont le développement est très dynamique : une nouvelle version sort tous les 22 du mois.
Gitlab est un logiciel en open core : il existe une version communautaire libre et une version entreprise propriétaire possédant plus de fonctionnalités.
Parmi les fonctionnalités les plus intéressantes de la version communautaire :
- possibilité de forker un dépôt depuis l'interface web
- possibilité de proposer une merge request
- système de tickets
- intégration continue avec Gitlab CI (mais fonctionne aussi avec d'autres logiciels comme Jenkins)
- possibilité de communication avec d'autres logiciels comme Mattermost (logiciel de communication instantanée)
- possibilité de créer des pages web pour chaque dépôt (à partir de la version 8.16)

