---
nom: "Glaxnimate"
date_creation: "Lundi, 3 octobre, 2022 - 19:42"
date_modification: "Lundi, 3 octobre, 2022 - 19:44"
logo:
    src: "images/logo/Glaxnimate.png"
site_web: "https://glaxnimate.mattbas.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Programme d'animation graphique vectoriel rapide et facile à utiliser."
createurices: "Mattia Basaglia"
alternative_a: "Adobe Animate CC, Flash (animation)"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "animation"
    - "vectoriel"
    - "dessin vectoriel"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Glaxnimate est un logiciel s'intéressant aux technologies récentes d'animation, notamment pour le web. Il est par exemple compatible avec les formats GIF, WebP, Lottie, SVG.
Il permet également d'aller plus loin en codant ses propres scripts en Python.
Quelques points clefs:
- Logique vectorielle
- Système de calques et groupes
- Interpolation d'image pour des animations plus fluide
- Interface fortement modifiable
Techniquement parlant, le logiciel est codé en C++ et utilise Qt5. On trouve pour Linux un AppImage et un paquet Debian.

