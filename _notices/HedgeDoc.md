---
nom: "HedgeDoc"
date_creation: "Dimanche, 22 mars, 2020 - 12:01"
date_modification: "Jeudi, 10 décembre, 2020 - 10:51"
logo:
    src: "images/logo/HedgeDoc.png"
site_web: "https://hedgedoc.org/"
plateformes:
    - "Autre"
    - "Web"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Un éditeur de texte collaboratif utilisant le markdown !"
createurices: ""
alternative_a: "HackMD"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "édition collaborative"
    - "traitement de texte"
    - "markdown"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
---

HedgeDoc (anciennement CodiMD) est un service web de traitement de texte collaboratif et en temps réel. Il utilise le langage Markdown, qui est un moyen simple pour formater un texte.
HedgeDoc propose un vaste choix de fonctionnalités pour toutes les utilisations courantes du traitement de texte :
Gestion des titres
Table de matières
Insertion d'images
Tableaux
Notes de bas de page
Incrustation de vidéos, de visualisateurs PDF, etc.
Différents niveaux de permission permettent de choisir qui peut lire ou éditer le document.

