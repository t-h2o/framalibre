---
nom: "Hugo"
date_creation: "Mercredi, 29 décembre, 2021 - 10:15"
date_modification: "Lundi, 17 janvier, 2022 - 14:03"
logo:
    src: "images/logo/Hugo.png"
site_web: "https://gohugo.io/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "Autres langues"
description_courte: "Hugo est un générateur de site Web HTML et CSS statique écrit en Go . Il est très performant."
createurices: "Steve Francia"
alternative_a: ""
licences:
    - "Licence Apache (Apache)"
tags:
    - "cms"
    - "création de site web"
    - "site web statique"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
---

Hugo est un générateur de site web statique, c'est à dire qu'il génère tous les fichiers HTML du site, et qu'un site web généré avec Hugo n'a ainsi pas besoin de base de données ou de langage de script type PHP. Écrit en Go, il est facile à installer et à lancer (il suffit d'un exécutable), et il est très rapide.
Hugo prend un répertoire avec du contenu et des modèles et les rend dans un site Web HTML complet.
Hugo s'appuie sur des fichiers Markdown avec un premier élément pour les métadonnées, et vous pouvez exécuter Hugo à partir de n'importe quel répertoire.
Cela fonctionne bien pour les hôtes partagés et autres systèmes où vous n'avez pas de compte privilégié.
Hugo génère un site Web typique de taille modérée en une fraction de seconde.
Une bonne règle de base est que chaque élément de contenu est rendu en environ 1 milliseconde.
Hugo est conçu pour bien fonctionner pour tout type de site Web, y compris les blogs.

