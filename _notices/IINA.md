---
nom: "IINA"
date_creation: "Dimanche, 13 novembre, 2022 - 12:57"
date_modification: "Vendredi, 13 janvier, 2023 - 14:22"
logo:
    src: "images/logo/IINA.png"
site_web: "https://iina.io"
plateformes:
    - "Mac OS X"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Le lecteur multimédia moderne pour macOS."
createurices: ""
alternative_a: "Quicktime"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "vidéo"
lien_wikipedia: "https://en.wikipedia.org/wiki/IINA"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

IINA est né pour être une application macOS moderne, de son framework à l'interface utilisateur. Il adopte le langage de conception post-Yosemite de macOS et suit le rythme des nouvelles technologies telles que Force Touch, Touch Bar et Picture-in-Picture.

