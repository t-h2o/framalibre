---
nom: "IraMuteq"
date_creation: "Samedi, 17 février, 2018 - 10:09"
date_modification: "Mercredi, 12 mai, 2021 - 14:55"
logo:
    src: "images/logo/IraMuteq.png"
site_web: "http://iramuteq.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Analyse Lexicale de textes"
createurices: "Pratinaud"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "intelligence artificielle"
    - "texte"
    - "analyse"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Iramuteq_(logiciel)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Interface de R pour les Analyses Multidimensionnelles de Textes et de Questionnaires. Un logiciel libre construit avec des logiciels libres.
Présentation
Iramuteq est un logiciel libre distribué sous les termes de la licence GNU GPL (v2).
Il permet de faire des analyses statistiques sur des corpus texte et sur des tableaux individus/caractères.
Il repose sur le logiciel R (www.r-project.org) et le langage python (www.python.org)

