---
nom: "JASP"
date_creation: "Mardi, 28 mars, 2023 - 11:45"
date_modification: "Mardi, 28 mars, 2023 - 12:16"
logo:
    src: "images/logo/JASP.png"
site_web: "https://jasp-stats.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Une nouvelle façon de faire des statistiques."
createurices: "JASP Team"
alternative_a: "SPSS"
licences:
    - "Licence Publique Générale Affero (AGPL)"
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "science"
    - "statistiques"
lien_wikipedia: "https://en.wikipedia.org/wiki/JASP"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

JASP est un logiciel multiplateforme doté d'une interface graphique performante. JASP permet d'effectuer des analyses statistiques en quelques secondes, sans avoir à apprendre la programmation ni risquer une erreur de programmation.
JASP propose des procédures d'analyses statistiques sous une forme classique (fréquentielle) et bayésienne.
Le principe de conception de base dans JASP est que les paramètres par défaut présentent des analyses simples, avec des options plus complexes disponibles en cas de besoin. Contrairement à la plupart des autres programmes de statistiques, JASP ne tente pas de « choquer et d'intimider » les praticiens en leur présentant un assortiment d'analyses exotiques et une multitude de résultats non structurés.

