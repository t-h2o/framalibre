---
nom: "Jellyfin"
date_creation: "Jeudi, 23 janvier, 2020 - 20:51"
date_modification: "Dimanche, 18 septembre, 2022 - 21:04"
logo:
    src: "images/logo/Jellyfin.png"
site_web: "https://jellyfin.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Jellyfin est un serveur multimedia.
ll permet de mettre sa médiathèque à disposition sur le web."
createurices: "Andrew Rabert, Joshua Boniface, Vasily, Anthony Lavado"
alternative_a: "Plex, Emby"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bibliothèque musicale"
    - "séries"
    - "navigateur web"
    - "multimédia"
    - "lecteur multimédia"
    - "linux"
    - "windows"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Jellyfin est un serveur multimedia. Il s'agit d'un fork de Emby (anciennement Media Browser) devenu officiellement propriétaire en 2018.
Il permet de mettre sa médiathèque à disposition sur le web, qu'il s'agisse de contenu vidéo (films et séries, télévision), audio, ou d'images.
Il permet de récupérer automatiquement les informations concernant les films et les séries depuis IMDb et TheMovieDb : descriptions, affiches, réalisateurs, acteurs, etc.
Ceci permet de naviguer entre les différents réalisateurs, ou les différents genres, de trier par date, de voir tous les films avec un acteur particulier, sa photo et sa biographie etc.
De base avec Jellyfin cette médiathèque est donc accessible et visionnable avec n'importe quel navigateur web, mais il existe de plus de nombreux clients qui permettent de simplifier son utilisation en fonction de l'appareil qu'on utilise.
On peut aussi créer des comptes supplémentaires pour partager ce contenu avec ses amis.

