---
nom: "Jitsi"
date_creation: "Mercredi, 4 janvier, 2017 - 04:38"
date_modification: "Mercredi, 12 mai, 2021 - 16:19"
logo:
    src: "images/logo/Jitsi.png"
site_web: "https://jitsi.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Web"
langues:
    - "Autres langues"
description_courte: "Visio-conférence, messagerie instantanée et plus à essayer sur meet.jit.si."
createurices: "Université de Strasbourg"
alternative_a: "Skype, Google Hangouts"
licences:
    - "Licence Apache (Apache)"
tags:
    - "voip"
    - "visioconférence"
    - "téléphonie sur ip"
    - "messagerie instantanée"
    - "communication chiffrée"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Jitsi"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
---

Excellente alternative à Skype. Jitsi permet de passer des appels audio et vidéo, avec une ou plusieurs personnes, d'enregistrer les appels, de partager son écran, de discuter par messagerie instantanée, de chiffrer ses communications, de transférer des fichiers, … on peut se créer une adresse SIP ou utiliser son adresse Google talk, Facebook, AIM, Yahoo Messenger, et d'autres.
On peut installer Jitsi sur son ordinateur ou l'utiliser en ligne, sans inscriptions, sur https://meet.jit.si/.
Bien que l'application web ne fonctionne pas sous mobile ou tablette, il existe une application sur Android.

