---
nom: "K3b"
date_creation: "Mercredi, 20 septembre, 2017 - 16:05"
date_modification: "Mardi, 11 mai, 2021 - 23:13"
logo:
    src: "images/logo/K3b.png"
site_web: "https://userbase.kde.org/K3b"
plateformes:
    - "GNU/Linux"
    - "BSD"
langues:
    - "Français"
    - "Autres langues"
description_courte: "K3b permet de graver des CDs, DVDs, images ISO, de ripper des DVDs, etc."
createurices: ""
alternative_a: "Nero"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "gravure"
    - "cd-rom"
    - "dvd"
lien_wikipedia: "https://fr.wikipedia.org/wiki/K3b"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

K3b propose sous une interface facile à utiliser (l'ajout de fichiers se fait par glisser-déposer) beaucoup de possibilités de gravure. Nous dirions  que c'est ce logiciel qui en présente le plus sous GNU/Linux.
Nous pouvons graver des CDs, des DVDs (y compris Blue-Rays), pour de la musique ou toute sorte de données; des images .ISO, et nous pouvons aussi ripper des DVDs (importer leur contenu).
Il est présent dans toutes les distributions, donc il est dans votre logithèque.

