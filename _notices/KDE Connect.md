---
nom: "KDE Connect"
date_creation: "Mercredi, 29 décembre, 2021 - 10:42"
date_modification: "Mercredi, 29 décembre, 2021 - 11:34"
logo:
    src: "images/logo/KDE Connect.png"
site_web: "https://kdeconnect.kde.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Apple iOS"
    - "Autre"
langues:
    - "Autres langues"
description_courte: "Faire communiquer ses périphériques sur un même réseau"
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
lien_wikipedia: "https://en.wikipedia.org/wiki/KDE_Connect"
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/org.kde.kdeconnect_tp/latest/"
identifiant_wikidata: ""
mis_en_avant: "oui"
---

KDE Connect facilite l'échange d'information entre périphériques connectés à un même réseau LAN. Il permet, entre autre, de partager des fichiers ou de partager le presse-papier (clipboard). Mais là où il se révèle le plus puissant, c'est dans l'intégration entre un smartphone et un ordinateur : il est ainsi possible de prendre le contrôle du second par le premier (par exemple, pour contrôler un diaporama en se servant du smartphone comme télécommande), d'envoyer des SMS depuis son ordinateur, ou encore d'interrompre automatiquement la lecture d'un média sur ordinateur, lorsque le smartphone reçoit un appel.
KDE Connect est disponible sur un grand nombre de plateformes.

