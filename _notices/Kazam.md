---
nom: "Kazam"
date_creation: "Jeudi, 20 avril, 2017 - 23:13"
date_modification: "Mardi, 9 mai, 2017 - 16:11"
logo:
    src: "images/logo/Kazam.png"
site_web: "https://launchpad.net/kazam"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Kazam Screencaster permet de filmer son écran tout en enregistrant le son (voix. musique…)"
createurices: "David Klasinc"
alternative_a: "Xsplit"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "screencast"
    - "capture d'écran"
    - "vidéos"
    - "vidéographie"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Très bon petit logiciel qui fait très bien son travail d'enregistrement vidéo d'un tutoriel commenté par exemple. La prise en main est très rapide ; logiciel efficient.
Personnellement je l'utilise en production pour concevoir des vidéos de quelques minutes pour mes élèves de lycée.

