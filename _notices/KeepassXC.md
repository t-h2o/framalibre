---
nom: "KeepassXC"
date_creation: "Lundi, 27 novembre, 2017 - 10:48"
date_modification: "Lundi, 10 mai, 2021 - 13:39"
logo:
    src: "images/logo/KeepassXC.png"
site_web: "https://keepassxc.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "KeePassXC est un fork communautaire de KeePassX. Ce logiciel dispose de fonctionnalités supplémentaires."
createurices: "KeePassXC Team"
alternative_a: "1password"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "sécurité"
    - "gestionnaire de mots de passe"
    - "chiffrement"
    - "mot de passe"
    - "authentification"
lien_wikipedia: "https://en.wikipedia.org/wiki/KeePassXC"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
---

KeePassXC est un fork communautaire de KeePassX faisant suite au développement très lent de KeePassX (la dernière version datant d'octobre 2016) et à l'absence de réponse de son mainteneur quant à une possible mise à jour.
KeePassXC est construit à partir de la version 5.2 des bibliothèques Qt, ce qui en fait une application multi-plateforme, qui peut être exécutée sous Linux, Windows et macOS.
Il utilise le format de base de données de mot de passe KeePass 2 (.kdbx) comme format natif. Il peut également importer (et convertir) les anciennes bases de données KeePass 1 (.kdb)
Le support natif des Yubikey est également intégré au logiciel.

