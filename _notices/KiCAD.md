---
nom: "KiCAD"
date_creation: "Samedi, 5 décembre, 2020 - 11:44"
date_modification: "Lundi, 13 mars, 2023 - 10:26"
logo:
    src: "images/logo/KiCAD.png"
site_web: "https://kicad.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "The référence pour concevoir une carte électronique librement."
createurices: "Jean-Pierre Charras"
alternative_a: "Eagle, Orcad, Zuken cadstar, Altium Designer"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "créativité"
    - "cao"
lien_wikipedia: "https://fr.wikipedia.org/wiki/KiCad"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Suite complète de logiciels pour concevoir une carte électronique : schéma électronique, dessin des composants, empreinte associée, routage du circuit, génération des fichiers de fabrication, visualisation du fichier Gerber, visualisation en 3D, possibilité de transfert vers FreeCAD, quelques outils de calcul pour la conception.
Ce projet, initialement porté par Jean-Pierre Charras, chercheur au CERN, a pris de l’ampleur grâce à un soutien important du CERN (institut public de recherche). Il prend maintenant son envol avec un soutien communautaire très important. De nombreux projets électroniques ont été conçus grâce à ce logiciel.

