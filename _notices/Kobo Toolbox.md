---
nom: "Kobo Toolbox"
date_creation: "Lundi, 13 juin, 2022 - 10:32"
date_modification: "Lundi, 13 juin, 2022 - 14:59"
logo:
    src: "images/logo/Kobo Toolbox.png"
site_web: "https://www.kobotoolbox.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Apple iOS"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Collecte de données et sondage tous terrains"
createurices: "Harvard Humanitarian Initiative, Phuong Pham, Patrick Vinck"
alternative_a: "Google Forms"
licences:
    - "Berkeley Software Distribution License (BSD)"
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "métiers"
    - "sondage"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

A l’origine, Kobo Toolbox est un projet pour les associations humanitaires qui souhaitent faire des sondages sur le terrain.
Elle permet de concevoir des formulaires de collecte de données (sondages) qui pourront être remplis sur le terrain ou directement envoyées à un répondant. En plus de sa version web, l'application propose une progressive webapp et une application Android très légère. Elles permettent des saisies en étant connectées à internet ou non.
Les types de questions disponibles est nombreux. En plus des traditionnels champs texte, numérique, listes de choix ... on peut également prendre des photos, les annoter, prendre des positions GPS, lire des QR codes ...
Cet outil s'appuie sur le framework Open Data Kit.

