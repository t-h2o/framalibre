---
nom: "Krita"
date_creation: "Jeudi, 29 décembre, 2016 - 16:50"
date_modification: "Lundi, 10 mai, 2021 - 13:50"
logo:
    src: "images/logo/Krita.png"
site_web: "https://krita.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Logiciel de création de dessins, peintures, logos et animations"
createurices: ""
alternative_a: "Adobe Photoshop, Adobe Illustrator, clip studio paint, Manga studio"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "peinture"
    - "animation"
    - "dessin"
    - "création multimédia"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Krita"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
---

Krita est un logiciel de traitement d'image point à point. Il s'adresse au professionnel du graphisme, de l'illustration de la bande dessinée et de la création de texture.
Krita s'appuie sur des modules développés au sein de Calligra, suite bureautique développée initialement pour environnement de bureau KDE (anciennement appelée Koffice).
Krita dispose de fonctionnalités avancées comme les modèles de couleurs Lab* et CMJN, différentes profondeurs de couleur (8,16,32 bits), calques, masques, filtres.... ainsi que la possibilité native de créer des animations.

