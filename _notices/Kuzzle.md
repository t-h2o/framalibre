---
nom: "Kuzzle"
date_creation: "Mercredi, 17 octobre, 2018 - 09:46"
date_modification: "Mercredi, 12 mai, 2021 - 15:20"
logo:
    src: "images/logo/Kuzzle.jpg"
site_web: "https://kuzzle.io/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
langues:
    - "English"
description_courte: "Kuzzle est un backend entièrement personnalisable, open-source et libre offrant des fonctionnalités avancées."
createurices: "The Kuzzle Team"
alternative_a: "Firebase, Couchbase, PubNub, Pusher, Backendless, Back4App"
licences:
    - "Licence Apache (Apache)"
tags:
    - "développement"
    - "backend"
    - "temps réel"
    - "iot"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Kuzzle est un backend clé en main utilisé pour accélérer le développement d'application web, mobile et IoT.
Il propose toutes les fonctionnalités nécessaires pour développer une application en se concentrant sur le développement frontend plutôt que backend.
Kuzzle est installable rapidement sur laptop, dans un cloud ou on premise grâce à nos images Docker.
Kuzzle fournit
 - une API multi-protocol (HTTP, Websocket, MQTT) pour créer, modifier et rechercher vos données
 - une couche complète de sécurité avec authentification et gestion des permissions
 - un système de plugin Node.js pour ajouter de la logique métier spécifique
 - des notifications temps réel en pub/sub
 - des fonctionnalités avancées tel que le geofencing
Kuzzle est également un écosystème comprenant de nombreux outils comme la console d'administration web,  plugin cluster pour le déploiement en production ainsi que des SDKs avec support du mode offline dans 11 langages différents.

