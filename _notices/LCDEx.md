---
nom: "LCDEx"
date_creation: "Vendredi, 14 juin, 2019 - 07:47"
date_modification: "Dimanche, 21 juillet, 2019 - 18:34"
logo:
    src: "images/logo/LCDEx.gif"
site_web: "https://cdex.mu/download"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Convertissez rapidement vos CD audio."
createurices: "cowo, philsf, roxo"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "cd ripper"
    - "logiciel audio"
lien_wikipedia: "https://fr.wikipedia.org/wiki/CDex"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

LCDex sous GNU Linux (CDEx sous Windows) est un logiciel libre permettant d'encoder les CD audio dans divers formats de compression comme le MP3, le WMA ou leur équivalent libre : le OGG ou le FLAC.
Il est également possible d'enregistrer via une source extérieure analogique connectée à la carte son (un microphone, un lecteur miniDisc, etc.) dans n’importe lequel des formats supportés.
Le logiciel dispose aussi de fonctionnalités telles que la reconnaissance automatique des fichiers sons (détermination du titre, de l'auteur, du style, etc. par comparaison aux bases de données connues via Internet).

