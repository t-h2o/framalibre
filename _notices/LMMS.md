---
nom: "LMMS"
date_creation: "Jeudi, 29 décembre, 2016 - 16:20"
date_modification: "Mardi, 1 septembre, 2020 - 11:30"
logo:
    src: "images/logo/LMMS.png"
site_web: "https://lmms.io/"
plateformes:
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "LInux MultiMedia Studio, le logiciel de création audio libre."
createurices: ""
alternative_a: "FL Studio, Garage Band, Cubase"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "éditeur audio"
    - "musique"
    - "son"
    - "création musicale"
    - "mao"
lien_wikipedia: "https://fr.wikipedia.org/wiki/LMMS"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Linux Multi Media Studio, ou LMMS, est un séquenceur, synthétiseur et composeur.
Le logiciel LMMS est utilisé dans le cadre de la création de musique assistée par ordinateur (MAO). Il est essentiellement constitué d'une table de montage séquentiel ("éditeur de morceau") correspondant à la chronologie du projet. L'utilisateur place sur cette table les différentes pistes dont il a besoin : des pistes instrumentales, des pistes de rythmes, et des pistes d'automation.
Le logiciel dispose pour les pistes instrumentales d'un piano virtuel semblable au "piano roll" de FL Studio, permettant de créer des mélodies, tandis que les pistes de rythmes permettent d'accompagner les mélodies par des percussions.
LMMS fournit des modules de synthétiseurs, d'autres plug-in ainsi que des samples.

