---
nom: "LUMI"
date_creation: "Mercredi, 28 juillet, 2021 - 14:44"
date_modification: "Mercredi, 28 juillet, 2021 - 17:35"
logo:
    src: "images/logo/LUMI.png"
site_web: "https://lumi.education/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Lumi permet de créer du contenu interactif avec des dizaines de types de contenu différents."
createurices: ""
alternative_a: "Anki, eXeLearning"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "éducation"
    - "exerciseur"
    - "contenu interactif"
    - "h5p"
    - "présentation"
    - "qcm"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Lumi est une application de bureau qui vous permet de créer, modifier, afficher et partager du contenu interactif avec des dizaines de types de contenu différents. C'est gratuit et open source.
La liste des contenus que l'on peut créer en html5 est tout simplement éblouissante : il y en a pour tous les goûts et tous les besoins : frise chronologique, flashcards, diaporama commentés, vidéo augmentée...
Tous est importable simplement dans Moodle ou dans Scenari (Opale par exemple) sous forme de fichier HTML5.
L'outil LUMI est similaire à Logiquiz de ladigitale.dev

