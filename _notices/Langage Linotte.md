---
nom: "Langage Linotte"
date_creation: "Vendredi, 20 décembre, 2019 - 09:49"
date_modification: "Mercredi, 9 mars, 2022 - 16:35"
logo:
    src: "images/logo/Langage Linotte.png"
site_web: "http://langagelinotte.free.fr/wordpress/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
langues:
    - "Français"
description_courte: "Le langage de programmation entièrement en français, simple, puissant."
createurices: "Ronan Mounès"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "développement"
    - "programmation"
    - "java"
    - "python"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Linotte_%28langage%29"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Linotte est un langage de programmation interprété de type L4G. Sa particularité est sa syntaxe en français.
Ce langage est libre et a été créé dans le but de permettre aux enfants et aux personnes n’ayant pas une connaissance approfondie de l’informatique d’apprendre la programmation facilement. Comme sa devise l’indique : « Tu sais lire un livre, alors tu peux écrire un programme informatique », ce langage se veut très simple ; une variante de la devise dit aussi « tu sais écrire une phrase, donc tu sais écrire un programme ». Linotte a la particularité d’utiliser un vocabulaire non technique plutôt proche de termes utilisés, soit dans le monde cinématographique, soit dans la littérature.
Un programme devient un livre, une méthode, un paragraphe, une variable, un acteur et l’écran, une toile. On n’exécute pas un livre, mais on le lit.
Linotte n’introduit pas de nouveaux concepts mais les renomme tout simplement.

