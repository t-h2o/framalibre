---
nom: "Leaflet"
date_creation: "Jeudi, 29 juin, 2017 - 17:49"
date_modification: "Mercredi, 12 mai, 2021 - 16:02"
logo:
    src: "images/logo/Leaflet.png"
site_web: "http://leafletjs.com/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
langues:
    - "English"
description_courte: "Librairie Javascript pour générer des cartes interactive et dessiner dessus, ajouter des marqueurs..."
createurices: "Vladimir Agafonkin"
alternative_a: "Google Maps"
licences:
    - "Berkeley Software Distribution License (BSD)"
tags:
    - "internet"
    - "carte géographique"
    - "cartographie"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Leaflet"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Leaflet est une librarie Javascript qui permet d'abord d'intégrer facilement une carte interactive sur laquelle on pourra zoomer ou explorer.
La librairie nous laisse le choix dans le service qui permet d'afficher les tuiles (bouts de carte), nous permettant donc d'utiliser les tuiles du service openstreetmap, mapbox... On peut donc choisir le thème de la carte.
Dans un second temps, la librairie nous permet d'ajouter des formes sur la carte, des images comme des marqueurs et une infobulle html...
Enfin, elle est conçue pour pouvoir facilement être étendue par des greffons, ajoutant des fonctionnalités comme par exemple affichée une image distordue pour être calquée au plan des rues, récupérer d'autres informations géographique comme l'altitude, ajouter d'autres contrôles utilisateur-interface.

