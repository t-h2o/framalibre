---
nom: "Lemmy"
date_creation: "Jeudi, 6 janvier, 2022 - 11:23"
date_modification: "Jeudi, 6 janvier, 2022 - 14:41"
logo:
    src: "images/logo/Lemmy.jpeg"
site_web: "https://join-lemmy.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Web"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Suivez des communautés partout dans le monde. Un agrégateur de liens pour le fediverse."
createurices: ""
alternative_a: "reddit, Lobste.rs, Hacker News"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "microblog"
    - "web"
    - "fediverse"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Lemmy est similaire à des sites comme Reddit, Lobste.rs, ou Hacker News : vous vous abonnez aux communautés qui vous intéressent, vous postez des liens et des discussions, puis vous votez et commentez.
Lemmy n'est pas seulement une alternative à reddit ; c'est un réseau de communautés interconnectées gérées par différentes personnes et organisations, qui se combinent toutes pour créer une seule et unique page d'accueil personnalisée de vos nouvelles, articles et mèmes préférés.
Lorsqu'on installe son propre serveur de Lemmy, on peut tout de même se connecter et interagir avec les autres serveur: c'est une propriété du Fediverse.

