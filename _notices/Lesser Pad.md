---
nom: "Lesser Pad"
date_creation: "Jeudi, 23 février, 2017 - 16:17"
date_modification: "Mercredi, 12 mai, 2021 - 15:51"
logo:
    src: "images/logo/Lesser Pad.png"
site_web: "http://kodakana.b.osdn.me/"
plateformes:
    - "Android"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Application de prise de notes pour Android"
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "prise de notes"
    - "notes"
    - "texte"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Lesser Pad est une application de prise de notes pour Android, très simple d'utilisation.
Les notes peuvent être organisées en catégories et protégées par un mot de passe.
Les fichiers sont directement enregistrés dans le téléphone au format .txt pour les notes simples et .len pour les notes protégées.

