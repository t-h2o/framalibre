---
nom: "Lessy"
date_creation: "Mercredi, 24 janvier, 2018 - 15:55"
date_modification: "Vendredi, 17 juin, 2022 - 10:20"
logo:
    src: "images/logo/Lessy.png"
site_web: "https://lessy.io/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
    - "Web"
langues:
    - "Français"
    - "English"
description_courte: "Un gestionnaire du temps respectueux et éthique."
createurices: "Marien Fressinaud"
alternative_a: "Microsoft Project, trello"
licences:
    - "Licence MIT/X11"
tags:
    - "gestion du temps"
    - "gestion de projet"
    - "tâche"
    - "temps"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Lessy est un gestionnaire du temps qui a pour objectif de vous aider dans votre organisation personnelle en réduisant votre stress et votre charge cognitive. Votre temps sera mieux géré et mieux utilisé.
Lessy vous permet de créer des tâches à effectuer. Chaque tâche peut ensuite être autonome ou bien affectée à un projet.
Lessy vous permet d'obtenir une version claire de l'ensemble des tâches ainsi que de l'avancement des différents projets. Il vous aide à vous fixer des objectifs en préparant votre liste de tâches pour la journée.
Pour l'utiliser, vous pouvez vous créer un compte sur lessy.io ou bien installer votre propre instance (Docker et Nginx pour Debian 9 recommandés).

