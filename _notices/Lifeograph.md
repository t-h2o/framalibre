---
nom: "Lifeograph"
date_creation: "Samedi, 22 avril, 2017 - 18:45"
date_modification: "Samedi, 22 avril, 2017 - 18:50"
logo:
    src: "images/logo/Lifeograph.png"
site_web: "http://lifeograph.sourceforge.net"
plateformes:
    - "GNU/Linux"
    - "Windows"
    - "Android"
langues:
    - "Autres langues"
description_courte: "Une application de prise de notes journalisées et encryptables."
createurices: "Ahmet Öztürk"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "prise de notes"
    - "journal"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Lifeograph est une application de prise de notes à la fois très simple et très complète distribuée sous Linux, Android et Windows — même si cette dernière version est assez décevante.
La saisie se fait à la volée avec un formatage type MarkDown et enregistrement automatique, ce qui permet de ne jamais perdre ses notes. Chaque journal peut être organisé par date ou par chapitre ou thème. Il existe un système de tags très bien fait et l'interface utilisateur est assez personnalisable.
Lifeograph est livré avec deux journaux intégrés (en anglais) : le manuel et un journal d'exemple.
On peut créer autant de journaux que nécessaire et protéger l'accès de chaque journal indépendamment des autres en en chiffrant les données.

