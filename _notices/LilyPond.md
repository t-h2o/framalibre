---
nom: "LilyPond"
date_creation: "Samedi, 31 décembre, 2016 - 18:19"
date_modification: "Samedi, 31 décembre, 2016 - 19:19"
logo:
    src: "images/logo/LilyPond.png"
site_web: "http://lilypond.org/index.fr.html"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Lilypond crée des partitions de musique de qualité professionnelle à partir d'un simple fichier texte."
createurices: "Han-Wen Nienhuys, Jan Nieuwenhuizen"
alternative_a: "Encore, Sibelius, Finale"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "partition de musique"
    - "musique"
    - "gravure musicale"
    - "éditeur de partition"
lien_wikipedia: "https://fr.wikipedia.org/wiki/LilyPond"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Liliypond est un compilateur qui génère des partitions de musique de qualité professionnelle à partir d'un simple fichier texte. En cela, différent d'un éditeur de partition avec une interface graphique, il est plus proche d'un langage de programmation et nécessite un apprentissage différent. Par exemple, chaque note de la gamme est traduite par une lettre et sa durée par un nombre. L'utilisation de Lilypond est donc soumise à l'usage d'un éditeur de texte, ce qui lui confère une interopérabilité certaine sur des systèmes informatiques différents et une grande souplesse relative à la simplicité du fichier texte. Certains outils existent pour faciliter son utilisation dans des logiciels très répandus, comme par exemple un document LibreOffice, ou une page de Mediawiki (Wikipédia).

