---
nom: "Linux Show Player"
date_creation: "Mardi, 19 février, 2019 - 16:40"
date_modification: "Samedi, 22 janvier, 2022 - 15:34"
logo:
    src: "images/logo/Linux Show Player.png"
site_web: "https://linux-show-player.org/"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
description_courte: "Linux Show Player permet de déclencher des bandes-son pour du spectacle vivant."
createurices: "Francesco Ceruti"
alternative_a: "QLab, Ableton Live, Cubase, Multiplay"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "vidéo"
    - "audio"
    - "réseau"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Linux Show Player (ou LiSP) est un logiciel libre de « toppage » (top, ou cue) de bandes-son pour du spectacle vivant (notamment le théâtre).
Il permet de créer une liste ou des évènements à démarrer de manière interactive (clavier d'ordi, automatique, écran tactile, contrôleur MIDI, réseau...).
On peut créer des événements MIDI (il peut envoyer des commandes MIDI).
Il comporte de quoi éditer les bandes sons, ainsi que rajouter des effets (normalisation, pitch, vitesse, compression...).

