---
nom: "Lollypop"
date_creation: "Jeudi, 20 juillet, 2017 - 18:43"
date_modification: "Samedi, 4 avril, 2020 - 00:20"
logo:
    src: "images/logo/Lollypop.png"
site_web: "https://wiki.gnome.org/Apps/Lollypop"
plateformes:
    - "GNU/Linux"
    - "BSD"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Un lecteur de musique moderne qui sort de l'ordinaire."
createurices: "Cédric Bellegarde"
alternative_a: "Banshee, Rhythmbox"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "lecteur musique"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Lollypop est un lecteur de musique créé pour Gnome, et qui ne ressemble pas vraiment à aucun autre lecteur ce qui fait son charme,
Ce lecteur est tout jeune mais très prometteur il gère déjà le téléchargement des pochettes d’albums et les photos d’artistes, la navigation par Artiste - Album ou Genre,
On peut aussi obtenir des informations sur les artistes ou les morceaux via Last FM et Wikipedia, support du “Replay Gain”, la recherche dans la bibliothèque, liste d’attente, recherche de paroles, supports de nombreux formats audio, et synchronise la musique avec Android ou n’importe quel appareil MTP,
Affichage plein écran et un mode soirée pour laisser Lollypop choisir la musique pour vous,

