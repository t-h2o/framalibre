---
nom: "Lubuntu"
date_creation: "Mardi, 7 janvier, 2020 - 14:54"
date_modification: "Jeudi, 20 mai, 2021 - 16:27"
logo:
    src: "images/logo/Lubuntu.png"
site_web: "https://lubuntu.fr/"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Lubuntu est un système d'exploitation basé sur Ubuntu, mais pensé pour les ordinateurs très peu puissants."
createurices: ""
alternative_a: "Microsoft Windows"
licences:
    - "Multiples licences"
tags:
    - "système"
    - "distribution gnu/linux"
    - "système d'exploitation (os)"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Lubuntu"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
---

Lubuntu est un système d'exploitation libre dérivé d'Ubuntu. Il bénéficie des mêmes qualités en termes de facilité d'installation et d'utilisation.
Lubuntu utilise l'environnement de bureau LXQT qui est très léger, ce qui peut permettre de faire revivre de très vieux ordinateurs.

