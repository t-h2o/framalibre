---
nom: "Lufi"
date_creation: "Jeudi, 5 janvier, 2017 - 10:58"
date_modification: "Vendredi, 7 mai, 2021 - 11:19"
logo:
    src: "images/logo/Lufi.png"
site_web: "https://demo.lufi.io"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Web"
langues:
    - "Autres langues"
description_courte: "Transférez vos fichiers en toute confidentialité."
createurices: "Luc Didry"
alternative_a: "WeTransfer"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "partage"
    - "partage de fichiers"
    - "chiffrement"
    - "anonymat"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
---

Vous avez un fichier à envoyer à quelqu'un, mais il est trop volumineux pour passer par mail ? Vous souhaitez vous assurer que ce fichier reste bien confidentiel ?
Lufi permet le dépôt et la récupération de fichiers en toute confidentialité grâce au chiffrement du fichier effectué côté navigateur : le serveur ne reçoit que des données chiffrées, et ne voit jamais la clé de chiffrement.
Il est possible de choisir une durée de rétention des fichiers : ils peuvent ainsi s'autodétruire après la première visite, au bout d'un jour, d'une semaine, d'un mois, d'un an ou jamais (les choix peuvent varier selon la configuration). La durée de rétention peut aussi varier selon le poids du fichier et la configuration du serveur.
Attention ! Comme c'est anonyme, on ne peut pas retrouver un fichier dont on a perdu l'adresse.

