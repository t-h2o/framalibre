---
nom: "LÖVE"
date_creation: "Dimanche, 26 décembre, 2021 - 01:06"
date_modification: "Dimanche, 26 décembre, 2021 - 15:00"
logo:
    src: "images/logo/LÖVE.png"
site_web: "https://love2d.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Apple iOS"
langues:
    - "Autres langues"
description_courte: "Moteur de jeu programmable en Lua"
createurices: ""
alternative_a: ""
licences:
    - "Licence Zlib"
tags:
    - "jeu"
    - "moteur de jeu"
lien_wikipedia: "https://fr.wikipedia.org/wiki/L%C3%96VE"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

LÖVE ou LÖVE2D est un moteur de jeu orienté 2D libre et multiplateforme, programmable en Lua. Le résultat peut être archivé sous forme de zip ou sous forme de binaire pour la majorité des systèmes de bureau et mobiles. L'API est simple à prendre en main mais très complète et prend tous les aspects multimédias et principales interfaces d'entrées. Le moteur utilise l'accélération matérielle et permet de programmer les shaders.

