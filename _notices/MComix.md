---
nom: "MComix"
date_creation: "Lundi, 30 octobre, 2017 - 19:28"
date_modification: "Mercredi, 12 mai, 2021 - 16:39"
logo:
    src: "images/logo/MComix.png"
site_web: "https://sourceforge.net/projects/mcomix/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Très bon lecteur de bande dessiné ouvrant les dossiers d’image, CBR, CBZ et TAR."
createurices: "Pontus Ekberg, Moritz Brunner, Ark, Benoit Pierre"
alternative_a: "CDisplayEx"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "comics"
    - "cbr"
    - "cbz"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Mcomix est un lecteur de bande dessiné, il ouvre les images, les archives zip (.cbz), rar (.cbr) et tar.
Il permet l’affichage sous forme de double page, d’inverser le sens de lecture avec son mode manga et divers options pour ajuster l’affichage.
On peut utiliser un affichage sous forme de vignettes pour naviguer dans l’œuvre.
Il comprend aussi un gestionnaire de bibliothèque pour visualisé sa collection.

