---
nom: "Maccy"
date_creation: "Dimanche, 13 novembre, 2022 - 12:09"
date_modification: "Vendredi, 13 janvier, 2023 - 14:22"
logo:
    src: "images/logo/Maccy.png"
site_web: "https://maccy.app"
plateformes:
    - "Mac OS X"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Gestionnaire de presse-papiers pour macOS"
createurices: "Alex Rodionov"
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "bureautique"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Maccy est un gestionnaire de presse-papiers léger pour macOS. Il conserve l'historique de ce que vous copiez et vous permet de naviguer, de rechercher et d'utiliser rapidement le contenu précédent du presse-papiers.
Maccy fonctionne sur macOS Mojave 10.14 ou supérieur.

