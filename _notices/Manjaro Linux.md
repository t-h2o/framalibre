---
nom: "Manjaro Linux"
date_creation: "Jeudi, 29 décembre, 2016 - 17:08"
date_modification: "Lundi, 10 mai, 2021 - 14:06"
logo:
    src: "images/logo/Manjaro Linux.png"
site_web: "https://manjaro.org/"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "La distribution linux pour les utilisateurs qui ne veulent pas se prendre la tête."
createurices: ""
alternative_a: "Microsoft Windows, MacOS"
licences:
    - "Multiples licences"
tags:
    - "système"
    - "distribution gnu/linux"
    - "système d'exploitation (os)"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Manjaro_Linux"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Manjaro Linux est une distribution GNU/Linux qui combine l'excellence technique d'Arch Linux avec un objectif de facilité d'utilisation et de prise en main.
Pas aussi bien documentée qu'Arch Linux et son wiki légendaire, la plupart de cette documentation reste valable pour Manjaro. Pas totalement libre car elle peut faire appel à des pilotes et logiciels propriétaires si nécéssaire, utilisant le modèle "Rolling Release" Manjaro propose assez rapidement les versions les plus récentes des divers logiciels, avec toutefois un léger décalage par rapport à Arch ce qui lui permets de ne pas pour autant sacrifier la stabilité.
Il y a 3 variantes stables officielles : une version peu gourmande en ressource avec XFCE, une version plus complète et nécéssitant plus de ressources avec KDE ainsi qu'une version minimale sans environnement de bureau. Il a 7 variantes stables communautaires avec différents environnement de bureau: budgie, cinnamon, deepin, i3, lxqt, gnome et mate.

