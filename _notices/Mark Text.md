---
nom: "Mark Text"
date_creation: "Jeudi, 7 novembre, 2019 - 18:16"
date_modification: "Jeudi, 5 janvier, 2023 - 16:40"
logo:
    src: "images/logo/Mark Text.png"
site_web: "https://github.com/marktext/marktext"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Éditeur Markdown simple et élégant. Axé sur la rapidité et la convivialité."
createurices: "Ran Luo"
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "bureautique"
    - "markdown"
    - "éditeur"
    - "latex"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Créer par Ran Luo avec le framework Electron. Mark Text est un éditeur multiplateforme pour le format Markdown proposant une prévisualisation en temps réel.
Simple et élégant, il met à disposition plusieurs thèmes d'affichages (3 Light thèmes et 3 Dark thèmes) pour le confort des yeux.

