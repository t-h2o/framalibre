---
nom: "Markor"
date_creation: "Samedi, 14 octobre, 2017 - 21:44"
date_modification: "Jeudi, 10 décembre, 2020 - 11:11"
logo:
    src: "images/logo/Markor.png"
site_web: "https://gsantner.net/project/markor.html"
plateformes:
    - "Android"
langues:
    - "Autres langues"
description_courte: "Markor est un éditeur de texte markdown très complet"
createurices: "Gregor Santner"
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "bureautique"
    - "markdown"
    - "traitement de texte"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Markor permet de lister l'ensemble des fichiers markdown présents dans les répertoires souhaités. On peut effectuer des recherches sur leurs contenus, les visualiser et surtout les modifier.
Idéal pour de la prise de note simple.

