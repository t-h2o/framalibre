---
nom: "Mattermost"
date_creation: "Jeudi, 30 août, 2018 - 10:14"
date_modification: "Vendredi, 12 avril, 2019 - 13:17"
logo:
    src: "images/logo/Mattermost.png"
site_web: "https://www.mattermost.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Apple iOS"
    - "Web"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Mattermost est un logiciel décentralisé de communication en équipe."
createurices: ""
alternative_a: "Slack"
licences:
    - "Licence MIT/X11"
tags:
    - "communication"
    - "chat"
    - "irc"
    - "entreprise"
    - "travail collaboratif"
    - "markdown"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/search/com.mattermost.rn/"
identifiant_wikidata: ""
mis_en_avant: "oui"
---

Mattermost est un  logiciel décentralisé de communication en équipe. Pour les adeptes de Slack, l'importation de vos données est possible, permettant de faire la transition vers cet outil libre en douceur.
La plate-forme permet :
- Le partage de messages et de fichiers sur différents appareils : PC, ordiphones et tablettes grâce aux applications dédiées.
- La mise en forme des messages se fait avec le langage Markdown.
- L'archivage continu et la recherche instantanée, et la prise en charge les notifications et les intégrations avec vos outils existants.

