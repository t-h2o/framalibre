---
nom: "Mednafen"
date_creation: "Mercredi, 15 mars, 2017 - 13:16"
date_modification: "Mercredi, 15 mars, 2017 - 13:16"
logo:
    src: "images/logo/Mednafen.png"
site_web: "https://mednafen.github.io/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "Autres langues"
description_courte: "Mednafen est un émulateur multi-système capable d'émuler une quinzaine de consoles de jeux vidéos."
createurices: "L'équipe Mednafen"
alternative_a: "Ouya"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "émulateur"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Mednafen"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Mednafen est un émulateur en ligne de commande qui émule les consoles:
Atari Lynx
Neo Geo Pocket (Color)
WonderSwan
GameBoy (Color)
GameBoy Advance
Nintendo Entertainment System
Super Nintendo Entertainment System/Super Famicom
Virtual Boy
PC Engine/TurboGrafx 16 (CD)
SuperGrafx
PC-FX
Sega Game Gear
Sega Genesis/Megadrive
Sega Master System
Sega Saturn (experimental, x86_64 only)
Sony PlayStation
Si le lancement et la configuration du logiciel en ligne de commande peut s'avérer difficile pour certains il est toutefois possible de greffer à Mednafen des interfaces graphiques qui permettent de gérer les paramètres graphiques, audio, la gestion des jeux, des sauvegardes ainsi que les touches. L'utilisation d'un joystick est recommandé en lieu et place du clavier.

