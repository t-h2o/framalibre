---
nom: "Meld"
date_creation: "Mardi, 10 février, 2015 - 18:07"
date_modification: "Samedi, 21 janvier, 2017 - 16:04"
logo:
    src: "images/logo/Meld.png"
site_web: "http://meldmerge.org/"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Meld est un visualiseur de différences."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "développement"
    - "traitement de texte"
lien_wikipedia: "https://en.wikipedia.org/wiki/Meld_%28software%29"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Meld est un visualiseur de différences. C'est une interface graphique très pratique pour comparer deux versions d'un même fichier texte, ou encore pour analyser les différences entre deux dossiers (fichiers ajoutés, enlevés ou modifiés). Il permet également d'éditer directement les fichiers texte et de comparer, copier ou supprimer les fichiers entre deux dossiers.
Meld supporte la comparaison triple pour les fichiers et les répertoires, et les gestionnaires de version.

