---
nom: "MicroAlg"
date_creation: "Mardi, 28 mars, 2017 - 10:16"
date_modification: "Samedi, 20 août, 2022 - 11:30"
logo:
    src: "images/logo/MicroAlg.png"
site_web: "http://microalg.info"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
description_courte: "MicroAlg est un langage de programmation en français dédié à l’algorithmique et à son enseignement."
createurices: "Christophe Gragnic"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "algorithme"
    - "langage"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

MicroAlg est un langage de programmation en français dédié à l’algorithmique et à son enseignement.
Simple (des parenthèses et des guillemets pour seuls caractères spéciaux), elle offre un cadre rigoureux pour l’apprentissage des concepts les plus importants de l’algorithmique et de la programmation impérative.
Créé par un professeur de mathématiques de la région Nantaise (contacter par courriel), c’est un logiciel libre (licence GPL2), ouvert et gratuit. Le langage en lui-même s’adresse à des enseignants qui voudraient utiliser, avec leurs élèves, un langage de programmation simple et facile à mettre en place. Il s’adresse aussi à des débutants qui voudraient apprendre par eux-mêmes.
Une des forces de MicroAlg est qu’elle est utilisable dans un navigateur.
!!! Ce projet semble abandonné : site officiel piraté et pas de modification sur GitHub depuis 3 ans !!!

