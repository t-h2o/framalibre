---
nom: "Minder"
date_creation: "Vendredi, 11 décembre, 2020 - 23:08"
date_modification: "Vendredi, 24 septembre, 2021 - 13:32"
logo:
    src: "images/logo/Minder.png"
site_web: "https://github.com/phase1geo/Minder"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Du mind mapping simple, efficace et personnalisable."
createurices: "Trevor Williams"
alternative_a: "mindjet, imindmap, Mindomo, MindMeister, novamind"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "mind mapping"
    - "carte heuristique"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Minder est un logiciel de mind mapping simple à prendre en main et qui propose plusieurs options permettant de personnaliser ses cartes : thèmes pré-établis, réglage du style et de l'épaisseur des lignes, des marges extérieures et intérieures… Des "stickers" peuvent être ajouté aux nœuds (une bibliothèque de plusieurs dizaines de stickers est inclue).
Ces derniers peuvent être agencés librement, ou bien être automatiquement disposés verticalement ou horizontalement.
La syntaxe Markdown peut être utilisée pour la rédaction des nœuds.
Minder peut exporter les cartes mentales sous une multitude de formats (images bmp ou png, mermaid, csv, text brut…)

