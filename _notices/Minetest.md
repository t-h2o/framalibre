---
nom: "Minetest"
date_creation: "Mercredi, 4 janvier, 2017 - 15:31"
date_modification: "Lundi, 10 mai, 2021 - 14:42"
logo:
    src: "images/logo/Minetest.png"
site_web: "http://www.minetest.net"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Un jeu de cubes dans un espace illimité ou pioches et pelles sont de rigueur pour édifier des constructions..."
createurices: "Perttu Ahola"
alternative_a: "Minecraft"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "jeu vidéo"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
---

Un jeu de cubes dans un espace illimité ou pioches et pelles sont de rigueur pour édifier des constructions imaginaires, creuser des tunels, se promener au grès de ses envies, faire des rencontres, ramasser quelques fleurs de ci de la, nager, ...
Minetest c'est l'équivalent de son petit frère Minecraft avec en supplément la gratuité du jeu et la possibilité d'intégrer une équipe de développement pour améliorer le produit.
Minetest, c'est aussi un serveur qui permet d'accueillir vos amis ou simplement quelques visiteurs curieux voire même des joueurs chevronnés.
L'installation est simple et le jeu est entièrement traduit en français.

