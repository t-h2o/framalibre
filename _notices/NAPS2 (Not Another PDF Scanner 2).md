---
nom: "NAPS2 (Not Another PDF Scanner 2)"
date_creation: "Mercredi, 14 octobre, 2020 - 22:36"
date_modification: "Mercredi, 12 mai, 2021 - 15:21"
logo:
    src: "images/logo/NAPS2 (Not Another PDF Scanner 2).png"
site_web: "https://www.naps2.com/"
plateformes:
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Scanner des documents facilement."
createurices: "Ben Olden-Cooligan"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "scanner"
    - "manipulation de pdf"
    - "reconnaissance optique de caractères"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

NAPS2 permet de scanner rapidement et facilement des documents pour les enregistrer en tant qu’image ou de PDF avec reconnaissance de caractère.
Les profils permettent de gérer différents matériels et différents paramètres comme couleur/niveau de gris ou la définition du scan.
Les scans de plusieurs pages peuvent être manipulés, réordonnés, pivotés avant enregistrement. Ces manipulations de pages fonctionnent aussi sur des documents PDF importés.
Il est compatible avec les formats WIA et TWAIN.
Et il est proposé en 40 langues.
Les utilisateurs plus avancés pourront aussi utiliser la ligne de commande.

