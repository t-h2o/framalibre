---
nom: "Nexuiz"
date_creation: "Dimanche, 16 avril, 2017 - 14:32"
date_modification: "Mercredi, 24 octobre, 2018 - 19:28"
logo:
    src: "images/logo/Nexuiz.png"
site_web: "http://www.alientrap.com/games/nexuiz/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "English"
description_courte: "Jeu de tir à la première personne basé sur une version mo"
createurices: ""
alternative_a: "Nexuiz par Illfonic"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "fps"
    - "tir"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Nexuiz"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Jeu de tir à la première personne basé sur une version modifié du Quake Engine après qu'il ait été libéré.
Le jeu peut se dérouler en réseau contre d'autres joueurs ou contre l'ordinateur en solo ou en équipe dans les deux cas.
Le jeu libre Nexuiz ne doit pas être confondu avec le jeu propriétaire Nexuiz par Illfonic sorti en 2012 après que cette société a acheté les droits sur le nom du jeu. Le développement du jeu libre a repris et continué sous le nom de Xonotic.

