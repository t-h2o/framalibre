---
nom: "Noethys"
date_creation: "Lundi, 19 mars, 2018 - 23:26"
date_modification: "Lundi, 19 mars, 2018 - 23:26"
logo:
    src: "images/logo/Noethys.png"
site_web: "https://www.noethys.com/"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Français"
description_courte: "Pour les crèches, les centres d'activités (MJC, centre aérés, club sportifs et cuturels …)"
createurices: "Ivan LUCAS"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "cantine"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Noethys est un logiciel permettant de gérer des individus à la facturation en passant par le pointage et l'encaissement, Noethys vous permet d'effectuer toutes les tâches administratives quotidiennes avec efficacité et rapidité.

