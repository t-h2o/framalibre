---
nom: "OBS studio"
date_creation: "Vendredi, 17 février, 2017 - 12:20"
date_modification: "Mercredi, 12 mai, 2021 - 15:07"
logo:
    src: "images/logo/OBS studio.png"
site_web: "https://obsproject.com/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "OBS studio est un logiciel de streaming vidéo et screencasting."
createurices: "Hugh Bailey"
alternative_a: "Xsplit, Camstudio"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "streaming"
    - "enregistrement"
    - "visioconférence"
    - "vidéo"
    - "screencast"
    - "capture d'écran"
    - "vidéographie"
lien_wikipedia: "https://fr.wikipedia.org/wiki/OBS_Studio"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
---

OBS studio (comme Open Broadcast Software), est un logiciel de streaming vidéo et de screencasting pour des plates formes telles que twicht, youtube gaming, dailymotion (et plein d'autres).
Il permet de paramétrer ce que sera diffusé à l'écran, et d'arranger les différentes fenêtres que l'on souhaite afficher (webcam, fenêtre de jeux, ou bureau, affichage de texte), tout en permettant beaucoup de configurations à l'aide de plugin.
Pour s'entraîner, un enregistrement en local est possible.
Le logiciel propose plusieurs types de paramétrages en fonction de votre configuration matérielle, de votre bande passante (débit montant), et de vos envies.

