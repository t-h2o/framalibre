---
nom: "OPEN-COPRO"
date_creation: "Mercredi, 27 septembre, 2017 - 20:25"
date_modification: "Vendredi, 26 mai, 2023 - 10:14"
logo:
    src: "images/logo/OPEN-COPRO.png"
site_web: "http://www.batisseurs-numeriques.fr/open-copro.php"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
langues:
    - "Français"
description_courte: "Le logiciel OPEN-COPRO qui permet une gestion des tâches en ligne pour les ASL et Syndic."
createurices: "Damien & Bruno"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "syndic"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

OPEN-COPRO qui une gestion des tâches en ligne pour les besoins des petits lotissements. Il permet de gérer la comptabilité et l'administratif.

Gestion des parcelles
Gestion des propriétaires
Saisie des charges annuelles
Envoi de messages personnalisées par email
Gestion des règlements

Un seul logiciel commun à installer sur internet.

