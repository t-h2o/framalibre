---
nom: "OmegaT"
date_creation: "Lundi, 30 mars, 2020 - 17:10"
date_modification: "Lundi, 23 janvier, 2023 - 08:37"
logo:
    src: "images/logo/OmegaT.png"
site_web: "https://omegat.org"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "OmegaT est un outil de traduction assistée par ordinateur (TAO) conçu pour les traducteurs professionnels."
createurices: ""
alternative_a: "SDL Trados, memoQ, DejaVu, Poedit"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "traduction"
lien_wikipedia: "https://fr.wikipedia.org/wiki/OmegaT"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

OmegaT est un puissant outil de traduction assistée par ordinateur (TAO) proposant notamment une segmentation personnalisable utilisant les expressions régulières, une mémoire de traduction avec extraction/insertion des correspondances partielles et propagation des correspondances, la recherche dans les glossaires et dictionnaires, la recherche dans les mémoires de traduction et dans les documents de référence ainsi qu'une vérification de l'orthographe et de la grammaire incorporée basée sur l'utilisation de Hunspell et de LanguageTool.

