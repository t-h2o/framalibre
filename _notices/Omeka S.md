---
nom: "Omeka S"
date_creation: "Mardi, 27 décembre, 2016 - 23:53"
date_modification: "Mercredi, 12 mai, 2021 - 15:46"
logo:
    src: "images/logo/Omeka S.png"
site_web: "https://github.com/omeka/omeka-s"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Une réécriture de Omeka orientée vers le web de données ou web sémantique"
createurices: "Roy Rosenzweig Center for History and New Media"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "cms"
    - "archives"
    - "collection"
    - "recherche"
    - "musée"
    - "bibliothèque"
    - "science"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Omeka S, constitue une refonte complète de Omeka, dit « classique », qui intègre les standards du web sémantique et tient compte des retours des utilisateurs des installations en production de Omeka.
Il conserve toutefois cette situation assez singulière, à la fois un outils de gestion de fonds (bibliothèques, archives, objets, etc.) et un système de publication destinés à faciliter l'accès aux contenus et leur partage.
Il offre les fonctionnalités suivantes :
Gestion d'un ensemble important d'items (métadonnées et ressources associées) selon des critères scientifiques et documentaires « à la carte ».
Gestion de sites publics  basés sur un choix d'items avec des contenus et des ressources complémentaires.
Interopérabilité. Les données sont stockées au format json-ld. L'import et l'export sont ainsi facilités et des modules d'échanges sont en cours de développement. Une API est accessible.

