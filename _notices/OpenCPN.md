---
nom: "OpenCPN"
date_creation: "Mardi, 3 mars, 2015 - 16:00"
date_modification: "Mardi, 10 janvier, 2017 - 03:35"
logo:
    src: "images/logo/OpenCPN.png"
site_web: "http://opencpn.org"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "OpenCPN est un logiciel cartographique dédié à la navigation maritime."
createurices: ""
alternative_a: "Maxsea"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "science"
    - "cartographie"
    - "sig"
    - "gps"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

OpenCPN est un logiciel cartographique dédié à la navigation maritime.
Permet de préparer les navigations ou de suivre votre navigation comme sur un traceur.
Compatible avec les cartographies S57, S63 (cartes vectorielles), BSB v3, raster,...
Un système de plugin permet d'ajouter des fonctionalités déjà nombreuses
GPS,
affichage de GRIB,
AIS...

