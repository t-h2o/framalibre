---
nom: "OpenMPTCProuter"
date_creation: "Samedi, 22 septembre, 2018 - 09:32"
date_modification: "Mercredi, 12 mai, 2021 - 15:15"
logo:
    src: "images/logo/OpenMPTCProuter.png"
site_web: "https://www.openmptcprouter.com"
plateformes:
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "OpenMPTCProuter permet l’agrégation de plusieurs connexions Internet en utilisant MPTCP et ShadowSocks."
createurices: "ycarus"
alternative_a: "OverTheBox"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "agrégateur"
    - "raspberry pi"
    - "vpn"
    - "réseau"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

OpenMPTCProuter est une solution permettant d'agréger et de chiffrer plusieurs connexions internet et de les terminer sur n'importe quel VPS, ce qui permet sécurité, fiabilité et une adresse IP fixe.
L'agrégation est basée sur Multipath TCP (MPTCP), qui est indépendant du FAI, de la latence et du type de connexion (Fibre, VDSL, ADSL or même 4G).
L'agrégation via Multi-link VPN (MLVPN) et Glorytun UDP avec le support multipath est aussi supportée.
La solution utilise OpenWRT afin d'avoir une interface simple et agréable et permettre l'installation de nombreux logiciels.

