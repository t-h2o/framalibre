---
nom: "OpenSeaMap"
date_creation: "Dimanche, 8 janvier, 2017 - 19:22"
date_modification: "Mercredi, 12 mai, 2021 - 15:57"
logo:
    src: "images/logo/OpenSeaMap.png"
site_web: "http://www.openseamap.org/index.php?id=openseamap&L=1"
plateformes:
    - "Autre"
langues:
    - "Autres langues"
description_courte: "OpenSeaMap - La cartographie nautique libre"
createurices: ""
alternative_a: ""
licences:
    - "Creative Commons -By-Sa"
    - "Licence de base de données ouverte (OdbL)"
tags:
    - "internet"
    - "sig"
    - "cartographie"
    - "carte géographique"
    - "gps"
    - "osm"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

OpenSeaMap - La cartographie nautique libre
OpenSeaMap propose une cartographie à destination des plaisanciers en ajoutant sur les cartes d'openstreetmap des informations nautiques. Cela regroupe aussi bien les balises, bouées nautiques, cardinales... mais aussi magasins & chantiers nautiques. Il s'agit d'une "couche" supplémentaire à la cartographie Openstreetmap.
Service créé en 2009 avec pour objectif de fournir des cartes marines accessibles facilement.
Les données cartographiques collectées sont ré-utilisables sous licence libre ODbL. les cartes (tuiles) générées par openseamap sont régies par la licence CC-BY-SA.
Avertissement : L'utilisation des cartes officielles nautiques ré-actualisées est la composante essentielle d'une bonne navigation.
OpenSeaMap peut servir à la préparation d'une croisière mais toujours en complément de l'usage des cartes nautiques "classiques".
OpenSeaMap ne saurait – en aucun cas – se substituer aux cartes officielles.

