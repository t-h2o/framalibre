---
nom: "Openturns"
date_creation: "Mercredi, 3 novembre, 2021 - 15:24"
date_modification: "Vendredi, 12 novembre, 2021 - 10:21"
logo:
    src: "images/logo/Openturns.png"
site_web: "https://openturns.github.io/www/"
plateformes: "GNU/Linux, Mac OS X, Windows"
langues: "English"
description_courte: "Bibliothèque Python pour la gestion des probabilités avec interface graphique Persalys"
createurices: ""
licences: "Licence publique générale limitée GNU (LGPL)"
tags:
    - "Science"
    - "probabilités"
    - "Incertitudes"
    - "Python"
    - "librairie"
lien_wikipedia: ""
lien_exodus: ""
alternative_a: "UQLab"
mis_en_avant: "non"
---

Openturns est une bibliothèque Python permettant la gestion des probabilités. Elle permet de définir des variables et expériences aléatoire et de leur appliquer des méthodes numériques ou semi analytique. La documentation est très fournie.
Il existe une interface graphique pour Salome, Persalys, pour un couplage plus facile avec des codes de calcul pour du calcul scientifique.

