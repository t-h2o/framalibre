---
nom: "Optim Office"
date_creation: "Vendredi, 21 avril, 2017 - 13:54"
date_modification: "Vendredi, 21 avril, 2017 - 14:27"
logo:
    src: "images/logo/Optim Office.png"
site_web: "https://www.scenari.org/modeles/OptimOffice/"
plateformes: "GNU/Linux, Mac OS X, Windows"
langues: "Français, English, Español"
description_courte: "Optim Office est une chaîne éditoriale dite «généraliste», c'est à dire qu'elle est adaptée à tous."
createurices: ""
licences: "Licence CECILL (Inria), Licence Publique Générale GNU (GNU GPL), Licence publique générale limitée GNU (LGPL), Licence Publique Mozilla (MPL)"
tags:
    - "Bureautique"
    - "chaîne éditoriale"
    - "création de site web"
    - "diaporama"
    - "Traitement de texte"
lien_wikipedia: ""
lien_exodus: ""
alternative_a: "Microsoft Office"
mis_en_avant: "non"
---

Optim Office est une chaîne éditoriale simple d'utilisation pour réaliser des rapports, dossiers, site web, diaporama, documents PDF…
De nombreux formats de fichiers peuvent être insérés (tableaux, images, vidéos…)
Des outils variés permettent de créer un blog avec flux RSS pour la génération de site Web, d'insérer des galeries d'images / photos…
Après s'être initié à Optim Office on peut activer Optim Office Plus pour bénéficier de fonctionnalités supplémentaires : créateur de CV, intégration de listing informatique, menus graphique pour les sites web.
Optim Office est un des modèles documentaires de Scenari.

