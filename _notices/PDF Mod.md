---
nom: "PDF Mod"
date_creation: "Lundi, 2 janvier, 2017 - 15:41"
date_modification: "Lundi, 2 janvier, 2017 - 15:41"
logo:
    src: "images/logo/PDF Mod.png"
site_web: "https://wiki.gnome.org/Apps/PdfMod"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Manipulez vos fichiers PDF avec un outil rapide et simple."
createurices: ""
alternative_a: "Adobe Acrobat DC"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "pdf"
    - "manipulation de fichier"
    - "mise en page"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
---

PDF mod permet de manipuler des fichiers PDF. De manière simple et intuitive, il permet de :
pivoter, extraire, supprimer des pages,
réorganiser des pages avec glisser/déposer,
combiner plusieurs documents PDF,
modifier les métadonnées...

