---
nom: "Paheko"
date_creation: "Mercredi, 28 décembre, 2016 - 13:00"
date_modification: "Vendredi, 16 décembre, 2022 - 16:32"
logo:
    src: "images/logo/Paheko.png"
site_web: "https://paheko.cloud/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
langues:
    - "Français"
description_courte: "Gestion d'association : membres, compta pro mais simple, cotisations, site web, cloud fichiers, etc."
createurices: ""
alternative_a: "Ciel Associations, helloasso, Assoconnect, Pep's Up"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "métiers"
    - "association"
    - "gestion"
    - "comptabilité"
    - "gestion de la relation client"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Paheko"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Gestionnaire d'association simple, léger, puissant et complet, à utiliser hors ligne ou en ligne, installé sur son serveur ou à utiliser directement sans installation. Permet de gérer la compta, les membres, les cotisations, les rappels par email, la messagerie entre membres, et aussi intègre un site web personnalisable, la gestion/stockage/partage des fichiers, etc.

