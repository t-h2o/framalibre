---
nom: "Pale Moon"
date_creation: "Mercredi, 18 janvier, 2017 - 23:47"
date_modification: "Mercredi, 12 mai, 2021 - 14:52"
logo:
    src: "images/logo/Pale Moon.png"
site_web: "http://www.palemoon.org/"
plateformes:
    - "GNU/Linux"
    - "Windows"
    - "Android"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Pale Moon est un navigateur basé sur un fork de Mozilla Firefox en 2009 et qui a suivi son propre chemin."
createurices: "Moonchild (M.C.Straver)"
alternative_a: "Microsoft Edge, Google Opera, Google Chrome, Maxthon, Internet Explorer"
licences:
    - "Licence Publique Mozilla (MPL)"
tags:
    - "internet"
    - "navigateur web"
    - "web"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Pale_Moon"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Pale Moon propose de nombreuses fonctionnalités que Firefox a abandonné comme la barre d'état ou l'interface pré-Australis ou la non intégration des DRM, le support des processeurs anciens tout en étant compatible avec un grand nombre d'extensions pour Firefox. Utilisant Goanna (un fork du moteur de rendu Gecko), Pale Moon est le navigateur pour ceux qui sont mécontents des changements que Mozilla leur impose.
Disponible pour les systèmes GNU/Linux, Android et Windows, le support pour d'autres plate-formes est en cours.

