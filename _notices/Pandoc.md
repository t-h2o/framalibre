---
nom: "Pandoc"
date_creation: "Jeudi, 29 décembre, 2016 - 16:15"
date_modification: "Lundi, 10 mai, 2021 - 14:39"
logo:
    src: "images/logo/Pandoc.png"
site_web: "http://pandoc.org"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Convertissez toutes sortes de documents avec ce couteau suisse multi-formats."
createurices: "John MacFarlane"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "format"
    - "markdown"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Pandoc"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
---

Pandoc est un puissant logiciel de conversion de documents en ligne de commande.  Il converti vers et depuis de multiples formats. Mais son intérêt réside surtout dans l'automatisation de tâches et la possibilité d'utiliser des modèles (templates) pour obtenir des produits de conversion. Ainsi, par exemple, il est possible d'éditer un fichier dans un format Markdown amélioré (le « Pandoc Markdown ») et obtenir des sorties LaTeX, HTML, ePub, ODT ou DOCX avec des mise en page prédéfinies. De même, en utilisant LaTeX, il est possible de produire des documents PDF (ou même des présentations) en se contentant de rédiger un document en Markdown et sans toucher une ligne de LaTeX. La commande Pandoc supporte plusieurs options servant à conditionner la conversion ou mettre en page la sortie demandée.

