---
nom: "Password Safe"
date_creation: "Lundi, 3 septembre, 2018 - 12:34"
date_modification: "Jeudi, 21 mai, 2020 - 15:34"
logo:
    src: "images/logo/Password Safe.png"
site_web: "https://gitlab.gnome.org/World/PasswordSafe"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Client KeePass intégré au bureau GNOME"
createurices: "Falk Alexander Seidl"
alternative_a: "1password, lastpass"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "sécurité"
    - "gnome"
    - "gestionnaire de mots de passe"
    - "mot de passe"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Password Safe est un gestionnaire de mot de passe basé sur le format Keepass v.4.
Il s'intègre parfaitement à l'environnement de bureau GNOME et fournit une interface dépouillée et facile d'accès pour la gestion des données de mots de passe.

