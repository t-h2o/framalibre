---
nom: "Pastèque"
date_creation: "Mercredi, 19 avril, 2017 - 09:44"
date_modification: "Vendredi, 7 mai, 2021 - 17:06"
logo:
    src: "images/logo/Pastèque.png"
site_web: "https://www.pasteque.org"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
langues:
    - "Français"
    - "English"
description_courte: "Pastèque est un logiciel libre de caisse enregistreuse"
createurices: "Cédric Houbart, Philippe Pary"
alternative_a: "pointex, tactill, hiboutik, jdc"
licences:
    - "Licence Publique Générale Affero (AGPL)"
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "caisse enregistreuse"
    - "gestion"
    - "commerce"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Past%C3%A8que_%28logiciel%29"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Pastèque est une suite de logiciels libres ayant pour finalité d’opérer des caisses enregistreuses.
La suite est composée de 3 logiciels :
— Pastèque Serveur, sous licence AGPL3+ : backoffice du commerçant. Enregistre les ventes des logiciels clients, permet l’administration des ventes etc. Le tout depuis un navigateur web (logiciel PHP)
— Pastèque Desktop, sous licence GPL3+ : front office du commerçant, prévu pour tourner sur PC (Windows ou Linux) ainsi que sur MacOS. Il a un look très old school, mais il fait son travail efficacement
— Pastèque Android, sous licence GPL3+ : front office du commerçant, prévu pour tourner sous tablette Android. Petit dernier de la série, il est développé pour être aussi efficace que son grand frère Desktop mais sur des écrans contraints (taille 10")

