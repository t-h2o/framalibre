---
nom: "Peazip"
date_creation: "Mardi, 24 janvier, 2017 - 22:54"
date_modification: "Samedi, 19 novembre, 2022 - 09:23"
logo:
    src: "images/logo/Peazip.png"
site_web: "https://peazip.github.io/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Gestionnaire d'archives supportant de nombreux formats"
createurices: "Giorgio Tani"
alternative_a: "winrar, winzip"
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "système"
    - "compression de fichiers"
    - "zip"
lien_wikipedia: "https://fr.wikipedia.org/wiki/PeaZip"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Peazip est un logiciel de compression et de décompression de fichiers qui gère :

en compression/décompression : 7z, FreeArc's arc/wrc, sfx (7z and arc), bz2, gz, paq/lpaq/zpaq, pea, quad/balz/bcm, split, tar, upx, wim, zip.
de très nombreux formats en décompression dont : apk, zipx, cab, rar, iso

Plus de détail ici.
Il contient un explorateur de fichier avec recherche, favoris, détection des fichiers en doublons, calcul de checksum.
La partie gestionnaire d'archive contient des fonctionnalités relatives à la sécurité comme le chiffrement fort, la double authentification, la gestion de mot de passe, la suppression sûre.
Il s'intègre au menu contextuel de Windows et Linux.
Enfin il possède également un visionneur de miniatures permettant d'effectuer des manipulations simples sur les images : rotation, rognage, mise à l'échelle.

