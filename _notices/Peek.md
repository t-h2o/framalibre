---
nom: "Peek"
date_creation: "Dimanche, 18 août, 2019 - 13:36"
date_modification: "Mercredi, 1 mars, 2023 - 09:32"
logo:
    src: "images/logo/Peek.png"
site_web: "https://github.com/phw/peek"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Peek est un logiciel de capture d'écran très simple d'utilisation."
createurices: "Philipp, Wolfer"
alternative_a: "bandicam, screenpresso, icecream screen recorder, ezvid, activepresenter"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "screencast"
    - "capture d'écran"
    - "capture vidéo"
    - "enregistrement"
    - "gif"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
---

Attention ! : Projet déprécié - plus maintenu
Le développeur a annoncé le 14janvier 2023 qu'il dépréciait son projet.
Peek est un logiciel de capture d'écran très simple d'utilisation. Porté sur de très nombreux systèmes d'exploitation GNU/Linux, il vous permettra d'enregistrer votre écran ou une partie aux formats .GIF .mp4 .WebM et .APNG .
Installation
Sur Debian
sudo apt install peek
Sur Ubuntu
sudo add-apt-repository ppa:peek-developers/stable
sudo apt update
sudo apt install peek
Via Flatpak
Installation: flatpak install flathub com.uploadedlobster.peek
Lancement: flatpak run com.uploadedlobster.peek
Mise à jour: flatpak update --user com.uploadedlobster.peek

