---
nom: "PeerTube"
date_creation: "Samedi, 1 septembre, 2018 - 16:34"
date_modification: "Mardi, 2 juillet, 2019 - 10:30"
logo:
    src: "images/logo/PeerTube.png"
site_web: "https://joinpeertube.org/fr/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Web"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "PeerTube est un logiciel décentralisé et fédéré d'hébergement de vidéos."
createurices: "Chocobozzz"
alternative_a: "Youtube, Dailymotion, Vimeo"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "vidéo"
    - "fediverse"
    - "streaming"
    - "p2p"
    - "activitypub"
    - "décentralisation"
lien_wikipedia: "https://fr.wikipedia.org/wiki/PeerTube"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
---

PeerTube est un logiciel décentralisé et fédéré d'hébergement de vidéos.
Pour publier des vidéos, l'utilisateur doit s'inscrire chez un hébergeur (nommé instance). Chaque hébergeur possède ses conditions d'utilisation (espace de stockage par utilisateur, règles de modération, thématiques, etc.).
Grâce à WebTorrent, si plusieurs personnes consultent une même vidéo, des fragments de celle-ci sont échangés entre les personnes afin de ne pas surcharger l'instance.
Décentralisé : Chaque instance peut suivre une ou plusieurs autres instances PeerTube afin de permettre à ses utilisateurs de visionner les vidéos de celles-ci.
Fédéré : Via le protocole ActivityPub, Peertube peut interagir avec d'autres logiciels qui font partie du Fediverse, comme Mastodon par exemple.

