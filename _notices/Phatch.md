---
nom: "Phatch"
date_creation: "Mardi, 25 avril, 2017 - 17:02"
date_modification: "Lundi, 17 janvier, 2022 - 17:17"
logo:
    src: "images/logo/Phatch.png"
site_web: "http://photobatch.wikidot.com/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Traitement d'images par lots."
createurices: "Stani, Nadia Alramli, Erich Heine, Juho Vepsäläinen"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "manipulation d'images"
lien_wikipedia: "https://en.wikipedia.org/wiki/Phatch"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Phatch permet d'appliquer un traitement (ou un ensemble de traitements) à tout un lot d'images en simultanés.
Très pratique notamment lorsqu'on a une série de captures d'écrans ou de photographies que l'on souhaite à la fois : changer de format, redimensionner, entourer d'une même bordure…
Ce logiciel réalise ce qu'on lui demande dans la limite de la longue liste d'options de traitements différents et c'est bien ce que l'on attend de lui.

