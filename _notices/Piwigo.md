---
nom: "Piwigo"
date_creation: "Jeudi, 9 mars, 2017 - 23:07"
date_modification: "Mercredi, 12 mai, 2021 - 15:27"
logo:
    src: "images/logo/Piwigo.png"
site_web: "http://fr.piwigo.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Apple iOS"
    - "Web"
langues:
    - "Autres langues"
description_courte: "Hébergez et partagez vos photos avec Piwigo"
createurices: "Pierrick Le Gall"
alternative_a: "Koken, Orphéa, Keepeek, Orkis Ajaris, Google Photos"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "galerie photo"
    - "photo"
    - "photothèque"
    - "photothèque en ligne"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Piwigo"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
---

En 2001, Pierrick Le Gall développe sa galerie photo PhpWebGallery. Ce projet libre connaît un grand engouement, et en 2009, devient Piwigo (gestion des ressources numériques). L'installation nécessite PHP 5.2, MySQL 5 et ImageMagick ou GD.
En 2010, Pierrick crée sa société et propose l'hébergement de photos ce qui permet de pérenniser le développement de fonctionnalités.
Quelques fonctionnalités :
téléverser des photos par FTP, web, digiKam, Shotwell, Lightroom, iPhoto, Aperture et app. iPhone/Android ;
organiser ses photos par albums, par tags et par dates ;
gérer finement les permissions d'accès aux différentes résolutions des photos ;
définir la zone d'intérêt de l'image (pour un joli zoom) ;
remplir automatiquement les propriétés à partir des métadonnées EXIF/IPTC ;
étendre les possibilités avec des thèmes et extensions.

