---
nom: "PixelFed"
date_creation: "Mardi, 4 septembre, 2018 - 12:35"
date_modification: "Mercredi, 19 septembre, 2018 - 11:24"
logo:
    src: "images/logo/PixelFed.png"
site_web: "https://pixelfed.org/?lng=fr"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Web"
langues:
    - "English"
description_courte: "Un logiciel décentralisé et fédéré de partage d'images."
createurices: "Daniel Supernault"
alternative_a: "Instagram"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "partage d'image"
    - "réseau social"
    - "décentralisation"
    - "fediverse"
    - "activitypub"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
---

PixelFed est un logiciel décentralisé et fédéré de partage d'images en cours de développement.
En plus de reprendre les fonctionnalités d'Instagram, le fonctionnement de PixelFed est:
Décentralisé : Chaque instance peut suivre une ou plusieurs autres instances PixelFed afin de permettre à leurs membres respectifs d'interagir. Une première instance publique pixelfed.social limitée à 10 000 membres a déjà été créée.
Fédéré : Via le protocole ActivityPub, PixelFed peut interagir avec d'autres logiciels qui font partie du Fediverse, comme Mastodon ou PeerTube par exemple.
Il est également possible d'importer ses données depuis Instagram.

