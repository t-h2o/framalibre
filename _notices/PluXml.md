---
nom: "PluXml"
date_creation: "Mercredi, 25 février, 2015 - 17:33"
date_modification: "Lundi, 10 mai, 2021 - 13:05"
logo:
    src: "images/logo/PluXml.png"
site_web: "https://www.pluxml.org"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Système de gestion de contenu développé en PHP"
createurices: "Peter"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "cms"
    - "blog"
lien_wikipedia: "http://fr.wikipedia.org/wiki/PluXml"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Système de gestion de contenu développé en PHP
Les données sont stockées dans des fichiers XML. Il n'est donc pas nécessaire de disposer du système de gestion de base de données.
PluXml est personnalisable par l'intermédiaire de thèmes et/ou de plugins.
Multi-utilisateurs avec des niveaux d'autorisations différents
Pages statiques, catégories, gestion des tags, archives
Gestion des commentaires
Gestionnaire de médias : images, documents
Flux Rss des articles, commentaires, tags, catégories
Traduit en 11 langues (français, allemand, anglais, espagnol, italien, néerlandais, occitan, polonais, portugais, roumain, russe)
Thèmes personnalisables (supporte les thèmes pour appareils mobiles et smartphones: iphone, blackberry, pocket-pc...)
Plugins
Réécriture d'urls
Le tout développé par une équipe francophone

