---
nom: "Polymny Studio"
date_creation: "Vendredi, 27 novembre, 2020 - 20:53"
date_modification: "Vendredi, 27 novembre, 2020 - 20:53"
logo:
    src: "images/logo/Polymny Studio.png"
site_web: "https://polymny.studio/"
plateformes:
    - "Autre"
langues:
    - "Français"
description_courte: "Polymny est le studio web des formateurs qui créent, modifient et gèrent des vidéos pédagogiques !"
createurices: "Nicolas Bertrand, Thomas Forgione, Axel Carlier, Vincent Charvillat"
alternative_a: ""
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "éducation"
    - "diapositive"
    - "vidéo"
    - "formation"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

A partir d’une présentation existante (libre office, powerpoint, beamer, etc.), vous fournissez vos diapositives en PDF et enregistrez une vidéo pédagogique pour vos élèves, vos étudiants, vos clients ou vos collègues.
Polymny est un logiciel libre, utilisable gratuitement, 100% web, indépendant du système d’exploitation de votre ordinateur (windows, macOS, linux). Il suffit de créer un compte pour enregistrer une première capsule vidéo. Besoin d'aide, de support  :contacter@polymny.studio

