---
nom: "Popcorn Time"
date_creation: "Mardi, 15 août, 2017 - 15:23"
date_modification: "Lundi, 26 juillet, 2021 - 18:49"
logo:
    src: "images/logo/Popcorn Time.jpeg"
site_web: "https://popcorntime.sh/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Autre"
langues:
    - "Français"
description_courte: "Popcorn Time est un lecteur de \"salon\" pour les films et les séries depuis des torrents."
createurices: "Fait avec ❤ par une bande de geeks venant du monde entier"
alternative_a: "Netflix, Videofutur, CanalPlay, Amazon Prime, Disney +"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "vidéos"
    - "séries"
    - "bittorrent"
    - "client bittorrent"
    - "p2p"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Popcorn_Time"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Popcorn Time est le résultat de nombreux développeurs et designers qui ont rassemblé plusieurs API pour rendre l'expérience de streaming par torrent aussi simple que possible.
Nous construisons ce projet en open source depuis les quatre coins du monde. Nous adorons le cinéma… et le popcorn !
PS: Le téléchargement d’œuvres protégées peut être interdit dans votre pays. À utiliser à vos risques et périls.
Veuillez visiter notre page Intégration Continue Popcorn Time pour des versions de développement.

