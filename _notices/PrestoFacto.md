---
nom: "PrestoFacto"
date_creation: "Jeudi, 20 octobre, 2022 - 14:13"
date_modification: "Lundi, 13 mars, 2023 - 14:46"
logo:
    src: "images/logo/PrestoFacto.png"
site_web: "https://www.philnoug.com/solutions"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
    - "Web"
langues:
    - "Français"
description_courte: "Application Web de gestion et facturation de prestations journalières (cantine, garderie, activités, etc.)"
createurices: "Philippe NOUGAILLON"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "école"
    - "association"
    - "cantine"
    - "facturation"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

PrestoFacto aide les petites organisations dans la gestion et la facturation de leurs prestations (repas cantine, garderie, activités, etc.).
Cette solution vous aidera dans l’organisation de votre association et vous fera gagner un temps précieux en simplifiant la gestion administrative et la facturation de vos cantines et services périscolaires.
Une comptabilité simple par famille. Facturation des prestations basée sur le principe de la réservation.
Une facture par mois des prestations consommées, avec solde. Balance famille détaillée.
PrestoFacto est destiné aux petites collectivités, associations, mairies, écoles, centre de loisirs ...

