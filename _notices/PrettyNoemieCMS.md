---
nom: "PrettyNoemieCMS"
date_creation: "Vendredi, 8 mai, 2020 - 22:45"
date_modification: "Lundi, 11 mai, 2020 - 14:26"
logo:
    src: "images/logo/PrettyNoemieCMS.png"
site_web: "https://framagit.org/framasoft/PrettyNoemieCMS"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
langues:
    - "Français"
description_courte: "Un site WEB en une page fait sans aucune connaissance, et seulement en mode graphique."
createurices: "Robin"
alternative_a: ""
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "cms"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

PrettyNoemieCMS, développé par un stagiaire de Framasoft, Robin permet en toute simplicité de créer un site WEB ultra basique.
Tout se fait en mode WYSIWYG (what you see is what you get), l'interface d'administration s'affichant très peu différemment de la page que vous obtenez.
Ça fonctionne grâce à une liste de modules que vous pouvez positionner dans l'ordre que vous voulez et que vous pouvez, évidemment personnaliser.
Il y a un système de thèmes, et quelques personnalisation de la page de manière générale.
Ce CMS est disponible pour YunoHost (https://github.com/YunoHost-Apps/prettynoemiecms_ynh).

