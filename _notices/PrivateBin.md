---
nom: "PrivateBin"
date_creation: "Dimanche, 18 novembre, 2018 - 12:17"
date_modification: "Mercredi, 14 septembre, 2022 - 11:12"
logo:
    src: "images/logo/PrivateBin.png"
site_web: "https://privatebin.info/"
plateformes:
    - "Autre"
    - "Web"
langues:
    - "Autres langues"
description_courte: "Parce que l'ignorance c'est le bonheur"
createurices: "Sebsauvage"
alternative_a: "Pastebin, ZeroBin"
licences:
    - "Open Software License (OSL)"
tags:
    - "privatebin"
    - "pastebin"
    - "bin"
    - "texte"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
---

PrivateBin est  un service de partage de texte en ligne sécurisé.
Les données stockées sur le serveur sont chiffrées puisque le déchiffrement a lieu dans le navigateur grâce à un algorithme AES 256 bits.
PrivateBin permet de paramétrer plusieurs options pour les textes envoyés :

Une date d'expiration
La coloration syntaxique
Une protection par mot de passe
L'effacement après lecture
Les commentaires

Ce service reprend le code source de ZeroBin de SebSauvage et a été l'oeuvre de nombreux contributeurs.
Vous pouvez trouver différentes instances  ici.

