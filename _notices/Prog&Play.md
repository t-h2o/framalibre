---
nom: "Prog&Play"
date_creation: "Mercredi, 21 octobre, 2020 - 16:13"
date_modification: "Mercredi, 12 mai, 2021 - 16:55"
logo:
    src: "images/logo/Prog&Play.jpg"
site_web: "http://progandplay.lip6.fr/"
plateformes:
    - "Windows"
langues:
    - "Français"
    - "English"
description_courte: "Prog&Play, programmez et jouez..."
createurices: "Mathieu Muratet"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "rts"
    - "programmation"
    - "jeu vidéo"
    - "jeu sérieux"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Prog&Play est un jeu sérieux sur la programmation. Ce jeu exploite l'univers de Kernel Panic fonctionnant sur le moteur Spring. Trois scénarios de jeux sont proposés :
1 - Kernel Panic Campaign: Digital War, vous immerge dans un scénario de jeu original où vous devrez programmer Bits, Bytes et Assemblers en vue de constituer une armée pour renverser votre adversaire. Le scénario de jeu est décomposé en missions qui vous guident à travers l'histoire du jeu en vue d'atteindre l'objectif final.
2 - Kernel Panic Campaign: Sili-Cium Alliance, propose une seconde campagne et de nouvelles missions. A l'aide de ses alliés et d'un Pointer vous devrez participer à la reconstruction d'une base et à la reconquête de positions perdues.
3 - Byte Battle se joue en réseaux. Chaque joueur contrôle un nombre de Bytes identique qui se font face. L'objectif du jeu consiste à éliminer les Bytes de l'adversaire.
Prog&Play inclus un editeur de niveau : SPRED.

