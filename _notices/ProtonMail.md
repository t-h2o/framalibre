---
nom: "ProtonMail"
date_creation: "Lundi, 16 janvier, 2017 - 10:17"
date_modification: "Lundi, 29 août, 2022 - 15:44"
logo:
    src: "images/logo/ProtonMail.png"
site_web: "https://proton.me/fr/mail"
plateformes:

langues:
    - "Français"
    - "Autres langues"
description_courte: "ProtonMail est un service de messagerie sécurisé respectant votre vie privée."
createurices: "Andy Yen, Jason Stockman, Wei Sun"
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "bureautique"
    - "client mail"
    - "vie privée"
    - "sécurité"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
---

Avertissement : Seuls les interfaces (webmails) et applications de Protonmail sont libres, mais pas la partie serveur.ProtonMail est un service de messagerie sécurisé fondé par des membres du CERN (Conseil européen pour la recherche nucléaire) et profite ainsi d'une grande expérience en matière de gestion de système sécurisé à grande échelle.
Il supporte un chiffrement de bout en bout et ses serveurs sont situés en Suisse sous leur juridiction.
Depuis peu, Protonmail propose une application "Bridge" permettant d'utiliser les protocoles IMAP/POP dans un logiciel de gestions de mail comme Thunderbird par exemple.
Il est aussi possible d'y accéder via mobile via votre app store préféré.ProtonMail propose des comptes gratuits en offre de base, mais il est possible d'étendre l'espace de stockage ou d'utiliser son nom de domaine moyennant un abonnement.ProtonMail est accessible depuis le réseau TOR depuis l'adresse https://protonirockerxow.onion/.

