---
nom: "Qgis"
date_creation: "Lundi, 26 décembre, 2016 - 17:11"
date_modification: "Vendredi, 5 août, 2022 - 00:18"
logo:
    src: "images/logo/Qgis.jpg"
site_web: "https://qgis.org"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Apple iOS"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "QGIS, ou le SIG libre pour tous !"
createurices: "Communauté QGIS"
alternative_a: "ArcGIS, MapInfo"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "cartographie"
    - "sig"
lien_wikipedia: "https://fr.wikipedia.org/wiki/QGIS"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

QGiS est un outil de traitement de l’information géographique, gérant les images matricielles (rasters), vectorielles (shapefile, couverture GRASS GIS…), ainsi que les bases de données. Il est facile à prendre en main et il est puissant. Il est utilisé dans un cadre professionnel.

