---
nom: "Quickstart Fortran on Windows"
date_creation: "Vendredi, 10 mars, 2023 - 10:16"
date_modification: "Vendredi, 10 mars, 2023 - 10:21"
logo:
    src: "images/logo/Quickstart Fortran on Windows.png"
site_web: "https://github.com/LKedward/quickstart-fortran"
plateformes:
    - "Windows"
langues:
    - "English"
description_courte: "Pour installer facilement des outils de développement en Fortran moderne sous Windows"
createurices: "Laurence Kedward"
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "science"
    - "fortran"
    - "installation"
    - "windows"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Quickstart Fortran permet d'installer facilement sous Windows de quoi développer en Fortran moderne : le compilateur GFortran (GCC), le Fortran Package Manager fpm, le gestionnaire de versions Git, la bibliothèque OpenBLAS (BLAS/LAPACK) et GNU make. Il n'est pas nécessaire d'avoir les droits d'administration de la machine. L'utilitaire propose également un script pour installer la bibliothèque standard stdlib.
fpm et stdlib sont des outils développés par la communauté Fortran-lang.

