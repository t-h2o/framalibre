---
nom: "Quod Libet"
date_creation: "Samedi, 18 mai, 2019 - 00:50"
date_modification: "Samedi, 18 mai, 2019 - 01:00"
logo:
    src: "images/logo/Quod Libet.png"
site_web: "https://quodlibet.readthedocs.io/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un lecteur musical bit-perfect à la fois simple et puissant, pour amateur éclairé ou débutant."
createurices: "Christoph Reiter"
alternative_a: "Audirvana, winamp, Windows Media Player, iTunes"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "audio"
    - "musique"
    - "lecteur musique"
    - "lecteur audio"
    - "bibliothèque musicale"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Quod_Libet"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Quod Libet permet d’écouter sa musique, quels que soient les formats de ses fichiers (MP3, Ogg Vorbis / Speex / Opus, FLAC, Musepack, MOD/XM/IT, Wavpack, MPEG-4 AAC, WMA, MIDI, Monkey’s Audio) et sous n’importe quel système d’exploitation.
Le système de recherche fera la joie des débutants comme des experts en expressions régulières.
Pour une utilisation bit-perfect sous Linux grâce à ALSA (Advanced Linux Sound Architecture), notez les numéros de la carte et du périphérique avec la commande "aplay -l" puis reportez-les ainsi dans le pipeline de sortie des préférences : par exemple "alsasink device=hw:1,0".
(Notez que contrairement au serveur MPD, il n’est a priori pas possible de lire des fichiers DSD encapsulés via DoP.)

