---
nom: "Raspi Check"
date_creation: "Samedi, 8 avril, 2017 - 16:33"
date_modification: "Mercredi, 12 mai, 2021 - 15:08"
logo:
    src: "images/logo/Raspi Check.png"
site_web: "https://github.com/eidottermihi/rpicheck"
plateformes:
    - "Android"
langues:
    - "English"
description_courte: "Contrôlez votre Raspberry Pi depuis votre smartphone Android."
createurices: "Michael Prankl"
alternative_a: "Raspberry Pi Control"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "réseau"
    - "ssh"
    - "raspberry pi"
    - "contrôle à distance"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Raspi Check vous permet d'afficher bon nombre d'informations concernant votre Raspberry Pi sur votre appareil Android à travers une connexion SSH. Sont affichées les informations concernant le système (mémoire, température processeur, etc), l'utilisation du disque, le réseau, et les processus en cours.
L'application supporte plusieurs connexions, et permet d'exécuter des commandes entrées au préalable (les droits root seront demandés pour une commande les nécessitant).

