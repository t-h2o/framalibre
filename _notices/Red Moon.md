---
nom: "Red Moon"
date_creation: "Mercredi, 22 mars, 2017 - 21:52"
date_modification: "Mercredi, 12 mai, 2021 - 14:44"
logo:
    src: "images/logo/Red Moon.png"
site_web: "https://github.com/raatmarien/red-moon"
plateformes:
    - "Android"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Reposez vos yeux avant d'aller dormir !"
createurices: "Marien Raat"
alternative_a: "Twilight"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "android"
    - "yeux"
    - "vision"
    - "colorimétrie"
    - "utilitaire"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Red Moon est une petite application permettant de filtrer la lumière bleue d'un écran de smartphone. Elle vous permettra de reposer vos yeux et améliorera votre sommeil si vous avez l'habitude de consulter votre appareil avant de dormir.
L'application permet de régler la température de couleur, le niveau d'intensité et de luminosité. Elle offre la possibilité de jongler entre différents profils personnalisables et d'automatiser l'utilisation du filtre selon l'heure ou la localisation.
Red Moon intègre également une option permettant de prendre en compte la superposition d'écran, désactivant et réactivant le filtre lorsque c'est nécessaire.

