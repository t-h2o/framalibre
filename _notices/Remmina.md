---
nom: "Remmina"
date_creation: "Mardi, 5 mai, 2020 - 18:09"
date_modification: "Mardi, 5 mai, 2020 - 18:16"
logo:
    src: "images/logo/Remmina.png"
site_web: "https://remmina.org"
plateformes:
    - "GNU/Linux"
    - "BSD"
langues:
    - "Autres langues"
description_courte: "Remmina est un client de bureau distant Open source et copyleft"
createurices: "Antenore Gatta"
alternative_a: "TeamViewer, ultra vnc"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Remmina est un client de bureau distant écrit en Small GTK + iconGTK +. Remmina cible principalement les administrateurs système et les voyageurs qui doivent travailler à distance avec des ordinateurs devant de grands écrans ou de minuscules netbooks. Il prend en charge plusieurs protocoles réseau au sein d'une interface utilisateur intégrée et cohérente.
Caractéristiques:
• Protocoles pris en charge: RDP, VNC, SPICE, NX, XDMCP, SSH et EXEC
• Maintenir une liste de fichiers de bureau à distance, organisés par groupes
• Les postes de travail distants avec des résolutions plus élevées sont défilables / évolutifs en mode fenêtre et plein écran
• Mode plein écran Viewport: le bureau à distance défile automatiquement lorsque la souris se déplace sur le bord.
• Barre d'outils flottante en mode plein écran qui vous permet de basculer entre les modes, de basculer la saisie du clavier et de minimiser

