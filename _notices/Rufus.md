---
nom: "Rufus"
date_creation: "Samedi, 14 janvier, 2017 - 20:50"
date_modification: "Lundi, 4 mars, 2019 - 00:30"
logo:
    src: "images/logo/Rufus.png"
site_web: "https://rufus.akeo.ie/"
plateformes:
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Rufus permet de formater et créer des médias usb démarrables"
createurices: "Pete Batard"
alternative_a: "Nero"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "utilitaire"
    - "gravure"
    - "iso"
    - "usb"
    - "live usb"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Rufus est un exécutable windows qui permet de formater différents médias usb afin de les rendre démarrables et ainsi d'y graver l'image de son choix.Rufus est portable donc installable sur une clé usb.

