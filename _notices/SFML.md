---
nom: "SFML"
date_creation: "Mercredi, 23 janvier, 2019 - 21:52"
date_modification: "Vendredi, 5 août, 2022 - 13:46"
logo:
    src: "images/logo/SFML.png"
site_web: "https://www.sfml-dev.org/index-fr.php"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
description_courte: "Une librairie pour créer des jeux et des applications multimédia."
createurices: "Laurent Gomila, Marco Antognini"
alternative_a: ""
licences:
    - "Licence Zlib"
tags:
    - "développement"
    - "graphisme"
    - "développement de jeu vidéo"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Simple_and_Fast_Multimedia_Library"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

La SFML est une librairie simple d'utilisation, multi-média et multi-plateformes, permettant de développer des programmes graphiques en C++, ou dans bien d'autres langages via les bindings (C, .Net, D, Go, Haskell, Java, Pascal, Python, Ruby, Rust, etc...).

