---
nom: "ShareLaTeX"
date_creation: "Jeudi, 23 mars, 2017 - 18:36"
date_modification: "Lundi, 10 mai, 2021 - 14:13"
logo:
    src: "images/logo/ShareLaTeX.jpeg"
site_web: "https://www.sharelatex.com/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Web"
langues:
    - "Français"
    - "Autres langues"
description_courte: "L'éditeur LaTeX collaboratif, en ligne et facile à utiliser."
createurices: "Henry Oswald, James Allen"
alternative_a: "Overleaf, Google Docs, Google Slides"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "éditeur"
    - "latex"
lien_wikipedia: "https://fr.wikipedia.org/wiki/ShareLaTeX"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

ShareLaTeX est un environnement LaTeX complet et prêt à l'emploi, qui fonctionne sur un serveur. ShareLateX prend en charge presque toutes les caractéristiques LaTeX, y compris l'insertion d'images, les bibliographies, les équations, etc.
De plus, ShareLaTeX est un éditeur collaboratif, il permet à plusieurs personnes de travailler en même temps sur un même document LaTeX.  Il n'y a qu'une seule version maître de chaque document, à laquelle tout le monde a accès. Il est ainsi impossible de créer des conflits de versions, et il n'est pas nécessaire d'attendre que vos collègues vous envoient leur dernière version de travail pour continuer à travailler.
ShareLaTeX référence plus de 400 Modèles dans sa galerie de modèles. Il est donc très facile de démarrer, que vous écriviez un article de revue, une thèse ou un CV.
Enfin, ShareLaTeX possède toutes les caractéristiques classiques des éditeurs LaTeX tel que la coloration syntaxique ou bien la correction orthographique.

