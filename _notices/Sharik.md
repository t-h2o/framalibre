---
nom: "Sharik"
date_creation: "Lundi, 12 décembre, 2022 - 12:57"
date_modification: "Lundi, 12 décembre, 2022 - 12:57"
logo:
    src: "images/logo/Sharik.png"
site_web: "https://github.com/marchellodev/sharik"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Apple iOS"
    - "Web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Partagez des fichiers facilement, en réseau local et sans Internet."
createurices: "marchellodev"
alternative_a: "WeTransfer"
licences:
    - "Licence MIT/X11"
tags:
    - "partage de fichiers"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/dev.marchello.sharik/latest/"
identifiant_wikidata: ""
mis_en_avant: "non"
---

Sharik permet de partager des fichiers ou du texte entre plusieurs appareils en réseau local (par exemple connectés à une même borne WIFI) sans utiliser Internet.
Le logiciel n'a pas besoin d'être installé sur la machine qui va recevoir le fichier, il suffit qu'il soit sur la machine devant l'envoyer. Il faut toutefois pouvoir recopier un lien ou scanner un QR code pour effectuer l'envoi.
Son utilisation est très simple, il est disponible en plusieurs langues. Cependant il n'est pas accessible avec le lecteur d'écran Orca.
Sharik a valu à son développeur principal le Special Hacker Award 2022 de la FSFE.

