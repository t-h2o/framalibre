---
nom: "Shotcut"
date_creation: "Mardi, 11 avril, 2017 - 12:48"
date_modification: "Mardi, 11 avril, 2017 - 12:48"
logo:
    src: "images/logo/Shotcut.png"
site_web: "https://www.shotcut.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Montage vidéo multiplateforme et portable."
createurices: ""
alternative_a: "Windows Movie Maker"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "vidéo"
    - "éditeur vidéo"
    - "montage vidéo"
    - "webcam"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Shotcut"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Shotcut est un logiciel de montage vidéo qui fonctionne autant sous GNU/Linux que Windows ou MacOS et qui possède une version portable (utilisable sans installation). Ces caractéristiques en feront sa force quand le logiciel paraît solide mais un peu moins pourvu en fonctionnalités que d'autres.
Néanmoins, Shotcut, créé et développé par les créateurs du framework de montage vidéo MLT (Medio Lovin' Toolkit), sur lequel reposent d'autres logiciels de montage vidéo sous Linux, possède son lot de fonctionnalités. Éditeur multipistes avec formes d'onde, effets vidéo et audio, capture de la webcam, très grande prise en charge de formats audio et vidéo et du matériel (multiprocesseurs),… Shotcut doit encore se faire connaître mais il peut déjà nous être bel et bien utile.

