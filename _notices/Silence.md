---
nom: "Silence"
date_creation: "Mardi, 3 janvier, 2017 - 23:25"
date_modification: "Mercredi, 12 mai, 2021 - 16:18"
logo:
    src: "images/logo/Silence.png"
site_web: "https://silence.im/"
plateformes:
    - "Android"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Silence est une application android de SMS/MMS respectant la vie privée."
createurices: ""
alternative_a: "Messenger, Google Hangouts, Viber, iMessage, WeChat"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "messagerie instantanée"
    - "sms"
    - "android"
    - "vie privée"
    - "chiffrement"
    - "communication chiffrée"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Silence_(application)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
---

Silence (anciennement SMSsecure) est une application de messagerie libre permettant l'envoi et la réception de SMS et MMS de manière sécurisée.
Les messages stockés sur votre appareil sont chiffrés localement, ce qui permet de les protéger en cas de perte, vol ou saisie de l'appareil.
L'échange des messages se fait en utilisant le chiffrement de bout en bout. Si votre correspondant n'utilise pas l'application, vous pouvez communiquer en clair avec lui en utilisant Silence.
L'application peut être téléchargée depuis F-Droid ou Google Play. Elle fonctionne sans connexion à Internet.

