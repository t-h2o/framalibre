---
nom: "Silex"
date_creation: "Mercredi, 22 mars, 2017 - 11:07"
date_modification: "Lundi, 10 mai, 2021 - 13:46"
logo:
    src: "images/logo/Silex.png"
site_web: "http://www.silex.me"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Silex est un outil en ligne de création de sites internet libre et gratuit."
createurices: "Alex Hoyau"
alternative_a: "wix, weebly, jimdo, squarespace, Adobe Dreamweaver"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "création de site web"
    - "cms"
    - "internet"
    - "html"
lien_wikipedia: "https://fr.wikipedia.org/wiki/SILEX-RIA"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Silex est une plateforme en ligne de création de sites internet open source et gratuite utilisant la technologie HTML5. Dès sa sortie en open source, Silex a été élu projet du mois en juin 2009 sur SourceForge.net
Silex est une alternative libre aux logiciels propriétaires Wix.com, Jimdo, Weebly.
Basé sur le Javascript, Silex est une application web "monopage". Une fois installé sur un serveur, Silex est accessible de n'importe quel ordinateur connecté au web et ne nécessite aucune installation.
L'association Silex Labs offre une version hébergée de Silex et utilise Silex pour initier des novices au CSS.

