---
nom: "Snapdrop"
date_creation: "Mercredi, 19 juin, 2019 - 13:18"
date_modification: "Mercredi, 19 juin, 2019 - 13:18"
logo:
    src: "images/logo/Snapdrop.png"
site_web: "https://snapdrop.net"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Web"
langues:
    - "English"
description_courte: "La façon la plus simple de transférer des données d'un appareil à l'autre."
createurices: ""
alternative_a: "AirDrop"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "transfert de fichiers"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Snapdrop permet de transférer simplement n'importe quel type de fichier d'un appareil à un autre sur un réseau local.
Ce transfert peut s'effectuer entre deux navigateurs web ou grâce à l'application mobile.

