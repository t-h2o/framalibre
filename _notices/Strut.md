---
nom: "Strut"
date_creation: "Vendredi, 14 février, 2020 - 21:15"
date_modification: "Mercredi, 12 mai, 2021 - 16:37"
logo:
    src: "images/logo/Strut.png"
site_web: "http://strut.io"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Web"
langues:
    - "English"
description_courte: "Création de présentations"
createurices: "Matt Crinklaw-Vogt"
alternative_a: "Prezi, SoZi, Google Slides, Office 365"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "présentation"
    - "diaporama"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
---

Strut est un projet libre permettant la création de présentations en diapositives en ligne.
Un des avantages est de pouvoir partager sa présentation via un lien sans être obligé de l'envoyer par mail.
Il était notamment utilisé pour Framaslides (https://alt.framasoft.org/fr/framaslides/).

