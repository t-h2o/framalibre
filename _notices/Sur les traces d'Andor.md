---
nom: "Sur les traces d'Andor"
date_creation: "Lundi, 19 septembre, 2022 - 11:37"
date_modification: "Lundi, 19 septembre, 2022 - 11:37"
logo:
    src: "images/logo/Sur les traces d'Andor.png"
site_web: "https://andorstrail.com/"
plateformes:
    - "Android"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un RPG libre axé sur les quêtes soutenu par une histoire forte."
createurices: "Oskar Wiksten"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "enquête"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

RPG sur Android multi langues avec une longue histoire et plein de quêtes à explorer.
Ce jeu libre à déjà plus de 10 ans et est soutenu par une forte communauté. Cette dernière dispose d'un forum regroupant la plupart des information pour trouver de l'aide ou savoir comment contribuer. Il y a des traductions dans plus d'une dizaine de langues entretenues régulièrement.
Le jeu est traduit à 93% en langue française.

Disponible sur F-droid
très long : la carte est super grande et longue à explorer
plusieurs modes de difficulté
Un super humour !


