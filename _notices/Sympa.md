---
nom: "Sympa"
date_creation: "Vendredi, 26 avril, 2019 - 21:43"
date_modification: "Vendredi, 26 avril, 2019 - 21:48"
logo:
    src: "images/logo/Sympa.png"
site_web: "http://www.sympa.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un gestionnaire de listes de diffusion d'e-mails !"
createurices: ""
alternative_a: "Google groups"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "client mail"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Sympa_(informatique)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
---

Sympa (Système de Multi-Postage Automatique) est un gestionnaire de listes électroniques. Développé par le réseau Renater qui relie les organismes de recherche français, il permet d'automatiser les fonctions de gestion des listes telles que les abonnements, la modération et la gestion des archives.
Sympa s'adapte à divers types de listes mail : confidentielle, publique, de type hotline, groupe de travail, forum web. Les paramètres de confidentialité sont réglables avec un haut degré de raffinement.
De nombreuses organisations hébergent des instances de Sympa sur leurs serveurs : fournisseurs d'accès à Internet, CHATONS, etc. Une des instances les plus connues est l'instance Framalistes maintenue par Framasoft.

