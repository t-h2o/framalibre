---
nom: "Tails"
date_creation: "Vendredi, 4 janvier, 2019 - 15:02"
date_modification: "Dimanche, 16 août, 2020 - 01:47"
logo:
    src: "images/logo/Tails.png"
site_web: "https://tails.boum.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Le système d'exploitation live qui préserve votre vie privée et votre anonymat."
createurices: "Tails developers"
alternative_a: "Microsoft Windows, MacOS"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "système d'exploitation (os)"
    - "tor"
    - "chiffrement"
    - "anonymat"
    - "live usb"
    - "distribution gnu/linux"
    - "sécurité"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Tails_(syst%C3%A8me_d'exploitation)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
---

Tails est un système d'exploitation live : il s'installe sur une clé USB qui permettra ensuite d'exécuter Tails depuis n'importe quel ordinateur en la sélectionnant au démarrage.
Il est basé sur Debian GNU / Linux et intègre un certain nombre de logiciels et fonctionnalités permettant de préserver la vie privée et l'anonymat de son utilisateur :
Connexion automatique au réseau Tor, permettant d'utiliser internet de manière anonyme et de contourner la censure,
Système live qui permet de ne pas laisser de traces sur l'ordinateur utilisé,
Outils de cryptographie pour chiffrer ses fichiers, ses mails, sa messagerie instantanée.
Il embarque également des logiciels de bureautique et d'édition multimédia (LibreOffice, Gimp, Audacity, etc.). Il est possible d'ajouter d'autres logiciels en plus.

