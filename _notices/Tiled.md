---
nom: "Tiled"
date_creation: "Dimanche, 12 mars, 2017 - 02:17"
date_modification: "Mercredi, 12 mai, 2021 - 16:01"
logo:
    src: "images/logo/Tiled.png"
site_web: "http://www.mapeditor.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Tiled est un éditeur de carte en 2D prévu pour fonctionner avec bon nombre de moteur 2D."
createurices: ""
alternative_a: "TuDee"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "2d"
    - "éditeur"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Tiled est un éditeur de carte prévu pour être simple à utiliser et compatible avec une grande variété de moteur 2D. Le logiciel utilise le format TMX pour sauvegarder les cartes. Tiled n'impose pas de taille de carte minimum, de nombre de calques ou de tuiles, cela le rend très flexible. Tiled est codé en C++ et utilise le framwork Qt.

