---
nom: "Timeline"
date_creation: "Mercredi, 21 août, 2019 - 18:00"
date_modification: "Mercredi, 21 août, 2019 - 18:00"
logo:
    src: "images/logo/Timeline.png"
site_web: "http://thetimelineproj.sourceforge.net/index.html"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Représenter et naviguer entre des évènements sur une frise chronologique"
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "évènements"
    - "histoire"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Timeline permet de représenter facilement des évènements sur une frise chronologique. Celle-ci peut s'étendre sur quelques heures comme sur plusieurs siècles, ce qui autorise la mise en forme d'une grande diversité d'évènements.
Beaucoup d'options permettent d'ajouter des informations aux évènements et de qualifier ces derniers : description, icône, lien hypertexte, couleur…
Une frise peut être exporter sous plusieurs formes (image, SVG, CSV) et peut donc être facilement intégrée dans un projet existant.
Timeline se positionne comme un outil fiable et complet, sur un segment logiciel disposant d'assez peu de solutions.

