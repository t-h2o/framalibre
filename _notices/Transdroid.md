---
nom: "Transdroid"
date_creation: "Lundi, 9 janvier, 2017 - 01:49"
date_modification: "Mercredi, 12 mai, 2021 - 14:57"
logo:
    src: "images/logo/Transdroid.png"
site_web: "http://www.transdroid.org/"
plateformes:
    - "Android"
langues:
    - "Autres langues"
description_courte: "Application de gestion de torrents à distance (pour la plupart des clients populaires de torrents)"
createurices: "Eric Kok"
alternative_a: "uTorrent Remote"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "torrent"
    - "bittorrent"
    - "p2p"
    - "client bittorrent"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Application Android permettant de gérer ses torrents à distance.
Il comprend toutes les fonctionnalités classiques comme :
- Ajouter, supprimer, démarrer, arrêter un torrent
- Ajouter des tags
- Voir les trackers et les fichiers individuellement
- Gérer les priorités
Il supporte les clients les plus populaires comprenant :
- Transmission
- uTorrent
- rTorrent
- BitTorrent 6
- Deluge
- Vuze
- Bitflu
- BitComet
- Qbittorent
- Ktorrent
- Torrentflux-b4rt
Et aussi les clients de Synology, D-Link et Buffalo NAS Clients.

