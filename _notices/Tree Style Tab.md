---
nom: "Tree Style Tab"
date_creation: "Samedi, 26 juin, 2021 - 23:48"
date_modification: "Samedi, 7 août, 2021 - 14:11"
logo:
    src: "images/logo/Tree Style Tab.png"
site_web: "https://addons.mozilla.org/en-US/firefox/addon/tree-style-tab/"
plateformes:
    - "Autre"
langues:
    - "Autres langues"
description_courte: "Pour des onglets organisés comme des arbres."
createurices: "YUKI"
alternative_a: "Vivaldi"
licences:
    - "Licence MIT/X11"
    - "Licence Publique Générale GNU (GNU GPL)"
    - "Licence publique générale limitée GNU (LGPL)"
    - "Multiples licences"
tags:
    - "internet"
    - "extension"
    - "navigateur web"
    - "firefox"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

C'est une extension web Firefox qui permet d'organiser les onglets comme des arbres. Il est similaire aux extensions web comme Tab Center Reborn qui permet de mettre à la verticale les onglets, Tree Tabs et Sidebar Tabs.

