---
nom: "Tryton"
date_creation: "Jeudi, 22 décembre, 2016 - 17:00"
date_modification: "Lundi, 14 novembre, 2022 - 08:47"
logo:
    src: "images/logo/Tryton.png"
site_web: "http://www.tryton.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Une solution ERP/PGI complète, modulaire, passant à l’échelle et sûre."
createurices: ""
alternative_a: "SAP, Sage, Microsoft Dynamics, Odoo, CIEL, EBP"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "erp"
    - "facturation"
    - "achat"
    - "gestion du stock"
    - "projet"
    - "comptabilité"
    - "entreprise"
    - "crm"
    - "pgi"
    - "gestion commerciale"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Tryton"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Tryton est une plateforme de développement d’applications pour entreprise (progiciel de gestion intégré ou PGI mais aussi ERP).
De base, Tryton comprend des modules génériques couvrant des fonctionnalités allant de la gestion des ventes et achats, à la gestion de production, en passant par la gestion de projet et la gestion de stock.
En tant que plateforme de développement, Tryton est utilisé dans des verticalisations à destination du secteur de la santé (GNU Health) ou de l'assurance.

