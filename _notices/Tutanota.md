---
nom: "Tutanota"
date_creation: "Lundi, 8 avril, 2019 - 14:47"
date_modification: "Mercredi, 12 mai, 2021 - 16:24"
logo:
    src: "images/logo/Tutanota.png"
site_web: "https://tutanota.com/fr"
plateformes:
    - "Android"
    - "Apple iOS"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un client e-mail sécurisé et respectueux de votre vie privée."
createurices: ""
alternative_a: "Gmail, Hotmail"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "client mail"
    - "vie privée"
    - "sécurité"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Tutanota"
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/65415/"
identifiant_wikidata: ""
mis_en_avant: "oui"
---

Tutanota est à la fois un fournisseur de boîte mail et un logiciel applicatif en ligne (pour votre navigateur) et pour votre téléphone intelligent (android ou iOS). L'offre de base est gratuite et des offres premium sont possibles. À noter que pour garantir le chiffrement de bout en bout des données, Tutanota ne permet l'utilisation du protocole IMAP. Du coup, vous devrez utiliser les clients Tutanota (disponibles pour Windows et Linux si vous ne voulez pas utiliser l'application en ligne) et vous ne pourrez pas utiliser de client comme Thunderbird. À noter que l'application Android est disponible dans la bibliothèque F-droid.

