---
nom: "Twake"
date_creation: "Jeudi, 16 juillet, 2020 - 09:55"
date_modification: "Mercredi, 29 décembre, 2021 - 15:22"
logo:
    src: "images/logo/Twake.png"
site_web: "https://twake.app"
plateformes: "GNU/Linux, BSD, Mac OS X, Windows, Android, FirefoxOS, Windows Mobile, Apple iOS"
langues: "Français, English, Autre"
description_courte: "En open source, Teams se dit Twake"
createurices: ""
licences: "Licence Publique Générale Affero (AGPL)"
tags:
    - "Bureautique"
    - "travail collaboratif"
    - "Espace numérique de travail (ENT)"
lien_wikipedia: ""
lien_exodus: ""
alternative_a: "microsoft team, Slack"
mis_en_avant: "non"
---

Twake est un espace de travail collaboratif open source. C'est une messagerie d'équipe augmentée de plusieurs modules de collaboration. Twake offre toutes les fonctionnalités nécessaires au bon travail d'équipe  :
- Chat d'équipe
- Espace de stockage
- Calendrier de l'équipe
- Liste des choses à faire
- Appel vidéo
Note de l’équipe de modération
Le logiciel était initialement un projet propriétaire qui a été racheté par la société Linagora, qui en a libéré le code en le plaçant sous licence AGPL. Linagora a ajouté des mentions dans la licence pour "protéger" la marque Twake et renforcer l’obligation d’attribution à l’entreprise Linagora. L’équipe de modération a reçu des signalements questionnant le caractère libre de cette licence AGPL modifiée. Cette problématique est également discutée sur le dépôt de Twake.

