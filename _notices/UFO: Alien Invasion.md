---
nom: "UFO: Alien Invasion"
date_creation: "Mercredi, 11 janvier, 2017 - 10:02"
date_modification: "Mercredi, 12 mai, 2021 - 16:55"
logo:
    src: "images/logo/UFO: Alien Invasion.png"
site_web: "http://ufoai.org"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Les aliens attaquent, défendez la Terre !"
createurices: ""
alternative_a: "X-COM"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "jeu vidéo"
    - "stratégie"
    - "tour par tour"
lien_wikipedia: "https://fr.wikipedia.org/wiki/UFO:_Alien_Invasion"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

En 2084, une vague d'attaques d'origine extraterrestre frappe la Terre. Nommé à la tête d'une organisation secrète chargée de répondre à la menace, vous devez construire vos bases, préparer vos troupes et vous lancer dans bataille.
Le jeu est très similaire à ceux de la série X-COM, le gameplay se déroule en deux phases :
- Une phase sur la géosphère où vous pouvez conduire des recherches sur la technologie extraterrestre, équiper vos groupes de combat et améliorer vos bases.
- Une phase de combat au tour à tour où vous dirigez une de vos équipe de combat afin de remplir divers objectifs comme sauver des civils ou exterminer tous les extraterrestres.
Le jeu est donc un mix de gestion et de stratégie avec un contenu déjà très riche. Il dispose d'une campagne solo et d'un mode multijoueurs en LAN (basé sur la phase de combat) opposant des équipes humaines contre aliens.

