---
nom: "Ubuntu Studio"
date_creation: "Vendredi, 16 avril, 2021 - 22:16"
date_modification: "Mercredi, 12 mai, 2021 - 15:37"
logo:
    src: "images/logo/Ubuntu Studio.png"
site_web: "https://ubuntustudio.org/"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Le système d'exploitation pour la création multimédia."
createurices: ""
alternative_a: "Microsoft Windows, MacOS"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "distribution gnu/linux"
    - "système d'exploitation (os)"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Ubuntu_Studio"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

C'est un système d'exploitation dérivé d'Ubuntu orienté pour la création multimédia. Il contient de nombreux logiciels libres dédiées à la création graphique, audio et vidéo. Ils peuvent être choisis pendant l'installation en sélectionnant le type de tâche à laquelle on destine l'ordinateur. Mais le système d'exploitation nécessite un ordinateur puissant avec 2 Go de RAM et 16 Go d'espace dans le disque dur .
Licence de la capture d'écran : Funkruf / Canonical Ltd., GPL http://www.gnu.org/licenses/gpl.html, via Wikimedia Commons

