---
nom: "UwAmp"
date_creation: "Mardi, 22 décembre, 2020 - 15:13"
date_modification: "Lundi, 10 mai, 2021 - 14:04"
logo:
    src: "images/logo/UwAmp.png"
site_web: "http://www.uwamp.com/"
plateformes:
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Votre serveur Wamp Apache MySQL PHP et SQLitesur sur clé USB"
createurices: "Guillaume LE COZ"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "php"
    - "développement web"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Sans Installation
UwAmp est disponible sous forme d'archive compressée, vous décompressez et vous lancez le serveur.
Interface Simple
UwAmp est fournie avec une interface de gestion vous permettant de demarrer ou d'arréter le serveur en un clic.
Multi Version de PHP
Changez la version de PHP en un clic, UwAmp fonctionne avec un système de dépot directement connecté avec le site officiel de PHP
Monitoring CPU
Affichage en temps réel avec graphique de l'utilisation CPU des processus Apache et MySQL.
Mode U3
Peut être lancé depuis une clé USB.

