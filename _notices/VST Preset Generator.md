---
nom: "VST Preset Generator"
date_creation: "Mercredi, 4 janvier, 2017 - 14:03"
date_modification: "Mercredi, 4 janvier, 2017 - 14:03"
logo:
    src: "images/logo/VST Preset Generator.png"
site_web: "http://vst-preset-generator.org"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Français"
    - "English"
description_courte: "Générateur aléatoire de préréglages pour les greffons audios au format VST."
createurices: "François Mazen"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "audio"
    - "musique"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

VST Preset Generator est un utilitaire pour générer des fichiers de préréglage au format fxp ou fxb qui peuvent être lus par des greffons audios VST. Pour chaque fichier les valeurs des paramètres sont produits aléatoirement ou semi-aléatoirement à partir des informations du greffon. L'interface du logiciel permet de gérer la méthode aléatoire utilisée pour chaque paramètre ainsi que les bornes de sortie.
L’intérêt de cet outil est de découvrir de nouveaux sons à partir de greffons audios existants.

