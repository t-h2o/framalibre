---
nom: "Valgrind"
date_creation: "Mercredi, 23 janvier, 2019 - 21:15"
date_modification: "Vendredi, 8 février, 2019 - 16:06"
logo:
    src: "images/logo/Valgrind.png"
site_web: "http://www.valgrind.org/"
plateformes:
    - "GNU/Linux"
    - "Android"
langues:
    - "English"
description_courte: "Valgrind est un debugger permettant de détecter les erreurs mémoires dans un programme."
createurices: "Julian Seward"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "développement"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Valgrind"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Valgrind est un debugger permettant de détecter les erreurs mémoires dans un programme. Il peut aussi aider pour optimiser un programme. Il marche avec de nombreux langages de programmation (C, C++, Java, Python, Assembleur, Ada).

