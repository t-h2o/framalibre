---
nom: "Veracrypt"
date_creation: "Dimanche, 15 janvier, 2017 - 11:08"
date_modification: "Vendredi, 12 mai, 2023 - 08:28"
logo:
    src: "images/logo/Veracrypt.png"
site_web: "https://www.veracrypt.fr/en/Home.html"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Veracrypt permet de créer des conteneurs chiffrés pour y déposer des fichiers."
createurices: "IDRIX"
alternative_a: "Trucrypt, Bitlocker"
licences:
    - "Licence Apache (Apache)"
tags:
    - "système"
    - "vie privée"
    - "chiffrement"
    - "fichier"
    - "sécurité"
lien_wikipedia: "https://fr.wikipedia.org/wiki/VeraCrypt"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Veracrypt est l'un des successeurs de Truecrypt (dont le développement à été arrêté) activement développé.
Il permet de créer des conteneurs chiffrés sous forme de fichiers pour sécuriser des fichiers et des dossiers. Il peut relire des conteneurs Truecrypt, est capable de chiffrer des volume entier (clé usb et disque dur) et comporte des améliorations en terme de sécurité (PIM, chiffrement d'un disque système...).

