---
nom: "Vtiger CRM"
date_creation: "Mardi, 18 septembre, 2018 - 17:52"
date_modification: "Mardi, 11 mai, 2021 - 23:14"
logo:
    src: "images/logo/Vtiger CRM.jpg"
site_web: "https://www.crm-expert.fr"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Logiciel de Gestion de la relation client basé sur SugarCRM"
createurices: "vtiger"
alternative_a: "sugarcrm"
licences:
    - "Multiples licences"
tags:
    - "métiers"
    - "crm"
    - "gestion de la relation client"
    - "gestion commerciale"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Vtiger_CRM"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Vtiger CRM est un logiciel de gestion de la relation client (GRC). Il couvre les différentes facettes de la vie d'un client en entreprise/
vtiger CRM propose des modules couvrant les campagnes de marketing, l'activité des forces de vente, l'édition et l'envoi des devis / factures et le service client.
Vtiger CRM permet ainsi, en mode web, de mutualiser les connaissances client de l'entreprise tout en suivant votre activité commerciale, depuis l'avant-vente, en passant par la facturation jusqu'au traitement du service après-vente.

