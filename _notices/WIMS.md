---
nom: "WIMS"
date_creation: "Lundi, 19 avril, 2021 - 13:39"
date_modification: "Dimanche, 16 mai, 2021 - 12:49"
logo:
    src: "images/logo/WIMS.gif"
site_web: "https://wims.univ-cotedazur.fr"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Serveur de réalisations aléatoires de modèles de questions éducatives préparées en un format html/LaTeX."
createurices: "Xiao Gang"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "mathématiques"
    - "école"
    - "université"
lien_wikipedia: "https://fr.wikipedia.org/wiki/WIMS"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Serveur éducatif qui génère des réalisations de questions définies en un format html/LaTeX. Des paramètres numériques aléatoires sont inscrits dans les modèles afin que chaque réalisation soit quasiment unique. Le serveur vérifie que l'utilisateur donne la bonne réponse et note le résultat.
Ce système, développé depuis 1999 [1][2], est utilisé couramment (2021) dans nombreux universités et lycées en France et ailleurs. Un serveur WIMS est utilisable ou bien pour les travaux dirigés, ou bien pour les examens. Un réseau de serveurs miroirs est en accès libre pour les professeurs, élèves, étudiant-e-s et d'autres [3].
[1] Xiao, Gang, 1999, 'WIMS, A server for interactive mathematics on the internet', https://wims.univ-cotedazur.fr/paper/wims/wims.pdf
[2] Xiao, Gang, 2004, 'On public-question tests', http://wims.unice.fr/paper/pqt.pdf
[3] https://wims.auto.u-psud.fr/wims, https://wims.math.cnrs.fr/wims, https://wims.matapp.unimib.it/wims, http://wims-deq.urv.cat/wims

