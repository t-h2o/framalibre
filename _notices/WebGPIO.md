---
nom: "WebGPIO"
date_creation: "Samedi, 29 décembre, 2018 - 15:48"
date_modification: "Lundi, 7 septembre, 2020 - 14:48"
logo:
    src: "images/logo/WebGPIO.png"
site_web: "https://q37.info/s/a/WebGPIO"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
langues:
    - "English"
description_courte: "WebGPIO - piloter à distance les GPIOs d'un Raspberry Pi"
createurices: "Claude SIMON"
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "système"
    - "raspberry pi"
    - "web"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

WebGPIO est un logiciel permettant de piloter les GPIOs d'un Raspberry Pi (ou similaire, comme l'ODROID-C2) à partir d'un navigateur web. Ce logiciel est particulièrement pratique pour tester et dépanner les dispositifs électroniques branchés sur le RPi.
La plus grosse partie du logiciel est déportée sur un serveur faisant office d'intermédiaire entre le RPi et le navigateur web. Avec la configuration par défaut du logiciel, le navigateur web n'a pas besoin d'un accès direct au RPi. Le RPi n'a besoin que d'un accès vers internet, et ne nécessite pas d'être accessible depuis internet.
Par défaut, le logiciel se connecte à un serveur public libre d'accès. Les données transitant par ce serveur sont utilisées à des fins purement techniques et ne servent en aucune manière à pister les utilisateurs. Les sources du logiciel tournant sur le serveur sont disponibles, et peuvent être utilisés par qui désire mettre en place son propre serveur.

