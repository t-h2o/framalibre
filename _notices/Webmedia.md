---
nom: "Webmedia"
date_creation: "Lundi, 24 avril, 2017 - 21:12"
date_modification: "Jeudi, 5 janvier, 2023 - 19:22"
logo:
    src: "images/logo/Webmedia.png"
site_web: "https://doc.scenari.software/Webmedia"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
description_courte: "Webmedia permet de chapitrer et enrichir une vidéo ou une bande son."
createurices: ""
alternative_a: ""
licences:
    - "Licence CECILL (Inria)"
    - "Licence Publique Générale GNU (GNU GPL)"
    - "Licence publique générale limitée GNU (LGPL)"
    - "Licence Publique Mozilla (MPL)"
tags:
    - "multimédia"
    - "audio"
    - "vidéo"
    - "enrichissement audio"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Webmedia permet de chapitrer et enrichir une vidéo ou une bande son.
Le principe de fonctionnement est simple :

on importe sa vidéo ou sa bande son ;
on segmente le fichier lors d'une simple lecture ;
on ajoute des informations textuelles, des images… sur les segments précédemment marqués ;
on publie le résultat sous forme de site web.

Permet en outre de faire des diaporama commentés en enrichissant par des diapos une bande son (enregistrement de commentaire audio)…
Aussi : la vidéo d'un conférencier en train de parler devant un écran pourra être enrichie par ses diapos en deux coups de cuiller à pot.
Webmedia est un des modèles documentaires de Scenari.

