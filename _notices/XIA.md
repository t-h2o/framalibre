---
nom: "XIA"
date_creation: "Mardi, 14 mars, 2017 - 14:48"
date_modification: "Mardi, 9 juin, 2020 - 13:18"
logo:
    src: "images/logo/XIA.png"
site_web: "https://xia.funraiders.org"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
description_courte: "XIA est une extension Inkscape pour créer des images interactives et des mini-jeux en html5"
createurices: "Pascal Fautrero"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "image"
    - "html5"
    - "extension"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

XIA est un petit module (pour Inkscape) qui permet de générer des images interactives en html5. Inkscape est utilisé pour concevoir la ressource et XIA pour la génération du document final. Une image interactive est une image pour laquelle certains détails sont mis en valeur (par de simples zooms) et sont enrichis (accompagnés de textes, de sons et de vidéos). XIA dispose également d'un moteur de génération de mini-jeux en exploitant deux modes d'interaction : le simple clic ou le glisser/déposer.

