---
layout: notice

nom: "XMind"
date_creation: "Dimanche, 1 mai, 2016 - 15:13"
date_modification: "Mercredi, 28 avril, 2021 - 19:03"
logo: 
    src: "https://framalibre.org/sites/default/files/leslogos/Firefox_Screenshot_2017-04-06T17-29-10.354Z.png"
site_web: "http://www.xmind.net"
plateformes: "GNU/Linux, Mac OS X, Windows"
langues: "Multilingue"
description_courte: "Concepteur de cartes heuristiques (mind map)."
createurices: ""
licences: "Licence Publique Eclipse (EPL), Licence publique générale limitée GNU (LGPL)"
mots_clefs: "carte heuristique, mind mapping"
lien_wikipedia: "https://en.wikipedia.org/wiki/XMind"
lien_exodus: ""
---

XMind est un outil de création de cartes heuristiques.
carte heuristique
tableau
diagramme Ishikawa (cause-effet)
ligne de temps
export Image (PDF multi-page)
compatibilité freemind/freeplane
raccourcis claviers pour plier/déplier les branches
raccourcis claviers/souris pour se focaliser sur une branche
versionning intégré
notes (commentaire sur un noeud)
possibilité d'importer/fusionner des cartes
gestion par onglet
partage sur réseau local
Il existe une version Pro payante proposant des fonctionnalités complémentaires (licence propriétaire XMind)

