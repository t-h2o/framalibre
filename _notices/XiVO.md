---
nom: "XiVO"
date_creation: "Mercredi, 28 décembre, 2016 - 21:15"
date_modification: "Jeudi, 29 décembre, 2016 - 12:57"
logo:
    src: "images/logo/XiVO.jpg"
site_web: "http://xivo.solutions.fr"
plateformes:
    - "Android"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
description_courte: "L'alternative Open Source à votre téléphonie"
createurices: ""
alternative_a: "alcatel, cisco, mitel"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "système"
    - "voip"
lien_wikipedia: "https://fr.wikipedia.org/wiki/XiVO"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

XiVO est un logiciel libre (distribué sous licence GNU GPLv3) d'autocommutateur téléphonique privé (PBX) ou IPBX. XiVO utilise le logiciel libre Asterisk.
En octobre 2016, Avencall renforce son positionnement d’éditeur et regroupe l’ensemble de ses activités sous un même nom : XiVO.solutions. Cette transformation s’accompagne non seulement d’un nouveau site internet http://www.xivo.solutions/ mais surtout d’un changement majeur dans la production de ses logiciels. La suite logicielle XiVO.solution est ainsi composée de 3 modules : XiVO téléphonie d’entreprise, XiVO Unified Communications (UC) et XiVO Contact Center (CC). Pour une meilleure lisibilité, elle est dorénavant produite sous une version unique  nommée « vAnnée.XX» au rythme d’une version toutes les six semaines.

