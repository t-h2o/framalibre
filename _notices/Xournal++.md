---
nom: "Xournal++"
date_creation: "Jeudi, 9 avril, 2020 - 01:18"
date_modification: "Jeudi, 27 octobre, 2022 - 23:35"
logo:
    src: "images/logo/Xournal++.png"
site_web: "https://github.com/xournalpp/xournalpp"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Pour prendre notes manuscrites et annoter des pdf sur tablette graphique."
createurices: "Andreas Butti Wilson Brenna (since 2012) Marek Pikuła (since 2015) Moreno Razzoli (since 2018) Luca Errani (since 2018) Ulrich Huber (since 2019) Justin Jones (since 2019) Bryan Tan (since 2019) Justus Rossmeier (since 2019) Fabian Keßler (since 2019) lehmanju (since 2019) Roland Lötscher (since 2020)"
alternative_a: "Adobe Acrobat Reader, GoodNotes, OneNote"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "pdf"
    - "édition"
    - "tablette graphique"
    - "note"
    - "prise de notes"
    - "productivité"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
---

Xournal++ sert à annoter un document pdf pour y ajouter des marques, des commentaires, surligner certains passages ou encore faire quelques corrections.  Particulièrement utile lorsque l'on fait des relectures et qu'on veut transmettre des des remarques sur le document. Le document retouché est exportable dans le format de Xournal pour des retouches ultérieures ou en pdf pour « embarquer » les annotations. En pédagogie et en formation, on peut l'utiliser pour apprécier des restitutions qui ont été scannées en pdf. Aussi, il peut être utilisé comme outil de prise de notes manuscrites. À la fin, le fichier peut être exporté en pdf, en tant qu'image vectoriel ou matriciel ou au format xopp qui permet d'être modifiable sur le long terme.

