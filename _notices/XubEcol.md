---
nom: "XubEcol"
date_creation: "Mardi, 23 mai, 2017 - 23:54"
date_modification: "Mercredi, 12 mai, 2021 - 15:37"
logo:
    src: "images/logo/XubEcol.png"
site_web: "https://www.xubecol.fr/index.php"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
description_courte: "Linux pour les écoles, des ordinateurs plus sûrs, plus rapides."
createurices: "Bruno DAVID"
alternative_a: "Microsoft Windows, MacOS"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "école"
    - "distribution gnu/linux"
    - "ordinateur"
    - "réemploi"
    - "système d'exploitation (os)"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

XubEcol est une personnalisation de la distribution GNU/Linux Xubuntu pour remplacer Windows XP sur des ordinateurs obsolètes dans des écoles rurales du département de l'Hérault.
Trois sessions sont pré-configurées : élève, prof et directeur.
Plutôt que d'ajouter de nombreux logiciels éducatifs, une sélection d'activités en ligne est proposée sur une page d'accueil paramétrable.

