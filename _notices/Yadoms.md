---
nom: "Yadoms"
date_creation: "Mercredi, 6 mai, 2020 - 23:30"
date_modification: "Samedi, 9 mai, 2020 - 13:09"
logo:
    src: "images/logo/Yadoms.png"
site_web: "http://yadoms.com/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
description_courte: "Logiciel de domotique complet et multi-plateformes"
createurices: "Jean-Michel Decoret, Nicolas Hilaire, Jean-Michel Berhault, Sébastien Gallou"
alternative_a: "Jeedom, eedomus"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "science"
    - "maison"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Outil idéal pour la maison, automatiser l'ouverture des volets, allumer le chauffage à distance, surveiller les consommations, optimiser ses factures énergétiques, suivre l'environnement du foyer (températures, humidité, etc...), etc...
Utilisation limitée que par l'imagination...
Caractéristiques principales :
- Tableau de bord web
- Automatisation graphique ou texte
- Support de nombreux protocoles radios ou filaires : ZWave, EnOcean, Blyss, Chacon, X10, OneWire, etc...
- Nombreux plugins disponibles
- Nombreux widgets graphiques
- Multi-plateformes : Windows, Linux (x86 ou Raspberry), MacOs

