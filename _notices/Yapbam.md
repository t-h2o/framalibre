---
nom: "Yapbam"
date_creation: "Samedi, 7 janvier, 2017 - 15:31"
date_modification: "Samedi, 25 avril, 2020 - 16:21"
logo:
    src: "images/logo/Yapbam.png"
site_web: "https://www.yapbam.net"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Simple, facile et très utile au quotidien pour gérer ses comptes, économiser, ..."
createurices: ""
alternative_a: "Microsoft Money, Quicken"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "gestion"
    - "finances"
    - "comptabilité"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Yapbam, en plus d'être simple et entièrement gratuit, est très utile. Les mises à jours sont simples à réaliser. Le transfert d'argent d'un compte à un autre est suivi, et le choix des catégories est libre. Le logiciel est graphiquement sobre mais terriblement efficace.
Si vous n'aimez pas faire vos comptes, c'est ce logiciel qu'il vous faut. 10' chrono, 2 fois par mois, et fini les galères, bonjour les projets !
Reste plus qu'à vous pencher dessus au démarrage.
ABBA :
Money, money, money
Must be funny
In the rich man's world

