---
nom: "Yunohost"
date_creation: "Mardi, 13 décembre, 2016 - 10:49"
date_modification: "Jeudi, 9 août, 2018 - 06:37"
logo:
    src: "images/logo/Yunohost.png"
site_web: "https://yunohost.org/"
plateformes:
    - "GNU/Linux"
    - "Web"
langues:
    - "Autres langues"
description_courte: "Une solution facile pour gérer l'auto-hébergement d'applications web (site, blog, cloud, etc.)"
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "auto-hébergement"
    - "décentralisation"
lien_wikipedia: "https://fr.wikipedia.org/wiki/YunoHost"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
---

YunoHost, signifiant « Pourquoi ne pas s’héberger ? », est une distribution basée sur Debian GNU/Linux composée, essentiellement, de logiciels libres, ayant pour objectif de faciliter l’auto-hébergement.
YunoHost facilite l’installation et l’utilisation d’un serveur personnel. Autrement dit, YunoHost permet d’héberger sur sa machine et chez soi son courrier électronique, son site web, sa messagerie instantanée, son agrégateur de nouvelles (flux RSS), son partage de fichiers, sa seedbox et bien d'autres…

