---
nom: "Zotero"
date_creation: "Mercredi, 4 janvier, 2017 - 13:02"
date_modification: "vendredi, 11 août, 2023 - 23:45"
logo:
    src: "images/logo/Zotero.png"
site_web: "https://www.zotero.org/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Zotero permet de collecter, annoter, gérer et citer vos différentes sources, documents électroniques ou papier"
createurices: "Dan Cohen"
alternative_a: "EndNote, Mendeley"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "bibliographie"
    - "référence bibliographique"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Zotero"
lien_exodus: ""
identifiant_wikidata: "Q226915"
mis_en_avant: "oui"
---

Zotero est un logiciel de gestion de références gratuit, libre et open source. À l'origine, il est une extension pour le navigateur Firefox ; il est maintenant disponible en version autonome (standalone)
Il permet de gérer des données bibliographiques et des documents de recherche, de les organiser, partager, synchroniser. Zotero génère aussi les citations (notes et bibliographies) dans un texte rédigé depuis les logiciels de traitement de textes comme LibreOffice grâce à un plugin.


