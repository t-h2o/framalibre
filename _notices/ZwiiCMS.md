---
nom: "ZwiiCMS"
date_creation: "Jeudi, 7 mai, 2020 - 08:57"
date_modification: "Samedi, 2 octobre, 2021 - 11:02"
logo:
    src: "images/logo/ZwiiCMS.png"
site_web: "https://zwiicms.com/"
plateformes: "GNU/Linux, Mac OS X, Windows"
langues: "Multilingue"
description_courte: "ZwiiCMS, le CMS simple, léger, sans base de données (flat-file), modulable, responsive et multilingue !"
createurices: "Développé par Rémi Jean de 2008 à 2018, désormais maintenu par Frédéric Tempez et toute la communauté."
licences: "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "CMS"
    - "blog"
    - "création de site web"
lien_wikipedia: ""
lien_exodus: ""
alternative_a: ""
mis_en_avant: "non"
---

Créez facilement autant de pages statiques ou articles de blog que nécessaire grâce à un éditeur de texte complet et un gestionnaire de fichiers et médias intuitif.
Personnalisez votre site en quelques clics : couleurs, tailles, polices de caractères, bannière, tout y est, avec un résultat nativement adapté à toutes les tailles d'écran !
Insérez de magnifiques galeries d'images ou de photos en un minimum de manipulation.
Développez une communauté de rédacteurs et d'administrateurs : Zwii est multi-utilisateurs et multi-langues, avec au choix :
° Traduction rédigée dans une langue européenne
° Traduction automatique dans une langue européenne
° Traduction selon la langue navigateur

