---
nom: "Akira"
date_creation:
logo: 
    src: "https://upload.wikimedia.org/wikipedia/commons/6/64/Akira_UX_logo_button.svg"
site_web: "https://github.com/akiraux/Akira"
systeme_exploitation: "GNU/Linux"
langues: "anglais"
description_courte: "Le premier logiciel de prototypage pour les designers qui veulent être libres !"
createurices:
licence:
mots_clefs: "design,ux,ui,prototypage,maquettage"
lien_wikipedia:
lien_exodus:
---

Akira UX, c'est un logiciel de prototypage !

Akira se concentre sur une approche moderne et rapide de la conception d'interface utilisateur et d'interface utilisateur, ciblant principalement les concepteurs de sites Web et les graphistes. Le but principal est d'offrir une solution valide et professionnelle pour les designers qui veulent utiliser Linux comme leur OS principal.
