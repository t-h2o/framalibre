---
nom: "AntennaPod"
date_creation:
logo: 
    src: "https://framalibre.org/sites/default/files/styles/thumbnail/public/leslogos/antennapod-logo.png?itok=zCYb1-OB"
site_web: "http://antennapod.org/"
systeme_exploitation: "Android"
langues: "français, anglais, multilingue"
description_courte: "Une chouette application pour vos podcasts favoris 🎧"
createurices: "Daniel Oeh"
licence: "Licence MIT/X11"
mots_clefs: "podcast, audio, musique"
lien_wikipedia: "https://en.wikipedia.org/wiki/AntennaPod"
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/search/de.danoeh.antennapod/"

---

AntennaPod c'est pour écouter des podcasts radicaux TMTC !

AntennaPod est un lecteur et gestionnaire de podcast permettant l'accès à des millions de podcast gratuits ou payants produit aussi bien par des podcasters indépendants que de gros éditeurs.

Ajoutez, importez et exportez leurs flux facilement à partir d'ITunes, de fichiers OPML ou simplement à partir de liens RSS.

Grace à son lecteur RSS, il vous avertira également de tous les nouvelles vidéos qui seront disponible sur vos chaînes Youtube et Dailymotion favorites.
