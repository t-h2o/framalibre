---
nom: "apple flinger"
date_creation: "Vendredi, 14 février, 2020 - 22:00"
date_modification: "Vendredi, 14 février, 2020 - 22:00"
logo:
    src: "images/logo/apple flinger.png"
site_web: "https://gitlab.com/ar-/apple-flinger"
plateformes:
    - "Android"
langues:
    - "Autres langues"
description_courte: "Jeu de lance-pierre type angry birds en solo ou multi-joueurs."
createurices: "Andreas Redmer"
alternative_a: "Angry Bird"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "arcade"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/com.ardash.appleflinger.android…"
identifiant_wikidata: ""
mis_en_avant: "non"
---

Tirez des pommes à l'aide d'une fronde. Soyez le premier à détruire toute la base ennemie, mais sachez que l'autre côté riposte. Ce jeu a un mélange équilibré de puzzle, de stratégie, de patience et d'action.
Mode de jeu en solo, contre IA ou en 1v1.

