---
nom: "darktable"
date_creation: "Lundi, 26 décembre, 2016 - 16:18"
date_modification: "Mercredi, 12 mai, 2021 - 15:27"
logo:
    src: "images/logo/darktable.png"
site_web: "http://www.darktable.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Un puissant logiciel de traitement photo spécialisé dans le format RAW."
createurices: "Henrik Andersson, Johannes Hanika et différents contributeurs"
alternative_a: "Adobe Lightroom, Adobe Bridge, Apple Aperture, Apple Photo, Apple iPhoto"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "photo"
    - "raw"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Darktable"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
---

Darktable est un logiciel de traitement photo et catalogueur d’images, spécialisé dans le format RAW. Il dispose d'une "table lumineuse", d'une "chambre noire", et de dizaines de modules de traitement d'image.
Il travaille de façon non-destructive, c’est à dire que les changements opérés sur les images sont enregistrés pour pouvoir être reproduits, mais l’image originale elle-même n’est jamais écrasée.
Il importe tous les formats d'image usuels (RAW, JPEG, CR2, HDR, PFM, etc) et exporte vers plusieurs formats 8 bits (JPEG, PNG, TIFF), 16 bits (PPM, TIFF), HDR (PFM, EXR) et vers des collections en ligne ou locales. Il génère simplement des galeries d'images en HTML.
Il supporte de nombreux appareils photos numériques par défaut (voir http://www.darktable.org/resources/camera-support/).
Le développement autour du logiciel est très actif. On compte plus de 200 contributeurs à ce jour, dont des chercheurs universitaire qui apportent leurs derniers travaux en retouche d'image.

