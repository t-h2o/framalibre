---
nom: "editInteractiveSVG"
date_creation: "Dimanche, 3 juin, 2018 - 23:45"
date_modification: "Dimanche, 10 juin, 2018 - 03:04"
logo:
    src: "images/logo/editInteractiveSVG.png"
site_web: "https://mothsart.github.io/editeur-svg-interactif.html"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
description_courte: "Editer un fichier SVG statique afin de le présenter sous une forme interactive."
createurices: "mothsart"
alternative_a: ""
licences:
    - "Berkeley Software Distribution License (BSD)"
tags:
    - "éducation"
    - "dessin vectoriel"
    - "svg"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Editer un fichier SVG statique afin de le présenter sous une forme interactive.
Objectifs :

Créer des illustrations interactives avec toujours le même gabarit : une légende contenant des indices titrés et un descriptif pour une zone de l'illustration.


Permettre des zooms sur les zones décrites


Avoir un contenu responsive (vu que le contenu est pleinement vectoriel, pourquoi s'en priver)


Être et rester simple d'utilisation (KISS et philosophie Unix) et productif pour enrichir une illustration à vocation pédagogique.


Rester simple à développer et maintenir : un mode Debug permet justement d'étendre le logiciel afin de mieux le tester.


Utilisation hors ligne et intégré dans une distribution Linux via un paquet .deb


