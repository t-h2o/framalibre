---
nom: "Firefox"
date_creation: 22/11/2021
logo: 
    src: "https://framalibre.org/sites/default/files/styles/thumbnail/public/leslogos/Firefox_logo_2019png.png?itok=8PXNDkG8"
site_web: "https://www.mozilla.org/fr/firefox/"
systeme_exploitation: "Windows, MacOS, GNU/Linux, Android, Apple iOS, /e/"
langues: "français, anglais, multilingue"
description_courte: "Un navigateur pour aller sur Internet et y faire tout ce que vous souhaitez en toute tranquillité."
createurices: Mozilla
licence: "Licence Publique Générale GNU (GNU GPL), Licence publique générale limitée GNU (LGPL), Licence Publique Mozilla (MPL)"
mots_clefs: "navigateur web, web, internet"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Mozilla_Firefox"
lien_exodus: "https://www.mozilla.org/fr/firefox/"
---

Firefox est la meilleure navigatrice web !

Diffusé depuis 2002, Mozilla Firefox est un navigateur web respectueux des standards et des données privées. Il est personnalisable grâce à des centaines d'extensions disponibles au téléchargement depuis le site officiel. Grâce aux milliers de contributeurs à ce logiciel libre, Firefox est en constante évolution et les mises à jour sont fréquentes.

À noter toutefois que la fondation Mozilla est presque entièrement financée par des accords avec de grands moteurs de recherche (comme Yahoo ou Google) pour que ces moteurs apparaissent dans la barre de recherche de Firefox. Mais ces conditions ne sont pas exclusives : en 2016, Mozilla a noué un partenariat avec Qwant qui partage les mêmes points de vue concernant le respect de la vie privée. On peut changer très facilement le réglage du moteur par défaut dans Firefox pour installer les moteurs de recherche que l'on souhaite.

