---
nom: "nnn"
date_creation: "Dimanche, 3 mai, 2020 - 16:08"
date_modification: "Vendredi, 30 juillet, 2021 - 11:17"
logo:
    src: "images/logo/nnn.jpg"
site_web: "https://github.com/jarun/nnn"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "nnn est un gestionnaire de fichiers de terminal portable écrit en C."
createurices: ""
alternative_a: ""
licences:
    - "Open Software License (OSL)"
tags:
    - "système"
    - "explorateur de fichier"
    - "cli"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

nnn est un gestionnaire de fichiers de terminal portable écrit en C. Il est facilement extensible via son système de plugins où vous pouvez ajouter vos propres scripts aux côtés de plugins déjà disponibles. Un plugin (néo) vim est également disponible.
En plus d'être un gestionnaire de fichiers, nnn est également un analyseur d'utilisation de disque, un lanceur d'applications floues, un renameur de fichiers batch et un sélecteur de fichiers.
nnn prend en charge la recherche instantanée au fur et à mesure que vous tapez avec des filtres regex (ou chaîne simple) et un mode de navigation au fur et à mesure pour une navigation continue en mode filtre avec sélection automatique de répertoire. Il prend en charge les contextes, les signets, plusieurs options de tri, SSHFS, les opérations par lots sur les sélections (un groupe de fichiers sélectionnés) et bien plus encore.
Malgré ses capacités, nnn est conçu pour être facile à utiliser.

