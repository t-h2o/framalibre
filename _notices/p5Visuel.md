---
nom: "p5Visuel"
date_creation: "Dimanche, 28 février, 2021 - 00:27"
date_modification: "Dimanche, 28 février, 2021 - 01:02"
logo:
    src: "images/logo/p5Visuel.png"
site_web: "http://p5Visuel.info"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Apple iOS"
langues:
    - "Français"
description_courte: "p5Visuel est un environnement de programmation visuelle destiné aux débutants."
createurices: "André Boileau"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "programmation"
    - "apprentissage"
    - "mathématiques"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

p5Visuel est un langage de programmation visuelle doté d'extensions relatives aux pages web et aux mathématiques. Il a été conçu notamment pour résoudre certains problèmes rencontrés avec Scratch :  scène limitée à 480 x 360 pixels, difficulté d'utiliser des notations mathématiques, lourdeur du recours à des tableaux de données à plusieurs colonnes, etc.
Il vise la création de pages web fortement interactives. Il ne nécessite pas de connaître quoi que ce soit de la mécanique des pages web (HTML, CSS et JavaScript), mais que ceux et celles qui disposent de telles connaissances pourraient les utiliser pour aller plus loin.
Mentionnons que p5Visuel s'utilise sur le web, mais qu'il existe aussi une version autonome que vous pouvez télécharger. Il est doté d'une documentation en ligne, comprenant notamment des vidéos de formation pour débutants, ainsi que des exemples de programmes et de problèmes à résoudre.

