---
nom: "pyRenamer"
date_creation: "Lundi, 30 octobre, 2017 - 18:01"
date_modification: "Mercredi, 12 mai, 2021 - 14:44"
logo:
    src: "images/logo/pyRenamer.png"
site_web: "https://launchpad.net/pyrenamer"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Renommer des groupes de fichiers en une seule fois."
createurices: "Adolfo González Blázquez"
alternative_a: "ReNamer"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "utilitaire"
    - "renommage de fichiers"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

pyRenamer permet de renommer plusieurs fichiers d’un seul coup.
Il peut remplacer une chaîne de caractère par une autre, d’en insérer ou d’en supprimer une à un endroit spécifique et de renommer des fichiers images ou audios selon leurs méta-données (tags).
Il est aussi possible de dater et incrémenter automatiquement le nom de fichier ou encore de changer des points en espaces et de passer de majuscules à minuscule et vice-versa.

