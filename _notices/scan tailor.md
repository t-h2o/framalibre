---
nom: "scan tailor"
date_creation: "Mercredi, 7 décembre, 2022 - 21:07"
date_modification: "Jeudi, 29 décembre, 2022 - 10:12"
logo:
    src: "images/logo/scan tailor.png"
site_web: "https://scantailor.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Scan Tailor est un outil de post-traitement interactif et simple pour les pages numérisées"
createurices: "Nate Craun"
alternative_a: "Microsoft Lens"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

Scan Tailor est un outil de post-traitement interactif et simple pour les pages numérisées qui a été lancé en 2007. Il effectue des opérations telles que le fractionnement de page, le redressement, l'ajout/la suppression de bordures, variation de la résolution DPI, etc. Vous lui donnez des scans bruts et vous obtenez des pages prêtes à être imprimées ou assemblées dans un fichier PDF ou DJVU. La numérisation, la reconnaissance optique de caractères et l'assemblage de documents de plusieurs pages sont hors de portée de ce logiciel. Vous obtiendrez des fichiers images. Il vous faudra un logiciel tiers pour faire le PDF ou DJVU.  Il est écrit en C++ avec Qt.
Le résultat avec des scans mal faits comme ceux de Google Books, il fait des merveilles pour les OCR.

