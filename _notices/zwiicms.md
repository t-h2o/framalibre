---
nom: "zwiicms"
date_creation: "Dimanche, 28 mai, 2023 - 18:45"
date_modification: "Dimanche, 28 mai, 2023 - 18:45"
logo:
    src: "images/logo/zwiicms.png"
site_web: "https://zwiicms.com/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un CMS pour créer et gérer facilement son site web
sans aucune connaissance en programmation"
createurices: "Frederic Tempez"
alternative_a: "Wordpress, Joomla"
licences:
    - "Creative Commons -By"
tags:
    - "cms"
    - "création site web"
    - "sans base de données"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
---

À l'aide de son interface complète et simple, créer et maintenir votre site web devient un jeu d'enfant.
Adapté aux débutants sans aucune connaissance techniques et aux connaisseurs qui ont accès à de nombreuses possibilités de personnalisations.
Sans base de données
Aucune base de données SQL à installer. Le contenu de votre site tient dans un jeu de fichiers au format texte (JSON) faciles à sauvegarder et à restaurer.
Repose sur une structure moderne, compatible avec les tablettes et les smartphones.
Éditeur de texte
L'éditeur de texte visuel vous permettra une mise en page simple et efficace sans aucune connaissance en programmation !
Gestionnaire de fichiers
Gérez vos fichiers ultra simplement depuis un gestionnaire de fichiers complet ! Un glissé-déplacé suffit pour téléverser vos documents.
Modulable
Des modules livrés à l'installation enrichissent vos pages en fonction de vos besoins : formulaire de contact, news, galerie photos, etc.

