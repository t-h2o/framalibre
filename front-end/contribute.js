//@ts-check

import { text, json } from "d3-fetch";
import { format } from 'date-fns';
import { fr } from 'date-fns/locale';

import createNoticeContent from '../shared/createNoticeContent.js'

const baseUrl = document.documentElement.getAttribute('data-base-relative')

const TAG_INPUT_ELEMENTS_SELECTOR = `[name="tag"]`;

const form = document.querySelector(`form.contribute`)
if (!form)
    throw new Error('form.contribute element could not be found, cannot contribute')

const dateCreationEl = form.querySelector(`[name="date_creation"]`)
const logoSrcEl = form.querySelector(`[name="logo_src"]`)
const nomEl = form.querySelector(`[name="nom"]`)
const logoEl = form.querySelector(`[name="logo"]`)
const siteWebEl = form.querySelector(`[name="site_web"]`)
const plateformesEls = form.querySelectorAll(`[name="plateformes"]`)
const languesEls = form.querySelectorAll(`[name="langues"]`)
const descriptionCourteEl = form.querySelector(`[name="description_courte"]`)
const createuricesEl = form.querySelector(`[name="createurices"]`)
const alternativeAEl = form.querySelector(`[name="alternative_a"]`)
const licencesEl = form.querySelector(`[name="licences"]`)
const lienWikipediaEl = form.querySelector(`[name="lien_wikipedia"]`)
const lienExodusEl = form.querySelector(`[name="lien_exodus"]`)
const identifiantWikidataEl = form.querySelector(`[name="identifiant_wikidata"]`)
const descriptionLongueEl = form.querySelector(`[name="description_longue"]`)
const contributeuriceEl = form.querySelector(`[name="contributeur.rice"]`)
const submitButton = document.querySelector('button[type="submit"]')

function titleToFilePath(title) {
    return title.replace(/\//g, '-').replace(/#/g, '-').replace(/\?/g, '-')
}

// const CONTIBUTION_SERVER_ORIGIN = 'http://localhost:8080' // dev
const CONTIBUTION_SERVER_ORIGIN = 'https://contribuer.framalibre.org' // prod


const paramsSearch = new URLSearchParams(window.location.search)
const noticeToEdit = paramsSearch.get('notice')

/** @type {Notice} */
let notice;

let action = 'create';


if (noticeToEdit) {
    const noticeURL = `${baseUrl}notices/${encodeURIComponent(noticeToEdit)}.json`

    json(noticeURL)
        .then(_notice => {
            //@ts-ignore
            notice = _notice;
            action = 'update'
            dateCreationEl.value = notice.date_creation
            logoSrcEl.value = notice.logo.src || ''
            nomEl.value = notice.nom
            siteWebEl.value = notice.site_web || ''

            for(const plateformEl of plateformesEls){
                if(notice.plateformes.includes(plateformEl.value)){
                    plateformEl.checked = true
                }
            }

            for(const langueEl of languesEls){
                if(notice.langues.includes(langueEl.value)){
                    langueEl.checked = true
                }
            }

            descriptionCourteEl.value = notice.description_courte || ''
            createuricesEl.value = notice.createurices || ''
            alternativeAEl.value = notice.alternative_a || ''
            const licencesOptions = [...licencesEl.options]
            licencesOptions.forEach(o => {
                if (notice.licences.includes(o.value)) {
                    o.selected = true
                }
            })
            lienWikipediaEl.value = notice.lien_wikipedia || ''
            lienExodusEl.value = notice.lien_exodus || ''
            descriptionLongueEl.value = notice.raw_content || ''
            
            notice.tags.forEach((tag, i) => {
                /** @type {NodeListOf<HTMLInputElement>} */
                const tagsEls = form.querySelectorAll(TAG_INPUT_ELEMENTS_SELECTOR)

                /** @type {HTMLInputElement?} */
                let tagInputElement = tagsEls[i]

                if (tagInputElement === undefined) {
                    tagInputElement = tagsEls[0].cloneNode();
                    [...tagsEls].at(-1).after(tagInputElement)
                }

                tagInputElement.value = tag
            })

            submitButton.textContent = 'Modifier la notice'
        })
        .catch(err => {
            console.error('TODO: handle error case (404, etc.)', err)
        })
}

form.addEventListener('submit', async e => {
    e.preventDefault();

    submitButton.disabled = true
    submitButton.textContent = 'Envoi en cours...'

    const nom = nomEl.value;
    const site_web = siteWebEl.value;
    const plateformes = [...plateformesEls].filter(el => el.checked).map(o => o.value);
    const langues = [...languesEls].filter(el => el.checked).map(o => o.value);
    const description_courte = descriptionCourteEl.value;
    const createurices = createuricesEl.value;
    const alternative_a = alternativeAEl.value;
    const licences = [...licencesEl.selectedOptions].map(o => o.value);

    const tagsEls = form.querySelectorAll(TAG_INPUT_ELEMENTS_SELECTOR)
    const tags = [...tagsEls]
        .map(el => el.value)
        .filter(t => t.length >= 1)

    const lien_wikipedia = lienWikipediaEl.value;
    const lien_exodus = lienExodusEl.value;
    const identifiant_wikidata = identifiantWikidataEl.value;
    const description_longue = descriptionLongueEl.value;
    const contributeurice = contributeuriceEl.value
    const date_modification = format(
        new Date(),
        "EEEE, d MMMM, yyyy - HH:mm",
        { locale: fr }
    )
    const date_creation = noticeToEdit ? dateCreationEl.value : date_modification
    const hasLogo = logoEl.files.length > 0
    let imageFilePath = null
    let base64Logo = null
    if (hasLogo) {
        const logo = await logoEl.files[0].arrayBuffer();
        const extension = logoEl.files[0].name.split('.').pop();

        base64Logo = btoa(new Uint8Array(logo).reduce((data, byte) => data + String.fromCharCode(byte), ''));
        imageFilePath = `images/logo/${titleToFilePath(nom)}.${extension}`
    } else if (noticeToEdit && logoSrcEl.value) {
        imageFilePath = logoSrcEl.value
    }


    const contenu = createNoticeContent({
        nom,
        date_creation,
        date_modification,
        logo: { src: imageFilePath },
        site_web,
        plateformes,
        langues,
        description_courte,
        createurices,
        alternative_a,
        licences,
        tags,
        lien_wikipedia,
        lien_exodus,
        identifiant_wikidata,
        description_longue,
        mis_en_avant: (notice && notice.mis_en_avant) || false
    })

    const file_path = `_notices/${titleToFilePath(nom)}.md`
    const previous_path = notice && `_notices/${titleToFilePath(notice.nom)}.md`

    const differentContentFile = notice && titleToFilePath(nom) !== titleToFilePath(notice.nom)

    const actions = [{
        action: differentContentFile ? 'move' : (notice ? 'update' : 'create'),
        file_path,
        previous_path,
        content: contenu,
        encoding: 'text'
    }]

    if (base64Logo && imageFilePath) {
        const previousImageFilePath = notice && notice.logo && notice.logo.src

        actions.push({
            action: imageFilePath !== previousImageFilePath ? 'move' : (notice ? 'update' : 'create'),
            file_path: imageFilePath, 
            previous_path: previousImageFilePath,
            content: base64Logo, 
            encoding: 'base64'
        })
    }

    text(`${CONTIBUTION_SERVER_ORIGIN}/contribute`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            contributor: contributeurice,
            actions
        })
    }).then(() => {

        window.location.href = noticeToEdit ? 
            `${baseUrl}contribuer/confirmation_de_modification` : 
            `${baseUrl}contribuer/confirmation_de_creation`

    }).catch(err => {
        console.error(`Erreur lors de l'envoi de la contribution au serveur de contribution`, err)

        submitButton.disabled = false

        submitButton.textContent = noticeToEdit ? 'Modifier la notice' : 'Créer la notice !';

        const alertEl = form.querySelector(`#alert`)
        alertEl.hidden = false

        alertEl.innerHTML = `
        <i class="fas fa-circle-xmark"></i>
        <strong>Il y a un souci de notre côté.</strong>
        Vous pouvez réessayer
        dans une heure ou demain. Si le problème persiste, vous
        pouvez
        <a href="https://contact.framasoft.org/fr/#framalibre">contacter
        l'équipe de Framalibre</a>.
        `
    })
})
