---
layout: default
styles:
    - href: "style/welcome.css"
scripts:
    - src: "build/welcome.js"
      defer: true
---

{% assign welcomeTags = "association,bloqueur de publicité,création de site web,dessin,fediverse,formulaire,gestion de projet,jeu,mail,texte,travail collaboratif,vidéo,visioconférence" | split: "," %}

<h1>
  <span class="fl-animated-title">
    <span class="fl-animated-title__fixed-item">Trouvez</span>
    <span class="fl-animated-title__fixed-item">
      <span class="fl-animated-title__moving-item">un outil</span>
      <span aria-hidden="true">,</span>
      <span class="fl-animated-title__moving-item">une appli</span>
      <span aria-hidden="true">,</span>
      <span class="fl-animated-title__moving-item">du matériel</span>
      <span aria-hidden="true">,</span>
      <span class="fl-animated-title__moving-item">un logiciel</span>
    </span>
    <span class="fl-animated-title__fixed-item">libre</span>
  </span>
</h1>

<div class="fl-accueil row mt-5--on-lg mb-2">
  <div class="col-lg-8">
    <h2 class="text-center">Via les étiquettes…</h2>

    <ul class="fl-tags">
      {% for tag in welcomeTags -%}
        <li class="fl-tags__items">{%- include tag-link.html tag=tag -%}</li>
      {%- endfor -%}
    </ul>

    <h2 class="text-center">Ou plutôt la recherche !</h2>

    <section
      class="mx-auto my-0 rounded fb-g2 py-3 px-2 col-sm-8 col-lg-6 col-10"
    >
      <div class="row mx-0">
        <label class="text-right fc-g7 pt-1 px-1 col-2" for="services-search">
          <i class="fl-magnifying-icon fas fa-lg"></i>
          <span class="sr-only">Rechercher</span>
        </label>
        <div class="px-1 col-10">
          <input
            id="services-search"
            type="search"
            placeholder="Rechercher"
            class="fl-search form-control"
          />
        </div>
      </div>
    </section>
  </div>
</div>

<output></output>
<!--
  Et genre aussi y'a
   - [une page avec tous les mots-clefs](./all-tags)
   - [une page avec tous les mots-clefs pour lesquels il n'y a qu'une seule page](./tag-une-notice)
-->
