---
title: Maiwann — Téléphone dégooglisé
layout: default
styles:
    - href: "style/notice.css"
    
---

# Maiwann : Téléphone dégooglisé

## Pour démarrer


![](https://framalibre.org/sites/default/files/styles/thumbnail/public/leslogos/Firefox_logo_2019png.png?itok=8PXNDkG8)  [Firefox](https://davidbruant.frama.io/brouillon-framalibre/notices/firefox.html)

Pour naviguer sur internet, auquel on rajoute…

![](https://davidbruant.frama.io/brouillon-framalibre/images/logo/uBlock%20Origin.png) [uBlock Origin](https://davidbruant.frama.io/brouillon-framalibre/notices/ublock-origin.html)

Un bloqueur de publicité pour naviguer plus vite sur le web !





## Mes applications chouchous

![](https://davidbruant.frama.io/brouillon-framalibre/images/logo/F-Droid.png) [F-Droid](https://davidbruant.frama.io/brouillon-framalibre/notices/f-droid.html)

Pour avoir accès à toutes les applications libres !


![](https://davidbruant.frama.io/brouillon-framalibre/images/logo/NewPipe.png) [New Pipe](https://davidbruant.frama.io/brouillon-framalibre/notices/newpipe.html)

Pour briller en société, une application pour avoir accès à tout YouTube (et PeerTube) sans publicité !
