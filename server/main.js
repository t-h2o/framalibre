//@ts-check
import Fastify from 'fastify';
import fastifyCORS from '@fastify/cors';

import { Projects, Commits, MergeRequests } from '@gitbeaker/node'; // Just the Project Resource


const { FRAMAGIT_ORIGIN, FRAMAGIT_TOKEN, FRAMALIBRE_FRAMAGIT_PROJECT_ID, HOST, PORT: _PORT } = process.env

if (!FRAMAGIT_ORIGIN) {
    console.error(`Il manque la variable d'environnement 'FRAMAGIT_ORIGIN'`)
    process.exit(1)
}
if (!FRAMAGIT_TOKEN) {
    console.error(`Il manque la variable d'environnement 'FRAMAGIT_TOKEN'`)
    process.exit(1)
}
if (!FRAMALIBRE_FRAMAGIT_PROJECT_ID) {
    console.error(`Il manque la variable d'environnement 'FRAMALIBRE_FRAMAGIT_PROJECT_ID'`)
    process.exit(1)
}
if (!HOST) {
    console.error(`Il manque la variable d'environnement 'HOST'`)
    process.exit(1)
}
if (!_PORT) {
    console.error(`Il manque la variable d'environnement 'PORT'`)
    process.exit(1)
}

const PORT = parseInt(_PORT)

if (!Number.isSafeInteger(PORT) || PORT < 0) {
    console.error(`PORT n'est pas un numéro de port valide (${_PORT})`)
    process.exit(1)
}




const projects = new Projects({
    host: FRAMAGIT_ORIGIN,
    token: FRAMAGIT_TOKEN
})
const commits = new Commits({
    host: FRAMAGIT_ORIGIN,
    token: FRAMAGIT_TOKEN
})
const mrs = new MergeRequests({
    host: FRAMAGIT_ORIGIN,
    token: FRAMAGIT_TOKEN
})

let project
let mainBranch

try {
    project = await projects.show(FRAMALIBRE_FRAMAGIT_PROJECT_ID)
    mainBranch = await project.default_branch

    console.info(`Projet ${project.name} trouvé sur framagit !`)
    console.info(`  - branche par défaut : ${mainBranch}\n`)
} catch (e) {
    if (e.code === "ERR_NON_2XX_3XX_RESPONSE") {
        console.error("=== Erreur HTTP ===")
        console.error("  Le token ne semble pas valide")
    } else {
        console.error(e)
    }
    process.exit(1)
}



/*

- rajouter une issue sur les limites actuelles et stratégies de mitigation
du spam qui peuvent être mises en place

*/

const server = Fastify();
server.register(fastifyCORS, {
    origin: [
        'http://127.0.0.1:4000',
        'http://localhost:4000',
        'https://davidbruant.frama.io', // à enlever quand la béta est validée
        'https://beta.framalibre.org', // à enlever après le lancement officiel
        'https://framalibre.org',
    ]
})


server.get('/', (request, response) => {
    response
        .code(404)
        .send(`Hey, bienvenue sur le serveur de contribution de framalibre ! Essaye plutôt de faire un POST sur /contribute !`)
})

server.get('/contribute', (request, response) => {
    response
        .code(405)
        .send(`Les GET ne sont pas acceptés sur /contribute. Essaye plutôt un POST !`)
})

server.post('/contribute', (request, response) => {
    // @ts-ignore
    const { contributor, actions } = request.body;

    /**
     * Data is passed directly from the HTTP request to the contribute function
     * without verifications because the contribution will always be part of a
     * gitlab merge request that will be manually reviewed
     */
    contribute({ contributor, actions })
        .then(() => {
            response
                .send(`Contribution acceptée et transformée en merge request sur framagit !`)
        })
        .catch(err => {
            response
                .code(502)
                .send(`Erreur côté framagit lors de l'envoi de la contribution (${err.message} - ${err.description} - ${err.code})`)
        })

})


server.listen({ port: PORT, host: HOST }, (err, address) => {
    if (err) {
        console.error(err)
        process.exit(1)
    }
    console.log(`Server is now listening on ${address}`)
})


/**
 * This function does not perform a lot of verifications and passes everything directly
 * to the commit. The commit will always be part of a gitlab merge request that will
 * be manually reviewed
 */
function contribute({ actions, contributor }) {
    const files = actions.map(({file_path}) => file_path)

    console.info(`\nNouvelle contribution:`)
    console.info(`  - fichiers concernés : ${ files.join(', ') }`)
    console.info(`  - contributeurice : ${contributor}`)

    const newBranchName = 'contribution-' + Math.random().toString(32).slice(2);

    const commitMessage = `Contribution de ${contributor} sur les fichiers ${files.join(', ')}`

    console.info(`  - nom de la branche git : ${newBranchName}`)
    console.info(`  - titre de la merge request : ${commitMessage}\n`)

    return commits.create(
        FRAMALIBRE_FRAMAGIT_PROJECT_ID,
        newBranchName,
        commitMessage,
        actions,
        {
            start_branch: mainBranch,
            author_name: `contributeur.rice formulaire framalibre - ${contributor}`
        }
    )
        .then(() => {
            return mrs.create(
                FRAMALIBRE_FRAMAGIT_PROJECT_ID,
                newBranchName,
                mainBranch,
                commitMessage,
                {
                    removeSourceBranch: true,
                    squash: true,
                }
            )
        })
        .then(res => {
            console.info(`  => Merge request créée ! Vous pouvez la consulter ici : ${res.web_url}`)
        })
}
